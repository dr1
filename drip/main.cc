#include <stdio.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/glu.h>

#include <jack/jack.h>

#include <time.h>

#include "drip.h"
#include "pluck.h"
#include "dline.h"
#include "event.h"

#include "drips.h"

struct PLOP
{
  float ramp, rampi, freqi, amp, phase, ramp2, rampi2, env, osc;
  int rampn;
};

float plop(PLOP &p, float mass, float energy)
{
  if (mass > 0)
  {
    float t = 1000 * mass / 8;
    p.ramp = 0;
    p.rampi = 1 / (t / 7 / 1000 * 48000);
    p.rampn = 144; // 3ms
    p.freqi = (t / 5 + 400) / 48000;
    p.amp = t / 1000;
  }
  p.ramp += p.rampi;
  p.ramp = fminf(1, p.ramp);
  p.rampn -= 1;
  if (p.rampn == 0) {
    p.ramp2 = 0;
    p.rampi2 = p.rampi;
  }
  p.ramp2 += p.rampi2;
  p.ramp2 = fminf(1, p.ramp2);
  float amp = 1 - p.ramp;
  amp *= amp;
  amp *= amp;
  // lop 12
  amp *= 0.01;
  amp += 0.99 * p.env;
  p.env = amp;
  amp *= p.amp;
  amp *= sinf(2.0 * 3.141592653589793 * (p.phase += p.freqi));
  p.phase -= floorf(p.phase);
  // lop 1000
  amp *= 0.1;
  amp += 0.9 * p.osc;
  p.osc = amp;
  return amp;
}

struct AUDIO {
  DRIP tap;
  PLOP p6;
#if 0
  PLUCK<960/6 + 1> p6l;
  PLUCK<960/6 - 1> p6r;
//#if 0
  DLINE<1> d6;
  DRIP t6;
  PLUCK<960/5 - 1> p5l;
  PLUCK<960/5 + 1> p5r;
  DLINE<1> d5;
  DRIP t5;
  PLUCK<960/4 + 1> p4l;
  PLUCK<960/4 - 1> p4r;
  DLINE<1> d4;
  DRIP t4;
  PLUCK<960/3 - 1> p3l;
  PLUCK<960/3 + 1> p3r;
  DLINE<1> d3;
  DRIP t3;
  PLUCK<960/2 + 1> p2l;
  PLUCK<960/2 - 1> p2r;
  DLINE<1> d2;
  DRIP t2;
  PLUCK<960/1 - 1> p1l;
  PLUCK<960/1 + 1> p1r;
#endif
//  DLINE<480/1> d1;
//  PLANT p
};

jack_client_t *client;
jack_port_t *port[2];
AUDIO a;
volatile int dflow = 0;
volatile unsigned long int count = 0;
volatile double FLOW = 0;

EVENTS e;

double rms = 0;
double gain = 0;


static int processcb(jack_nframes_t nframes, void *arg) {
  (void) arg;
  jack_default_audio_sample_t *out[2];
  out[0] = (jack_default_audio_sample_t *)
    jack_port_get_buffer(port[0], nframes);
  out[1] = (jack_default_audio_sample_t *)
    jack_port_get_buffer(port[1], nframes);
  double flow = FLOW;
  for (unsigned int k = 0; k < nframes; ++k) {
    double t = count / (48000.0 * 60.0 * 60.0);
    if (dflow) {
      flow = t < 1 ? t : 0;
      count++;
    }
    flow /= 4800;
    double mass = flow, energy = 0;
    double audiol = 0, audior = 0;
    double mass0 = mass;
    mass = 0;
    for (int i = 0; i < 10; ++i)
    {
      double energy1;
       mass += drip(a.tap, mass0, energy1);
       energy += energy1;
     }
    if (0 < mass) { event(e, count, mass, energy, 6); }
    float am = plop(a.p6, mass, energy);
    audiol += am;
    audior += am;
#if 0
    audiol += pluck(a.p6l, energy);
    audior += pluck(a.p6r, energy);
    mass = dline(a.d6, mass);
    mass = drip(a.t6, mass, energy);
    if (0 < mass) { event(e, count, mass, energy, 5); }
    audiol += pluck(a.p5l, energy);
    audior += pluck(a.p5r, energy);
    mass = dline(a.d5, mass);
    mass = drip(a.t5, mass, energy);
    if (0 < mass) { event(e, count, mass, energy, 4); }
    audiol += pluck(a.p4l, energy);
    audior += pluck(a.p4r, energy);
    mass = dline(a.d4, mass);
    mass = drip(a.t4, mass, energy);
    if (0 < mass) { event(e, count, mass, energy, 3); }
    audiol += pluck(a.p3l, energy);
    audior += pluck(a.p3r, energy);
    mass = dline(a.d3, mass);
    mass = drip(a.t3, mass, energy);
    if (0 < mass) { event(e, count, mass, energy, 2); }
    audiol += pluck(a.p2l, energy);
    audior += pluck(a.p2r, energy);
    mass = dline(a.d2, mass);
    mass = drip(a.t2, mass, energy);
    if (0 < mass) { event(e, count, mass, energy, 1); }
    audiol += pluck(a.p1l, energy);
    audior += pluck(a.p1r, energy);
#endif
    //mass = dline(a.d1, mass);
    //plant(a.p, mass);
    rms = rms * 0.9999 + 0.0001 * (audiol * audiol + audior * audior);
    gain = gain * 0.9999 + 0.0001 * 0.25 / sqrt(rms);
    if (! isfinite(gain)) { gain = 0; }
    gain = fmin(1, gain);
    out[0][k] = gain * audiol;
    out[1][k] = gain * audior;
  }
  if (dflow)
    FLOW = flow;
  return 0;
}

void keycb(GLFWwindow *window, unsigned int key) {
  switch (key) {
    case 'q': glfwSetWindowShouldClose(window, GL_TRUE); break;
    case ' ': dflow = 1 - dflow; break;
    case '\'': FLOW -= 1; break;
    case '[': FLOW -= 0.1; break;
    case '-': FLOW -= 0.01; break;
    case '=': FLOW += 0.01; break;
    case ']': FLOW += 0.1; break;
    case '#': FLOW += 1; break;
  }
  FLOW = fmax(0, FLOW);
  fprintf(stderr, "%.16f\t%.16f\r", FLOW, gain);
}

const char *drip_vert =
"#version 330 core\n"
"void main() {\n"
"  vec4 pos[4] = vec4[4](vec4(-1.0, -1.0, 0.0, 1.0)\n"
"                       ,vec4(-1.0,  1.0, 0.0, 1.0)\n"
"                       ,vec4( 1.0, -1.0, 0.0, 1.0)\n"
"                       ,vec4( 1.0,  1.0, 0.0, 1.0)\n"
"                       );\n"
"  gl_Position = pos[gl_VertexID];\n"
"}\n"
;

const char *drip_frag =
"#version 330 core\n"
"#extension GL_ARB_explicit_uniform_location : require\n"
"layout (location = 0) uniform float dmass;\n"
"layout (location = 1) uniform sampler2D prev;\n"
"layout (location = 0) out vec3 next;\n"
"void main()\n"
"{\n"
"  float dt = 1.0 / 4800.0;\n"
"  float density = 1;\n"
"  float radius = 0.916;\n"
"  float area = 3.141592653589793 * radius * radius;\n"
"  float flow = dmass / (density * area);\n"
"  vec3 d = texelFetch(prev, ivec2(gl_FragCoord.xy), 0).xyz;\n"
"  for (int j = 0; j < 1024; ++j)\n"
"  {\n"
"      float mass = d.x + dmass;\n"
"      float velocity = d.z;\n"
"      float gravity = 1.0 * mass;\n"
"      float spring = -max(0, 52.5 - 11.4 * mass) * d.y;\n"
"      float push = dmass * (flow - velocity);\n"
"      float resistance = -0.05 * velocity;\n"
"      float force = gravity + spring + push + resistance;\n"
"      float acceleration = force / mass;\n"
"      velocity += acceleration * dt;\n"
"      float position = d.y + velocity * dt;\n"
"      d.x = mass;\n"
"      d.y = position;\n"
"      d.z = velocity;\n"
"      if (d.y > 5.5) {\n"
"        float m = 0.8 * d.x - 0.3;\n"
"        d.x = d.x - m;\n"
"        d.y = d.y - 5.5 + 2;\n"
"        d.z = 0;\n"
"      }\n"
"  }\n"
"  next = d;\n"
"}\n"
;

const char *drop_vert =
"#version 330 core\n"
"#extension GL_ARB_explicit_uniform_location : require\n"
"layout (location = 0) uniform sampler2D prev;\n"
"layout (location = 1) uniform sampler2D next;\n"
"out vec4 colour;\n"
"void main()\n"
"{\n"
"  int i = (gl_VertexID / 2) % textureSize(prev, 0).x;\n"
"  int j = (gl_VertexID / 2) / textureSize(prev, 0).x;\n"
"  bool even = (gl_VertexID & 1) == 0;\n"
"  vec3 vprev = texelFetch(prev, ivec2(i, j), 0).xyz;\n"
"  vec3 vnext = texelFetch(next, ivec2(i, j), 0).xyz;\n"
"  vec3 vertex = even ? vprev : vnext;\n"
"  gl_Position = vec4((vertex.x - 4.0) / 3.5, - vertex.z / 16.0, 0.0, 1.0);\n"
"  float h = (vertex.y + 2.0) / 8.0;\n"
"  colour = vec4(h, even ? 0.0 : 1.0 - h, even ? 1.0 - h : h, vprev.x < vnext.x ? 0.001 : 0.0);\n"
"}\n"
;

const char *drop_frag =
"#version 330 core\n"
"in vec4 colour;\n"
"layout (location = 0) out vec4 c;\n"
"void main()\n"
"{\n"
"  c = colour;\n"
"}\n"
;

void debug_program(GLuint program, const char *name)
{
  if (program)
  {
    GLint linked = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (linked != GL_TRUE) fprintf(stderr, "%s: OpenGL shader program link failed\n", name);
    GLint len = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
    char *s = (char *) calloc(1, len + 1);
    if (! s) abort();
    glGetProgramInfoLog(program, len, 0, s);
    s[len] = 0;
    if (*s)
      fprintf(stderr, "%s: OpenGL shader program info log\n%s\n", name, s);
    free(s);
  }
  else
  {
    fprintf(stderr, "%s: OpenGL shader program creation failed\n", name);
  }
}

void debug_shader(GLuint shader, GLenum typ, const char *name)
{
  const char *tname = "unknown";
  if (typ == GL_VERTEX_SHADER) tname = "vertex";
  if (typ == GL_FRAGMENT_SHADER) tname = "fragment";
  if (shader)
  {
    GLint compiled = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (compiled != GL_TRUE) fprintf(stderr, "%s: OpenGL %s shader compile failed\n", name, tname);
    GLint len = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
    char *s = (char *) calloc(1, len + 1);
    if (! s) abort();
    glGetShaderInfoLog(shader, len, 0, s);
    s[len] = 0;
    if (*s) fprintf(stderr, "%s: OpenGL %s shader info log\n%s\n", name, tname, s);
    free(s);
  }
  else
  {
    fprintf(stderr, "%s: OpenGL %s shader creation failed\n", name, tname);
  }
}

void compile_shader(GLuint program, GLenum typ, const char *name, const char *source)
{
  GLuint shader = glCreateShader(typ);
  glShaderSource(shader, 1, &source, 0);
  glCompileShader(shader);
  debug_shader(shader, typ, name);
  glAttachShader(program, shader);
  glDeleteShader(shader);
}

GLuint compile_program(const char *name, const char *vert, const char *frag)
{
  GLuint program = glCreateProgram();
  compile_shader(program, GL_VERTEX_SHADER, name, vert);
  compile_shader(program, GL_FRAGMENT_SHADER, name, frag);
  glLinkProgram(program);
  debug_program(program, name);
  return program;
}


int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  if (! glfwInit()) {
    return 1;
  }
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(1280, 720, "drip", 0, 0);
  if (! window) {
    return 1;
  }
  glfwSetCharCallback(window, keycb);
  glfwMakeContextCurrent(window);
  glewInit();

  if (!(client = jack_client_open("drip", JackOptions(0), 0))) {
    return 1;
  }
  jack_set_process_callback(client, processcb, 0);
  port[0] = jack_port_register(
    client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
  );
  port[1] = jack_port_register(
    client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
  );
  if (jack_activate(client)) {
    fprintf(stderr, "cannot activate JACK client");
    return 1;
  }
  const char **ports;
  if ((ports = jack_get_ports(
    client, NULL, NULL, JackPortIsPhysical | JackPortIsInput
  ))) {
    int i = 0;
    while (ports[i] && i < 2) {
      if (jack_connect(
        client, jack_port_name(port[i]), ports[i]
      )) {
        fprintf(stderr, "cannot connect output port\n");
      }
      i++;
    }
    free(ports);
  }

  GLuint tex, fbo;
  glGenTextures(1, &tex);
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 1280, 720, 0, GL_RGBA, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT);


  GLuint tex2[2], fbo2[2];
  glGenTextures(2, &tex2[0]);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex2[0]);
#define TEX_SIZE 1024
{
  float init[TEX_SIZE*TEX_SIZE][3];
  for (int i = 0; i < TEX_SIZE * TEX_SIZE; ++i)
  {
      init[i][0] = rand() / (float) RAND_MAX * 5;
      init[i][1] = rand() / (float) RAND_MAX * 8 - 2.5;
      init[i][2] = (rand() / (float) RAND_MAX * 2 - 1) * 10;
  }
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, TEX_SIZE, TEX_SIZE, 0, GL_RGB, GL_FLOAT, init);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, tex2[1]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, TEX_SIZE, TEX_SIZE, 0, GL_RGB, GL_FLOAT, init);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
}
  glGenFramebuffers(2, &fbo2[0]);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo2[0]);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex2[0], 0);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo2[1]);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex2[1], 0);
  int which = 0;
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT);

  GLuint pdrip = compile_program("drip", drip_vert, drip_frag);
  GLuint pdrop = compile_program("drop", drop_vert, drop_frag);

  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);

      float colours[6][3] =
        { { 1, 1, 0 }
        , { 0, 1, 0 }
        , { 0, 1, 1 }
        , { 0, 0, 1 }
        , { 1, 0, 1 }
        , { 1, 0, 0 }
        };


  dflow = 1;

  unsigned long int speed = 48000;
  unsigned long int last5 = 0;
  unsigned long int frame = 0;
  
  double mass_mean = 3;
  double mass_stddev = 10;
  double energy_mean = 5;
  double energy_stddev = 10;
  
//  DRIPS<1920 * 108> dv = DRIPS<1920 * 108>();
 
  do {
//    fprintf(stderr, "%ld\n", last5);
//    glClear(GL_COLOR_BUFFER_BIT);
    unsigned long int now = count;
    int head = e.head;

    glViewport(0, 0, TEX_SIZE, TEX_SIZE);
    glDisable(GL_BLEND);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo2[1-which]);
    glUseProgram(pdrip);
    glUniform1f(0, FLOW);
    glUniform1i(1, which);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glViewport(0, 0, 1280, 720);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glUseProgram(0);
    glLoadIdentity();
    glOrtho(-1, 1, -1, 1, -1, 1);
    glBegin(GL_QUADS);
      glColor4f(0, 0, 0, 0.2);
      glVertex2f(-1, -1);
      glVertex2f(-1,  1);
      glVertex2f( 1,  1);
      glVertex2f( 1, -1);
      glColor4f(1, 1, 1, 1);
    glEnd();

    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    glUseProgram(pdrop);
    glUniform1i(0, which);
    glUniform1i(1, 1 - which);
    glDrawArrays(GL_LINES, 0, TEX_SIZE * TEX_SIZE * 2);

    which = 1 - which;
#if 0

/*
    auto odv = dv;
    drips(dv, FLOW / 10, 200);
*/
    glLoadIdentity();
    glOrtho(-16, 16, 1, 6, -1, 1);
    glBegin(GL_LINES);
    for (int i = 0; i < 1920 * 108; ++i)
    {
      if (odv.mass[i] > dv.mass[i]) continue;
      float a = 0.005;
      float h = (odv.position[i] + 2) / 8;
      glColor4f(h, 0, 1 - h, a);
      glVertex2f(odv.velocity[i], odv.mass[i]);
      h = (dv.position[i] + 2) / 8;
      glColor4f(h, 1 - h, h, a);
      glVertex2f(dv.velocity[i], dv.mass[i]);
    }
    glEnd();
#endif


    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBlitFramebuffer(0, 0, 1280, 720, 0, 0, 1280, 720, GL_COLOR_BUFFER_BIT, GL_NEAREST);

#if 0
//    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    glLoadIdentity();
    double aspect = 16.0 / 9.0;
    glOrtho(-aspect, aspect, -1, 1, -1, 1);
    glBegin(GL_QUADS);
    int setlast5 = 0;
    for (int i = 0; i < 8192; ++i) {
      int j = (head + i) % 8192;
      unsigned long int timestamp = e.e[j].timestamp;
      if (timestamp + 48000 * 60 < now) {
        break;
      }
      if (timestamp == 0) {
        break;
      }
      double mass = e.e[j].mass;
      double energy = e.e[j].energy;
      int voice = (e.e[j].voice + 5) % 6;
      if (voice == 5)
      {
        if (setlast5 == 1)
        {
          speed = last5 - timestamp;
          setlast5 = 2;
        }
        if (setlast5 == 0)
        {
          last5 = timestamp;
          setlast5 = 1;
        }
      }
      double angle = fmod((last5 - (double) timestamp) / (1.0 * speed), 1);
      angle = fmod(1 + angle, 1);
      double angle0 = angle - 0.005 * gain * sqrt(mass);
      double angle1 = angle + 0.005 * gain * sqrt(mass);
      double radius = pow(0.9, (now - timestamp) / (1.0 * speed));
      double dradius = sqrt(1 + 0.05 * gain * sqrt(energy));
      double radius0 = radius * dradius;
      double radius1 = radius;// / dradius;
      float alpha = (1024 / (65536 * (angle1 - angle0) * (radius0 - radius1)));
//fprintf(stderr, "%4d %d %e %e\t%e %e\t%e %e\t%e %ld\n", i, voice, mass, energy, angle0, angle1, radius0, radius1, alpha, speed);
      float x00 = radius0 * cos(2 * 3.141592653589793 * angle0);
      float y00 = radius0 * sin(2 * 3.141592653589793 * angle0);
      float x01 = radius0 * cos(2 * 3.141592653589793 * angle1);
      float y01 = radius0 * sin(2 * 3.141592653589793 * angle1);
      float x11 = radius1 * cos(2 * 3.141592653589793 * angle1);
      float y11 = radius1 * sin(2 * 3.141592653589793 * angle1);
      float x10 = radius1 * cos(2 * 3.141592653589793 * angle0);
      float y10 = radius1 * sin(2 * 3.141592653589793 * angle0);
      glColor4f(colours[voice][0], colours[voice][1], colours[voice][2], alpha);
#if 1
      glVertex2f(x00, y00);
      glVertex2f(x01, y01);
      glVertex2f(x11, y11);
      glVertex2f(x10, y10);
#else
      glVertex2f(angle0, radius0);
      glVertex2f(angle1, radius0);
      glVertex2f(angle1, radius1);
      glVertex2f(angle0, radius1);
#endif
    }
    glEnd();
#endif
#if 0
    glLoadIdentity();
    glOrtho(mass_mean - 3 * mass_stddev, mass_mean + 3 * mass_stddev, energy_mean - 3 * energy_stddev, energy_mean + 3 * energy_stddev, -1, 1);
    glBegin(GL_LINES);
    float previous[6][2];
    memset(previous, 0, sizeof(previous));
    double mass0 = 0, mass1 = 0, mass2 = 0;
    double energy0 = 0, energy1 = 0, energy2 = 0;
    for (int i = 0; i < 8192; ++i)
    {
      int j = (head + i) % 8192;
      unsigned long int timestamp = e.e[j].timestamp;
      if (timestamp + 48000 * 60 < now) {
        break;
      }
      if (timestamp == 0) {
        break;
      }
      double mass = e.e[j].mass;
      double energy = e.e[j].energy;
      int voice = (e.e[j].voice + 5) % 6;
      if (voice != 5) continue;
      double w = pow(1 - (now - timestamp) / (60.0 * 48000), 4);
      mass0 += w;
      mass1 += w * mass;
      mass2 += w * mass * mass;
      energy0 += w;
      energy1 += w * energy;
      energy2 += w * energy * energy;
      if (previous[voice][0])
      {
        glColor4f(colours[voice][0], colours[voice][1], colours[voice][2], w);
        glVertex2f(previous[voice][0], previous[voice][1]);
        glVertex2f(mass, energy);
      }
      previous[voice][0] = mass;
      previous[voice][1] = energy;
    }
    glEnd();
    if (mass0 > 1)
    {
      mass_mean *= 0.9;
      mass_mean += 0.1 * mass1 / mass0;
      mass_stddev *= 0.9;
      mass_stddev += 0.1 * sqrt(mass0 * mass2 - mass1 * mass1) / mass0;
    }
    if (energy0 > 1)
    {
      energy_mean *= 0.9;
      energy_mean += 0.1 * energy1 / energy0;
      energy_stddev *= 0.9;
      energy_stddev += 0.1 * sqrt(energy0 * energy2 - energy1 * energy1) / energy0;
    }
#endif
    glfwSwapBuffers(window);
    glfwPollEvents();
  } while (! glfwWindowShouldClose(window));
  glfwTerminate();
  return 0;
}
