#ifndef DRIPS_H
#define DRIPS_H 1

#include <math.h>

template <int N>
struct DRIPS {
  float mass[N];
  float position[N];
  float velocity[N];
  DRIPS() {
    for (int i = 0; i < N; ++i)
    {
      mass[i] = rand() / (float) RAND_MAX * 5;
      position[i] = rand() / (float) RAND_MAX * 8 - 2.5;
      velocity[i] = (rand() / (float) RAND_MAX * 2 - 1) * 10;
    }
  };
};

template <int N>
void drips(DRIPS<N> &d, float dmass, int L) {
  float dt = 1.0 / 4800.0;
  float density = 1;
  float radius = 0.916;
  float area = 3.141592653589793 * radius * radius;
  float flow = dmass / (density * area);
  #pragma omp parallel for
  for (int i = 0; i < N; ++i)
  {
    for (int j = 0; j < L; ++j)
    {
      float mass = d.mass[i] + dmass;
      float velocity = d.velocity[i];
      float gravity = 1 * mass;
      float spring = -fmax(0, 52.5 - 11.4 * mass) * d.position[i];
      float push = dmass * (flow - velocity);
      float resistance = -0.05 * velocity;
      float force = gravity + spring + push + resistance;
      float acceleration = force / mass;
      velocity += acceleration * dt;
      float position = d.position[i] + velocity * dt;
      d.mass[i] = mass;
      d.position[i] = position;
      d.velocity[i] = velocity;
      if (d.position[i] > 5.5) {
        float m = 0.8 * d.mass[i] - 0.3;
        d.mass[i] = d.mass[i] - m;
        d.position[i] = d.position[i] - 5.5 + 2;
        d.velocity[i] = 0;
      }
    }
  }
}

#endif
