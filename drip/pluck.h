#ifndef PLUCK_H
#define PLUCK_H 1

#include <stdlib.h>
#include <string.h>

template <int P>
struct PLUCK {
  int w;
  int i;
  float z[2][P];
  PLUCK() : w(0), i(0) {
    memset(&z[0][0], 0, 2 * P * sizeof(z[0][0]));
  };
};

template <int P>
float pluck(PLUCK<P> &p, float amp) {
  if (amp > 0) {
    for (int j = 0; j < P; ++j) {
      p.z[p.w][j] = (480.0 / P) * 1e-2 * sqrt(amp) * (rand() / (double) RAND_MAX - 0.5);
    }
    p.i = 0;
  }
  if (p.i >= P) {
    for (int j = 0; j < P; ++j) {
      p.z[1 - p.w][j] = pow(0.999, 480.0 / P) / 3.0 * (p.z[p.w][j] + p.z[p.w][(j + 1)% P] + p.z[p.w][(j + P - 1)% P]);
    }
    p.i = 0;
    p.w = 1 - p.w;
  }
  return p.z[p.w][p.i++];
}

#endif
