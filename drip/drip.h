#ifndef DRIP_H
#define DRIP_H 1

#include <math.h>

struct DRIP {
  float mass;
  float position;
  float velocity;
  DRIP() : mass(0.375), position(2), velocity(0) { };
};

float drip(DRIP &d, float dmass, double &energy_out) {
  float dt = 1.0 / 4800.0; // 0.001;
//  float momentum = d.mass * d.velocity;
  float density = 1;
  float radius = 0.916;
  float area = 3.141592653589793 * radius * radius;
  float flow = dmass / density / area;
  float mass = d.mass + dmass;
  float velocity = d.velocity; // momentum / mass;
//  if (! isfinite(velocity)) { velocity = 0; }
  float gravity = 1 * mass;
  float spring = -fmax(0, 52.5 - 11.4 * mass) * d.position;
  float push = dmass * (flow - velocity);
  float resistance = -0.05 * velocity;
  float force = gravity + spring + push + resistance;
  float acceleration = force / mass;
  velocity += acceleration * dt;
  float position = d.position += velocity * dt;
  d.mass = mass;
  d.position = position;
  d.velocity = velocity;
  if (d.position > 5.5) {
    float m = 0.8 * d.mass - 0.3;
    float v = d.velocity;
    d.mass = d.mass - m;
    d.position = d.position - 5.5 + 2;
    d.velocity = 0;
    energy_out = 0.5 * m * v * v;
    return m;
  } else {
    energy_out = 0;
    return 0;
  }
}

#endif
