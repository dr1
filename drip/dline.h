#ifndef DLINE_H
#define DLINE_H 1

template <int N>
struct DLINE {
  double d[N];
  DLINE() { memset(d, 0, sizeof(d)); };
};

template <int N>
double dline(DLINE<N> &d, double x) {
  double y = 0.0001 * d.d[N-1];
  for (int i = N - 1; i > 0; --i) {
    d.d[i] = 0.9999 * d.d[i] + 0.0001 * d.d[i - 1];
  }
  d.d[0] = 0.9999 * d.d[0] + x;
  return y;
}

#endif
