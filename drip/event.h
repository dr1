#ifndef EVENT_H
#define EVENT_H 1

struct EVENT {
  unsigned long int timestamp;
  double mass;
  double energy;
  int voice;
  EVENT() : timestamp(0), mass(0), energy(0), voice(0) { };
};

struct EVENTS {
  EVENT e[8192];
  int head;
  EVENTS() : head(0) { };
};

void event(EVENTS &event, unsigned long int timestamp, double mass, double energy, int voice) {
  int h = (event.head - 1 + 8192) % 8192;
  event.e[h].timestamp = timestamp;
  event.e[h].mass = mass;
  event.e[h].energy = energy;
  event.e[h].voice = voice;
  event.head = h;
}

#endif
