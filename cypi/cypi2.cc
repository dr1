/*
    cypi -- audio-visual drone for OpenGL and SDL2
    Copyright (C) 2010,2018,2022  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* standard C library */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_opengles2.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/html5.h>
#include "/usr/include/glm/glm.hpp" // HACK
#else
#include <glm/glm.hpp>
#endif

#define twopi 6.283185307179586
#define CYPI_SIZE 1024

unsigned int sample;
unsigned int frame;
unsigned int frameR;

SDL_Window *window = nullptr;

/* JACK state */
static struct {
  /* DC blocker filters */
  float r[2], xn1[2], yn1[2];
} cypi_jack;

/* OpenGL state */
static struct {
  /* window */
  float scale;
  /* shader */
  GLuint prog;
  GLuint prog2;
  /* uniforms */
  GLint number;
  GLint amount;
  GLint fade;
  glm::mat4 numberm;
  glm::vec4 amountv;
} cypi_gl;

GLuint vbo = 0;
GLuint vbo2 = 0;

/* GLUT display callback */
static void cypi_display() {
  /* modulation amounts based on frame count */
  float z = 0.5 - 0.5 * cos(twopi * frame / (CYPI_SIZE / 2));
  for (int i = 0; i < 4; ++i) {
    cypi_gl.amountv[i] = z;
  }
  float c = frameR / (float) (CYPI_SIZE/4);
  c = fmin(fmax(c, 0.0), 1.0);

  /* handle aspect */
  int display_w = 1280;
  int display_h = 720;
#ifdef __EMSCRIPTEN__
  if (emscripten_get_canvas_element_size("#canvas", &display_w, &display_h) != EMSCRIPTEN_RESULT_SUCCESS)
  {
    // failsafe
    display_w = 1280;
    display_h = 720;
  }
#else
  SDL_GL_GetDrawableSize(window, &display_w, &display_h);
#endif
  glViewport(0, 0, display_w, display_h);
  glClear(GL_COLOR_BUFFER_BIT);
  int x, y, w, h;
  if (16 * display_h > 4.5 * display_w) {
    // narrow and tall
    x = 0;
    w = display_w;
    h = 4.5 * display_w / 16;
    y = (display_h - h) / 2;
  }
  else
  {
    // wide and short
    y = 0;
    h = display_h;
    w = 16 * display_h / 4.5;
    x = (display_w - w) / 2;
  }
  glViewport(x, y, w, h);

  /* draw */
  glUniformMatrix4fv(cypi_gl.number, 1, 0, &cypi_gl.numberm[0][0]);
  glUniform4fv(cypi_gl.amount, 1, &cypi_gl.amountv[0]);
  glUniform1f(cypi_gl.fade, c);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  /* display */
  SDL_GL_SwapWindow(window);
  /* report errors */
  int status = glGetError();
  if(status) fprintf(stderr, "GL: error %08x\n", status);
}

static void cypi_audio(void *userdata, Uint8 *stream, int len)
{
  (void) userdata;
  glm::mat4 number = cypi_gl.numberm;
  glm::vec4 amount = cypi_gl.amountv;
  float *b = (float *) stream;
  int m = len / sizeof(float) / 2;
  int k = 0;
  for (int i = 0; i < m; ++i)
  {
    float out[2];
    // texture coordinates
    if (++sample == CYPI_SIZE * CYPI_SIZE / 2) {
      sample = 0;
      for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
          int r = rand() % 17;
          cypi_gl.numberm[i][j] = r - 8;
        }
      }
    }
    if (sample % CYPI_SIZE == 0)
    {
      frame = (frame + 1) % (CYPI_SIZE / 2);
      frameR++;
    }
    float f = frameR / (float) (CYPI_SIZE/4);
    f = fmin(fmax(f, 0.0), 1.0);
    float x = (sample % CYPI_SIZE) / (float) CYPI_SIZE;
    float t = sample / (float) (CYPI_SIZE * CYPI_SIZE / 2);
    glm::vec4 p[2] = { glm::vec4(x, t, x, t), glm::vec4(t, x, t, x) };
    for (int c = 0; c < 2; ++c)
    {
      // same maths as fragment shader
      glm::vec4 q = number * p[c];
      glm::vec4 x = -glm::cos(6.283185307179586f * (p[c] + amount * glm::sin(6.283185307179586f * q)*0.5f+0.5f))*0.5f+0.5f;
      float xn = f * x.x * x.y * x.z * x.w;
      // high pass filter
      cypi_jack.yn1[c] =
        xn - cypi_jack.xn1[c] + cypi_jack.r[c] * cypi_jack.yn1[c];
      cypi_jack.xn1[c] = xn;
      out[c] = cypi_jack.yn1[c];
    }
    b[k++] = out[0];
    b[k++] = out[1];
  }
}

#ifdef __EMSCRIPTEN__
static void main1(void)
{
  cypi_display();
}
#endif

/* main program */
int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  printf("cypi (AGPL3) 2010,2018,2022 Claude Heiland-Allen\n");

  SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO);
  // TODO adapt to window size
  int width = 1280;
  int height = 720;
#ifdef __EMSCRIPTEN__
  if (emscripten_get_canvas_element_size("#canvas", &width, &height) != EMSCRIPTEN_RESULT_SUCCESS)
  {
    std::fprintf(stderr, "error: couldn't get canvas element size\n");
    // failsafe
    width = 1280;
    height = 720;
  }
#endif

  // GL ES 2.0 + GLSL 100
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
  window = SDL_CreateWindow("cypi", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
  if (! window)
  {
    const std::string message = "SDL_CreateWindow: " + std::string(SDL_GetError());
    if (0 != SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "cypi", message.c_str(), nullptr))
    {
      SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "%s", message.c_str());
    }
    SDL_Quit();
    return 1;
  }
  SDL_GLContext gl_context = SDL_GL_CreateContext(window);
  if (! gl_context)
  {
    const std::string message = "SDL_GL_CreateContext: " + std::string(SDL_GetError());
    if (0 != SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "cypi", message.c_str(), window))
    {
      SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "%s", message.c_str());
    }
    SDL_Quit();
    return 1;
  }
  SDL_GL_MakeCurrent(window, gl_context);
  SDL_GL_SetSwapInterval(1);

  glDisable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);

  /* bad source of pseudorandomness */
  unsigned int t = time(NULL);
  //printf("%08X\n", t);
  srand(t);

  /* the CyPi shader source code */
  const char *vert_src =
    "#version 100\n"
    "attribute vec2 v_position;\n"
    "attribute vec2 v_texcoord;\n"
    "attribute vec2 v_fadecoord;\n"
    "varying vec2 texcoord;\n"
    "varying vec2 fadecoord;\n"
    "void main() {\n"
    "  gl_Position = vec4(v_position, 0.0, 1.0);\n"
    "  texcoord = v_texcoord;\n"
    "  fadecoord = v_fadecoord;\n"
    "}\n"
    ;
  const char *frag_src =
    "#version 100\n"
    "precision highp float;\n"
    "uniform mat4 number;\n"
    "uniform vec4 amount;\n"
    "uniform float fade;\n"
    "varying vec2 texcoord;\n"
    "varying vec2 fadecoord;\n"
    "const float twopi = 6.283185307179586;\n"
    "vec2 tanh_(vec2 x) {"
    "  vec2 e = exp(x);\n"
    "  return (e - 1.0 / e) / (e + 1.0 / e);\n"
    "}\n"
    "void main() {\n"
    "  vec4 p = texcoord.xyxy;\n"
    "  vec4 q = number * p;\n"
    "  vec4 x = -cos(twopi * (p + amount * sin(twopi * q)*0.5+0.5))*0.5+0.5;\n"
    "  float z = x.x * x.y * x.z * x.w;\n"
    "  vec2 f = tanh_((clamp(1.0 - cos(fadecoord * twopi), 0.0, 1.0)) * 8.0);\n"
    "  gl_FragColor = vec4(vec3(z * f.x * f.y * fade), 1.0);\n"
    "}\n"
    ;
  /* set up OpenGL variables */
  frame = 0;
  frameR = 0;
  cypi_gl.scale = 1;
  /* initialize OpenGL */
  float texw = CYPI_SIZE;
  float texh = CYPI_SIZE;
  float tx0 = (1.0 * texw - 1920) / (1.0 * texw);
  float ty0 = (2.0 * texh - 1080) / (2.0 * texh);
  float tx1 = (1.0 * texw + 1920) / (1.0 * texw);
  float ty1 = (2.0 * texh + 1080) / (2.0 * texh);
  float data[4 * 6] =
    { tx0, ty0, 0, 0, -1, -1
    , tx1, ty0, 1, 0,  1, -1
    , tx0, ty1, 0, 1, -1,  1
    , tx1, ty1, 1, 1,  1,  1
    };
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), ((char *) nullptr) + 4 * sizeof(GLfloat));
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), ((char *) nullptr) + 0 * sizeof(GLfloat));
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), ((char *) nullptr) + 2 * sizeof(GLfloat));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);

  /* compile shaders */
  char infolog[1000];
  GLuint prog = glCreateProgram();
  GLuint vert = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vert, 1, &vert_src, 0);
  glCompileShader(vert);
  glGetShaderInfoLog(vert, sizeof(infolog), nullptr, infolog),
  //fprintf(stderr, "%s\n", infolog);
  glAttachShader(prog, vert);
  glDeleteShader(vert);
  GLuint frag = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(frag, 1, &frag_src, 0);
  glCompileShader(frag);
  glGetShaderInfoLog(frag, sizeof(infolog), nullptr, infolog),
  //fprintf(stderr, "%s\n", infolog);
  glAttachShader(prog, frag);
  glDeleteShader(frag);
  glBindAttribLocation(prog, 0, "v_position");
  glBindAttribLocation(prog, 1, "v_texcoord");
  glBindAttribLocation(prog, 2, "v_fadecoord");
  glLinkProgram(prog);
  glGetProgramInfoLog(prog, sizeof(infolog), nullptr, infolog),
  //fprintf(stderr, "%s\n", infolog);
  glUseProgram(prog);
  /* access uniforms */
  cypi_gl.number = glGetUniformLocation(prog, "number");
  cypi_gl.amount = glGetUniformLocation(prog, "amount");
  cypi_gl.fade   = glGetUniformLocation(prog, "fade");

  /* initialize DC blocking filters */
  cypi_jack.r[0] = 0.995;
  cypi_jack.xn1[0] = 0;
  cypi_jack.yn1[0] = 0;
  cypi_jack.r[1] = 0.995;
  cypi_jack.xn1[1] = 0;
  cypi_jack.yn1[1] = 0;

  SDL_AudioSpec want, have;
  want.freq = 44100;
  want.format = AUDIO_F32;
  want.channels = 2;
  want.samples = 1024;
  want.callback = cypi_audio;
  SDL_AudioDeviceID dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_ANY_CHANGE);
  if (have.freq > 192000 || have.format != AUDIO_F32 || have.channels != 2)
  {
    fprintf(stderr, "want: %d %d %d %d\n", want.freq, want.format, want.channels, want.samples);
    fprintf(stderr, "have: %d %d %d %d\n", have.freq, have.format, have.channels, have.samples);
    fprintf(stderr, "error: bad audio parameters\n");
  }
  else
  {
    // start audio processing
    SDL_PauseAudioDevice(dev, 0);
#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop(main1, 0, 1);
#else
    bool running = true;
    while (running)
    {
      SDL_Event e;
      while (running && SDL_PollEvent(&e))
      {
        switch (e.type)
        {
          case SDL_QUIT:
            running = false;
            break;
        }
      }
      if (running)
      {
        cypi_display();
      }
    }
#endif
  }
  return 0;
}
