/*
    cypi -- audio-visual drone for OpenGL and JACK
    Copyright (C) 2010,2018  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* standard C library */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* OpenGL library */
#include <GL/glew.h>
#include <GL/glut.h>

/* JACK library */
#include <jack/jack.h>

#define twopi 6.283185307179586
#define CYPI_SIZE 1024
#define CYPI_COUNT 48

/* non-realtime mode */
static int cypi_nrt;

/* JACK state */
static struct {
  /* JACK handles */
  jack_client_t *client;
  jack_port_t *port[2];
  /* audio buffers */
  unsigned int index;
  jack_default_audio_sample_t buffer[2][2][CYPI_SIZE];
  /* DC blocker filters */
  jack_default_audio_sample_t r[2], xn1[2], yn1[2];
  /* nrt things */
  FILE *fl;
  FILE *fr;
} cypi_jack;

/* OpenGL state */
static struct {
  /* frame count */
  int frame;
  unsigned int frameR;
  /* window */
  int winw, winh;
  float scale;
  /* frame buffer */
  GLuint fbo;
  /* texture */
  unsigned int tex;
  int texw, texh;
  /* shader */
  GLcharARB *src;
  GLhandleARB prog;
  GLhandleARB frag;
  GLcharARB *src2;
  GLhandleARB prog2;
  GLhandleARB frag2;
  /* uniforms */
  GLint number;
  GLint amount;
  GLfloat numberm[16];
  GLfloat amountv[4];
  GLint tex2;
  GLint fade2;
  /* nrt things */
  unsigned char *buffer;
  FILE *fv;
  int frame2;
  int frame3;
  int frameT;
  int frameC;
} cypi_gl;

/* synchronisation between JACK callback thread and OpenGL main thread */
static volatile int cypi_which = 0;

/* GLUT display callback */
static void cypi_display() {
  /* modulation amounts based on frame count */
  float z = 0.5 - 0.5 * cos(twopi * cypi_gl.frame / CYPI_SIZE);
  for (int i = 0; i < 4; ++i) {
    cypi_gl.amountv[i] = z;
  }
  /* set up view */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(-1, 1, -1, 1);       
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glViewport(0, 0, cypi_gl.texw, cypi_gl.texh);
  glClear(GL_COLOR_BUFFER_BIT);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, cypi_gl.fbo);
  /* generate a modulation texture */
  glFramebufferTexture2DEXT(
    GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, cypi_gl.tex, 0
  );
  glUseProgramObjectARB(cypi_gl.prog);
  glUniformMatrix4fvARB(cypi_gl.number, 1, 0, &cypi_gl.numberm[0]);
  glUniform4fvARB(cypi_gl.amount, 1, &cypi_gl.amountv[0]);
  glBegin(GL_QUADS); {
    glColor4f(1,1,1,1);
    glTexCoord4f(0, 0, 0, 0); glVertex2f(-1, -1);
    glTexCoord4f(1, 0, 1, 0); glVertex2f( 1, -1);
    glTexCoord4f(1, 1, 1, 1); glVertex2f( 1,  1);
    glTexCoord4f(0, 1, 0, 1); glVertex2f(-1,  1);
  } glEnd();
  glUseProgramObjectARB(0);
  /* read horizontal/vertical scanlines into JACk back buffers */
  glReadPixels(
    0, cypi_gl.frame % cypi_gl.texh, cypi_gl.texw, 1, GL_RED, GL_FLOAT,
    cypi_jack.buffer[0][1-cypi_which]
  );
  glReadPixels(
    cypi_gl.frame % cypi_gl.texw, 0, 1, cypi_gl.texh, GL_RED, GL_FLOAT,
    cypi_jack.buffer[1][1-cypi_which]
  );
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  /* fill the window tiled with the generated texture */
  glViewport(0, 0, cypi_gl.winw, cypi_gl.winh);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(-1, 1, -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glBindTexture(GL_TEXTURE_2D, cypi_gl.tex);
  glUseProgramObjectARB(cypi_gl.prog2);
  glUniform1iARB(cypi_gl.tex2, 0);
  float c = cypi_gl.frameR / (float) (CYPI_SIZE/2);
  c = fmin(fmax(c, 0.0), 1.0);
  float c2 = ((CYPI_COUNT+1) * CYPI_SIZE - cypi_gl.frameR) / (float) (CYPI_SIZE/2);
  c2 = fmin(fmax(c2, 0.0), 1.0);
  glUniform1fARB(cypi_gl.fade2, c * c2);
  glBegin(GL_QUADS); {
    float s = cypi_gl.scale;
    float tx0 = (1.0 * cypi_gl.texw - cypi_gl.winw / s) / (1.0 * cypi_gl.texw);
    float ty0 = (2.0 * cypi_gl.texh - cypi_gl.winh / s) / (2.0 * cypi_gl.texh);
    float tx1 = (1.0 * cypi_gl.texw + cypi_gl.winw / s) / (1.0 * cypi_gl.texw);
    float ty1 = (2.0 * cypi_gl.texh + cypi_gl.winh / s) / (2.0 * cypi_gl.texh);
    glTexCoord4f(tx0, ty0, 0, 0); glVertex2f(-1, -0.5);
    glTexCoord4f(tx1, ty0, 1, 0); glVertex2f( 1, -0.5);
    glTexCoord4f(tx1, ty1, 1, 1); glVertex2f( 1,  0.5);
    glTexCoord4f(tx0, ty1, 0, 1); glVertex2f(-1,  0.5);
  } glEnd();
  glUseProgramObjectARB(0);
  /* display */
  glutSwapBuffers();
  /* report errors */
  int status = glGetError();
  if(status) fprintf(stderr, "GL: error %08x\n", status);
  /* increment/wrap frame count and randomize modulators */
  if (++cypi_gl.frame == CYPI_SIZE) {
    cypi_gl.frame = 0;
    if (++cypi_gl.frameC > CYPI_COUNT) {
      for (int i = 0; i < 16; ++i) {
        cypi_gl.numberm[i] = 0;
      }
    } else {
      for (int i = 0; i < 16; ++i) {
        int r = rand() % 17;
        cypi_gl.numberm[i] = r - 8;
//        fputc("0123456789abcdefg"[r], stderr);
      }
//      fputc('\n', stderr);
    }
  }
  cypi_gl.frameR++;
  if (cypi_nrt) {
    /* save audio buffer */
    if (cypi_gl.frame2 == 0) {
      fwrite(cypi_jack.buffer[0][1-cypi_which], CYPI_SIZE * sizeof(float), 1, cypi_jack.fl);
      fwrite(cypi_jack.buffer[1][1-cypi_which], CYPI_SIZE * sizeof(float), 1, cypi_jack.fr);
    }
    /* save video buffer */
    if (cypi_gl.frame3 == 0) {
      glReadPixels(0, 0, cypi_gl.winw, cypi_gl.winh, GL_RGB, GL_UNSIGNED_BYTE, cypi_gl.buffer);
      const char *header = "P6\n1280 720 255\n";
      fwrite(header, strlen(header), 1, cypi_gl.fv);
      fwrite(cypi_gl.buffer, cypi_gl.winw * cypi_gl.winh * 3, 1, cypi_gl.fv);
    }
    cypi_gl.frame2 = (cypi_gl.frame2 + 1) % 2;
    cypi_gl.frame3 = (cypi_gl.frame3 + 1) % 3;
    cypi_gl.frameT++;
  }
  if (cypi_gl.frameR == (CYPI_COUNT+1) * CYPI_SIZE) exit(0);
}

/* GLUT reshape callback */
static void cypi_reshape(int w, int h) {
  cypi_gl.winw = w;
  cypi_gl.winh = h;
}

/* GLUT idle callback: render as fast as possible */
static void cypi_idle() {
  glutPostRedisplay();
}

/* JACK audio processing callback */
static int cypi_process(jack_nframes_t nframes, void *arg) {
  /* get JACK buffers */
  jack_default_audio_sample_t *out[2];
  out[0] = (jack_default_audio_sample_t *)
    jack_port_get_buffer(cypi_jack.port[0], nframes);
  out[1] = (jack_default_audio_sample_t *)
    jack_port_get_buffer(cypi_jack.port[1], nframes);
  /* copy buffers to JACK */
  for (int i = 0; i < nframes; ++i) {
    for (int c = 0; c < 2; ++c) {
      jack_default_audio_sample_t xn =
        cypi_jack.buffer[c][cypi_which][(cypi_jack.index + i) % CYPI_SIZE];
      cypi_jack.yn1[c] =
        xn - cypi_jack.xn1[c] + cypi_jack.r[c] * cypi_jack.yn1[c];
      cypi_jack.xn1[c] = xn;
      out[c][i] = cypi_jack.yn1[c];
    }
  }
  /* increment/wrap write counter, if done then swap buffers */
  cypi_jack.index = (cypi_jack.index + nframes) % CYPI_SIZE;
  if (cypi_jack.index == 0) {
    cypi_which = 1 - cypi_which;
  }
  return 0;
}

/* JACK sample rate callback: currently unused */
static int cypi_srate(jack_nframes_t nframes, void *arg) {
  return 0;
}

/* JACK error callback */
static void cypi_error(const char *desc) {
  fprintf(stderr, "JACK error: %s\n", desc);
}

/* JACK shutdown callback */
static void cypi_shutdown(void *arg) {
  exit(1);
}

/* exit callback */
static void cypi_atexit(void) {
  jack_client_close(cypi_jack.client);
}

/* main program */
int main(int argc, char **argv) {
  /* bad source of pseudorandomness */
  unsigned int t = time(NULL);
  printf("%08X\n", t);
  srand(t);
  /* the CyPi fragment shader source code */
  cypi_gl.src =
    "uniform mat4 number;\n"
    "uniform vec4 amount;\n"
    "const float twopi = 6.283185307179586;\n"
    "void main() {\n"
    "  vec4 p = gl_TexCoord[0];\n"
    "  vec4 q = number * p;\n"
    "  vec4 x = -cos(twopi * (p + amount * sin(twopi * q)*0.5+0.5))*0.5+0.5;\n"
    "  float z = x.x * x.y * x.z * x.w;\n"
    "  gl_FragColor = vec4(z,z,z,1.0);\n"
    "}\n";
  cypi_gl.src2 =
    "#version 130\n"
    "uniform sampler2D tex;\n"
    "uniform float fade;\n"
    "const float twopi = 6.283185307179586;\n"
    "void main() {\n"
    "  vec4 p = gl_TexCoord[0];\n"
    "  float a = tanh((clamp(1.0 - cos(p.z * twopi), 0.0, 1.0)) * 8.0);\n"
    "  float b = tanh((clamp(1.0 - cos(p.w * twopi), 0.0, 1.0)) * 8.0);\n"
    "  gl_FragColor = texture2D(tex, p.xy) * a * b * fade;\n"
    "}\n";
  /* set up OpenGL variables */
  cypi_gl.frame = CYPI_SIZE/2;
  cypi_gl.frameC = 0;
  cypi_gl.winw = 1280;
  cypi_gl.winh = 720;
  cypi_gl.scale = 1280.0 / 1920.0;
  cypi_gl.texw = CYPI_SIZE;
  cypi_gl.texh = CYPI_SIZE;
  /* initialize OpenGL */
  glutInitWindowSize(cypi_gl.winw, cypi_gl.winh);
  glutInit(&argc, argv);
  cypi_nrt = argc > 1;
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow("CyPi");
  glewInit();
  glClearColor(0.0, 0.0, 0.0, 1.0);
  glShadeModel(GL_FLAT);
  /* allocate frame buffer */
  glGenFramebuffersEXT(1, &cypi_gl.fbo);
  /* allocate texture */
  glGenTextures(1, &cypi_gl.tex);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, cypi_gl.tex);
  glTexImage2D(
    GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, cypi_gl.texw, cypi_gl.texh, 0, GL_RGBA,
    GL_FLOAT, 0
  );
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  /* compile shaders */
  GLint success;
  cypi_gl.prog = glCreateProgramObjectARB();
  cypi_gl.frag = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
  glShaderSourceARB(cypi_gl.frag, 1, (const GLcharARB **) &cypi_gl.src, 0);
  glCompileShaderARB(cypi_gl.frag);
  glAttachObjectARB(cypi_gl.prog, cypi_gl.frag);
  glLinkProgramARB(cypi_gl.prog);
  glGetObjectParameterivARB(cypi_gl.prog, GL_OBJECT_LINK_STATUS_ARB, &success);
  if (!success) exit(1);
  cypi_gl.prog2 = glCreateProgramObjectARB();
  cypi_gl.frag2 = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
  glShaderSourceARB(cypi_gl.frag2, 1, (const GLcharARB **) &cypi_gl.src2, 0);
  glCompileShaderARB(cypi_gl.frag2);
  glAttachObjectARB(cypi_gl.prog2, cypi_gl.frag2);
  glLinkProgramARB(cypi_gl.prog2);
  glGetObjectParameterivARB(cypi_gl.prog2, GL_OBJECT_LINK_STATUS_ARB, &success);
  if (!success) exit(1);
  /* access uniforms */
  cypi_gl.number = glGetUniformLocationARB(cypi_gl.prog, "number");
  cypi_gl.amount = glGetUniformLocationARB(cypi_gl.prog, "amount");
  cypi_gl.tex2   = glGetUniformLocationARB(cypi_gl.prog2, "tex");
  cypi_gl.fade2  = glGetUniformLocationARB(cypi_gl.prog2, "fade");
  /* set up callbacks */
  glutDisplayFunc(cypi_display);
  glutReshapeFunc(cypi_reshape);
  glutIdleFunc(cypi_idle);
  /* initialize DC blocking filters */
  cypi_jack.r[0] = 0.995;
  cypi_jack.xn1[0] = 0;
  cypi_jack.yn1[0] = 0;
  cypi_jack.r[1] = 0.995;
  cypi_jack.xn1[1] = 0;
  cypi_jack.yn1[1] = 0;
  if (cypi_nrt) {
    cypi_gl.fv   = fopen("video.ppm",   "wb");
    cypi_jack.fl = fopen("audio_l.raw", "wb");
    cypi_jack.fr = fopen("audio_r.raw", "wb");
    cypi_gl.buffer = malloc(cypi_gl.winw * cypi_gl.winh * 3);
    cypi_gl.frame2 = 0;
    cypi_gl.frame3 = 0;
    cypi_gl.frameT = 0;
  } else {
    cypi_gl.fv = 0;
    cypi_jack.fl = 0;
    cypi_jack.fr = 0;
    cypi_gl.buffer = 0;
    /* set up JACK */
    jack_set_error_function(cypi_error);
    if (!(cypi_jack.client = jack_client_open("cypi", 0, 0))) {
      fprintf (stderr, "jack server not running?\n");
      return 1;
    }
    atexit(cypi_atexit);
    jack_set_process_callback(cypi_jack.client, cypi_process, 0);
    jack_set_sample_rate_callback(cypi_jack.client, cypi_srate, 0);
    jack_on_shutdown(cypi_jack.client, cypi_shutdown, 0);
    /* create ports */
    cypi_jack.port[0] = jack_port_register(
      cypi_jack.client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
    );
    cypi_jack.port[1] = jack_port_register(
      cypi_jack.client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
    );
    /* activate audio */
    if (jack_activate(cypi_jack.client)) {
      fprintf (stderr, "cannot activate JACK client");
      return 1;
    }
    /* must be activated before connecting JACK ports */
    const char **ports;
    if ((ports = jack_get_ports(
      cypi_jack.client, NULL, NULL, JackPortIsPhysical | JackPortIsInput
    ))) {
      /* connect up to two physical playback ports */
      int i = 0;
      while (ports[i] && i < 2) {
        if (jack_connect(
          cypi_jack.client, jack_port_name(cypi_jack.port[i]), ports[i]
        )) {
          fprintf(stderr, "cannot connect output port\n");
        }
        i++;
      }
      free(ports);
    }
  }
  /* activate video */
  glutMainLoop();
  return 0; /* never reached */
}

/* EOF */

