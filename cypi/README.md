# cypi

audio-visual drone

<https://mathr.co.uk/cypi>

<https://code.mathr.co.uk/dr1>

    git clone https://code.mathr.co.uk/dr1.git

## live

view web version live in your browser at
<https://mathr.co.uk/cypi/live>

plays automatically in Firefox, click to play in Chromium

reload after changing window size for correct aspect ratio

## videos

<https://archive.org/details/ClaudiusMaximus_-_CyPi_4BBEDCAB>

<https://archive.org/details/ClaudiusMaximus_-_CyPi_4BD53C76>

## version 1

uses GLUT and OpenGL

    make && ./cypi

## version 2

uses SDL2 and OpenGL ES

    make cypi2 && ./cypi2

## version 2 for web

uses SDL2 and OpenGL ES via Emscripten

    make cypi.js && serve directory over http(s)

files to serve:

  - cypi.js
  - cypi.wasm
  - index.html

also optionally .gz versions if your server supports gzip

corresponding source code must be made available too

## legal

cypi -- audio-visual drone

Copyright (c) 2010,2018,2022 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

---
<https://mathr.co.uk>
