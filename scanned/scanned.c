/*
  frame 0: ;gain 266 ;scale 260
  frame 250: ;gain 256
  frame 1125: ;scale 256
  frame 2000: ;gain 246
  frame 2250: ;pd quit

  [loadbang]
   |
  "576 788 3 # 256"
   |
  [# rand]
   |
  [s feedback]

  [r framebang]
   |
   |   [r feedback]
   |    |
  [#store]
   |
  [t a a]
   |   |
   |  [s out]
   |
  [#convolve 1 2 # 1]
   |
  [# >> 1]   [r remapped]
   |          |
  [#remap_image]
   |          |
  [# - 128]  [s remap]
   |
   |   [r gain]
   |    |
  [# *>>8]
   |
  [# + 128]
   |
  [s feedback]

  [r remap]
   |
  [# - ( 2 # 288 360 )]
   |
  [#rotate 1600]
   |
   |   [r scale]
   |    |
  [# *>>8]
   |
  [# + ( 2 # 288 360 )]
   |
  [s remapped]

  [r out]
   |
  [# & 255]
   |
  [t a a]
   |   |
   |  [s tables]
   |
  [#out]
  
  [r tables]
  vertical lines mode 720 (increment x per frame)
  export list (scaled from 0..255 to -1..1) to 1 table per RGB channel
  
  audio:
  phasor~ 100 * 576 tabread4~ r pan (0.6666, 0.3333)
  phasor~ 120 * 576 tabread4~ g pan (0.5, 0.5)
  phasor~ 150 * 576 tabread4~ b pan (0.3333, 0.6666)
  *+ 0.5 hip~ 10 dac~
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <sndfile.h>

#ifndef M_PI
#define M_PI 3.141592653589793
#endif

int32_t store[2][576][788][3];

unsigned char ppm[576][788][3];

float wavetable[3][576];
double phase[3];

float audio[1920][2];
float hip[2];

int main(int argc, char **argv)
{
  // a/v parameters
  const int32_t FPS = 25;
  const int32_t SR = 48000;
  SF_INFO info = { 0, SR, 2, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
  SNDFILE *sfo = sf_open("scanned.wav", SFM_WRITE, &info);
  if (! sfo)
  {
    return 1;
  }
  // phasor increments
  const double inc[3] = { 100.0 / SR, 120.0 / SR, 150.0 / SR };
  // filter parameters
  const float h0 = 1 - 2 * M_PI * 10 / SR;
  const float h1 = (1 + h0) / 2;
  // mixing parameters
  const float pan[3][2] =
    { { 0.6666 * 0.5, 0.3333 * 0.5 }
    , { 0.5 * 0.5, 0.5 * 0.5 }
    , { 0.3333 * 0.5, 0.6666 * 0.5 }
    };
  // rotation
  const int32_t m00 = 65536 * cos(2 * M_PI * 1600 / 36000);
  const int32_t m01 = 65536 * -sin(2 * M_PI * 1600 / 36000);
  const int32_t m10 = -m01;
  const int32_t m11 = m00;
  // sequencables
  int32_t gain, scale;
  // initialize
  for (int32_t y = 0; y < 576; ++y)
  {
    for (int32_t x = 0; x < 788; ++x)
    {
      for (int32_t c = 0; c < 3; ++c)
      {
        store[0][y][x][c] = 256.0 * rand() / (double) RAND_MAX;
      }
    }
  }
  for (int32_t frame = 0; frame < 2250; ++frame)
  {
    const int32_t src = frame & 1;
    const int32_t dst = ! src;
    // sequencing
    switch (frame)
    {
      case    0: gain = 266; scale = 260; break;
      case  250: gain = 256; break;
      case 1125: scale = 256; break;
      case 2000: gain = 246; break;
    }
    // video
    for (int32_t y = 0; y < 576; ++y)
    {
      for (int32_t x = 0; x < 788; ++x)
      {
        const int32_t u = x - (720/2);
        const int32_t v = y - (576/2);
        const int32_t p = (((m00 * u)>>8) + ((m01 * v)>>8) + 128) >> 8;
        const int32_t q = (((m10 * u)>>8) + ((m11 * v)>>8) + 128) >> 8;
        const int32_t s = ((p * scale)>>8) + (720/2);
        const int32_t t = ((q * scale)>>8) + (576/2);
        const int32_t i = ((s % 788) + 788) % 788;
        const int32_t j = ((t % 576) + 576) % 576;
        const int32_t i1 = (i + 1) % 788;
        for (int32_t c = 0; c < 3; ++c)
        {
          const int32_t l = (store[src][j][i][c] + store[src][j][i1][c]) >> 1;
          const int32_t m = (((l - 128) * gain)>>8) + 128;
          store[dst][y][x][c] = m;
          ppm[y][x][c] = m & 255;
        }
      }
    }
    // audio
    const int32_t x = frame % 720;
    for (int32_t y = 0; y < 576; ++y)
    {
      for (int32_t c = 0; c < 3; ++c)
      {
        wavetable[c][y] = ((store[dst][y][x][c] & 255) / 255.0 - 0.5) * 2.0;
      }
    }
    for (int32_t t = 0; t < 1920; ++t)
    {
      float out[2] = { 0, 0 };
      for (int32_t z = 0; z < 3; ++z)
      {
        phase[z] += inc[z];
        phase[z] -= floor(phase[z]);
        const float p = phase[z] * 576;
        const int32_t i = p;
        const float t = p - i;
        const float u = wavetable[z][i];
        const int32_t i1 = i + 1 == 576 ? 0 : i + 1;
        const float v = wavetable[z][i1];
        const float w = u + t * (v - u);
        out[0] += pan[z][0] * w;
        out[1] += pan[z][1] * w;
      }
      for (int32_t c = 0; c < 2; ++c)
      {
        const float y = out[c] + h0 * hip[c];
        audio[t][c] = h1 * (y - hip[c]);
        hip[c] = y;
      }
    }
    sf_writef_float(sfo, &audio[0][0], 1920);
    printf("P6\n788 576\n255\n");
    fwrite(ppm, 576 * 788 * 3, 1, stdout);
    fflush(stdout);
  }
  sf_close(sfo);
  return 0;
}
