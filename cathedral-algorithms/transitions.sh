#!/bin/bash
(
  echo 'digraph G {'
  echo '  node[label="",shape="circle",style="filled"];'
  edges=$(ls -1 out | grep ".txt$" | sed "s|.txt$||")
  nodes=$(for edge in ${edges} ; do echo ${edge} ; done | sed "s|_-_.*$||" | sort | uniq)
  for node in ${nodes}
  do
    colour=$(echo ${node} | sed "s|_.*$||")
    case ${colour} in
      0)
        echo "node[fillcolor=\"green\"]; node_${node};"
        ;;
      1)
        echo "node[fillcolor=\"white\"]; node_${node};"
        ;;
      2)
        echo "node[fillcolor=\"orange\"]; node_${node};"
        ;;
    esac
  done
  for edge in ${edges}
  do
    source=$(echo ${edge} | sed "s|_-_.*$||")
    sink=$(echo ${edge} | sed "s|^.*_-_||")
    echo "node_${source} -> node_${sink};"
  done
  echo '}'
) > transitions.dot
circo -Tpng < transitions.dot > transitions.png
