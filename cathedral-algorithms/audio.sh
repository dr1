#!/bin/bash
STEMS=$(ls -1 out | grep ".txt$" | sed "s|.txt$||")
SOURCES=$(for STEM in ${STEMS} ; do echo ${STEM} ; done | sed "s|_-_.*||" | sort | uniq)
for SOURCE in ${SOURCES}
do
  SINKS=$(for STEM in ${STEMS} ; do echo ${STEM} ; done | grep "^${SOURCE}_-_")
  WARMUP=$(for STEM in ${STEMS} ; do echo ${STEM} ; done | grep "_-_${SOURCE}$" | head -n 1)
  for SINK in ${SINKS}
  do
    pd -noprefs -noaudio -nogui -stderr -r 48000 -open "cathedral.pd" -send "SETUP out/${WARMUP}.txt out/${SINK}.txt out/${SINK}.wav"
  done
done
