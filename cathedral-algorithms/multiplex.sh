#!/bin/bash
for M2V in $@
do
  MP2="${M2V%m2v}mp2"
  MPEG="${M2V%m2v}mpeg"
  mplex -f 8 -V -o "${MPEG}" "${M2V}" "${MP2}"
done
