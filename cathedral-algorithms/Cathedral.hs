module Main(main) where

import Graphics.UI.GLUT hiding (Green, rotate, Render)
import Data.IORef
import Data.List(sortBy)
import Data.Ord(comparing)
import Control.Monad(liftM, when)
import System.Random(getStdGen, randoms)
import Data.Time(UTCTime(UTCTime), fromGregorian, secondsToDiffTime, getCurrentTime, diffUTCTime)
import System.IO(withBinaryFile, Handle(), IOMode(ReadMode, WriteMode), openBinaryFile, hClose, hPutStrLn, hGetBuf)
import System.FilePath((</>))
import System.Exit(exitFailure, exitSuccess)
import Foreign.Marshal.Alloc(allocaBytes)

import Snapshot

record :: Bool
record = True

segments :: Int
segments = 2

winWidth :: GLsizei
winWidth  = 1044

winHeight :: GLsizei
winHeight =  336

count :: Int
count = 25

chunk :: Int -> [a] -> [[a]]
chunk _ [] = []
chunk n ls = let (xs, ys) = splitAt n ls in xs : chunk n ys

data Cell = Green | White | Orange
  deriving (Read, Show, Eq, Ord, Enum, Bounded)

child :: (Cell, Bool) -> ((Cell, Bool), (Cell, Bool))
child (Green,  b) = if b then ((Orange, not b), (White,  not b))
                         else ((White,  not b), (Orange, not b))
child (White,  b) = if b then ((Green,  not b), (Orange, not b))
                         else ((Orange, not b), (Green,  not b))
child (Orange, b) = if b then ((White,  not b), (Green,  not b))
                         else ((Green,  not b), (White,  not b))

rotate :: (Cell, Bool) -> (Cell, Bool)
rotate t@(_, b) = let (c, _) = fst (child t) in (c, b)

fromBits :: [Bool] -> GLdouble
fromBits = foldr (\b s -> (if b then 0.5 else 0) + 0.5 * s) 0

data World = World
  { wTop :: (Cell, Bool)
  , wBits :: [Bool]
  , wDelta :: GLdouble
  }

world0 :: World
world0 = World
  { wTop = (Green, False)
  , wBits = repeat False
  , wDelta = 0
  }

data Render = Render
  { rPlan :: [(World, FilePath)]
  , rVideo :: Maybe Handle
  , rScore :: Maybe Handle
  , rCorners :: (GLdouble, GLdouble, GLdouble, GLdouble)
  , rTex :: Cell -> TextureObject
  , rFrames :: Int
  , rTotal :: Int
  , rNow :: GLdouble
  , rTime :: GLdouble
  }

render0 :: Render
render0 = Render
  { rPlan = []
  , rVideo = Nothing
  , rScore = Nothing
  , rCorners = (0, 0, 0, 0)
  , rTex = error "rTex"
  , rFrames = 0
  , rTotal = 0
  , rNow = 0
  , rTime = 0
  }

reshape :: IORef Render -> Size -> IO ()
reshape renderRef vp@(Size w h) = do
  let cs@(x0,x1,y0,y1) = let v = fromIntegral w / fromIntegral h
                         in (-v/8, v/8, 0, 0.25)
  writeIORef renderRef. (\rr -> rr{ rCorners = cs }) =<< readIORef renderRef
  viewport $= (Position 0 0, vp)
  matrixMode $= Projection
  loadIdentity
  ortho x0 x1 y0 y1 (-1) 1
  matrixMode $= Modelview 0
  loadIdentity
  postRedisplay Nothing

start :: IORef Render -> IO ()
start renderRef = do
  rr <- readIORef renderRef
  when (null (rPlan rr)) exitSuccess
  let (_, nn) = head (rPlan rr)
  case (rVideo rr, rScore rr) of
    (Nothing, Nothing) -> do
      hv <- openBinaryFile ("out" </> nn ++ ".ppm") WriteMode
      hs <- openBinaryFile ("out" </> nn ++ ".txt") WriteMode
      writeIORef renderRef rr
        { rVideo = Just hv
        , rScore = Just hs
        , rFrames = 0
        , rTotal = 0
        , rNow = 0
        , rTime = 0
        }
    _ -> error "start"

stop :: IORef Render -> IO ()
stop renderRef = do
  rr <- readIORef renderRef
  when (not . null . rPlan $ rr) $ do
    case (rVideo rr, rScore rr) of
      (Just hv, Just hs) -> do
        hClose hv
        hClose hs
        writeIORef renderRef rr
          { rPlan  = tail (rPlan rr)
          , rVideo = Nothing
          , rScore = Nothing
          }
      _ -> error "stop"

step' :: World -> World
step' ww = ww
  { wTop = (if head (wBits ww) then snd else rotate . fst) (child (wTop ww))
  , wBits = tail (wBits ww)
  , wDelta = fromBits . take 64 . tail . wBits $ ww
  }

step :: IORef Render -> IO ()
step renderRef = do
  rr <- readIORef renderRef
  let (ww, nn):pp = rPlan rr
      rr' = if rFrames rr + 1 == count
            then rr{ rPlan = (step' ww, nn) : pp
                   , rFrames = 0
                   , rTime = 0
                   , rTotal = rTotal rr + 1
                   }
            else rr{ rFrames = rFrames rr + 1
                   , rTime = fromIntegral (rFrames rr + 1) / fromIntegral count
                   }
  writeIORef renderRef rr'
  when (rTotal rr' == 60) $ do
    stop renderRef
    start renderRef

simulateNRT :: Int -> IORef Render -> IO ()
simulateNRT mspf renderRef = do
  step renderRef
  postRedisplay Nothing
  addTimerCallback mspf $ simulateNRT mspf renderRef

simulateRT :: Int -> IORef Render -> IO ()
simulateRT = simulateNRT {- mspf renderRef = do
  rr <- readIORef renderRef
  tnow <- utcr
  let t = tnow - rNow rr
  when (t >= 1) $ step renderRef
  rr' <- readIORef renderRef
  writeIORef renderRef rr'{ rTime = tnow - rNow rr' }
  postRedisplay Nothing
  addTimerCallback mspf $ simulateRT mspf renderRef
-}

display :: IORef Render -> IO ()
display renderRef = do
  r <- readIORef renderRef
  let w = fst . head . rPlan $ r
  d <- if record then return (rTime r) else subtract (rNow r) `liftM` utcr
  let t = wTop w
      delta = wDelta w
      ds = 2 ** d
      x0 = (delta - 2) * ds
      x1 = (delta + 2) * ds
      y0 =  0
      y1 =  4 * ds
      (cx0, cx1, cy0, cy1) = rCorners r
  clear [ColorBuffer]
  blend $= Enabled
  blendFunc $= (SrcAlpha, OneMinusSrcAlpha)
  texture Texture2D $= Enabled
  drawTiles (rTex r) cx0 cy0 cx1 cy1 x0 y0 x1 y1 t
  textureBinding Texture2D $= Nothing
  texture Texture2D $= Disabled
  swapBuffers
  when record $ do
    let Just hv = rVideo r
        Just hs = rScore r
    hSnapshot hv (Position 0 0) (Size winWidth winHeight)
    when (rFrames r == 0) $ do
      let scorePart (x, c) = "new " ++ show x ++ " " ++ show (fromEnum c) ++ ", "
          s = concatMap scorePart . take 9 . sortBy (comparing (abs . fst)) . getTileRow x0 y0 x1 y1 t $ 8
      hPutStrLn hs (s ++ "done;")

getTileRow :: GLdouble -> GLdouble -> GLdouble -> GLdouble -> (Cell, Bool) -> Int -> [(GLdouble, Cell)]
getTileRow x0 y0 x1 y1 t n | n > 0 = let x2 = (x0 + x1) / 2
                                         y2 = (y0 + y1) / 2
                                         (l, r) = child t
                                         m = n - 1
                                     in getTileRow x0 y0 x2 y2 l m ++
                                        getTileRow x2 y0 x1 y2 r m
                           | otherwise = let x = (x0 + x1) / 2
                                             (c, _) = t
                                         in [(x,c)]

drawTiles :: (Cell -> TextureObject) -> GLdouble -> GLdouble -> GLdouble -> GLdouble -> GLdouble -> GLdouble -> GLdouble -> GLdouble -> (Cell, Bool) -> IO ()
drawTiles tex cx0 cy0 cx1 cy1 x0 y0 x1 y1 t@(c, _) = do
  let outside = or [ x1 < cx0, cx1 < x0, y1 < cy0, cy1 < y0 ]
  when (not outside) $ do
    textureBinding Texture2D $= Just (tex c)
    renderPrimitive Quads $ do
      let tc, v :: GLdouble -> GLdouble -> IO ()
          tc x y = texCoord $ TexCoord2 x y
          v  x y = vertex   $ Vertex2   x y
      color $ Color3 1 1 (1::GLdouble)
      tc 0 1 >> v x0 y0
      tc 0 0 >> v x0 y1
      tc 1 0 >> v x1 y1
      tc 1 1 >> v x1 y0
    let small = abs (((x1 - x0) * (y1 - y0)) / ((cx1 - cx0) * (cy1 - cy0))) < 0.00001
    when (not small) $ do
      let x2 = (x0 + x1) / 2
          y2 = (y0 + y1) / 2
          (l, r) = child t
      drawTiles tex cx0 cy0 cx1 cy1 x0 y0 x2 y2 l
      drawTiles tex cx0 cy0 cx1 cy1 x2 y0 x1 y2 r

textures :: TextureObject -> TextureObject -> TextureObject -> Cell -> TextureObject
textures g _ _ Green  = g
textures _ w _ White  = w
textures _ _ o Orange = o

main :: IO ()
main = do
  let w = winWidth
      h = winHeight
      v = fromIntegral w / fromIntegral h
      cs = (-v/8, v/8, 0, 0.25)
      mspf = 1000 `div` count
  initialWindowSize $= Size w h
  initialDisplayMode $= [RGBAMode, DoubleBuffered]
  _ <- getArgsAndInitialize
  _ <- createWindow "Cathedral Algorithms"
  green  <- loadTexture "green.rgba"
  white  <- loadTexture "white.rgba"
  orange <- loadTexture "orange.rgba"
  g <- getStdGen
  tnow <- utcr
  let bits = randoms g
      numbers = take segments (chunk 60 bits)
      plan =
        [ (world, name)
        | c1 <- [ Green, White, Orange]
        , n1 <- numbers
        , n2 <- numbers
        , let n = n1 ++ n2
        , let world = world0
                       { wTop = (c1, False)
                       , wBits = cycle n
                       , wDelta = fromBits . take 64 . cycle $ n
                       }
        , let c2 = fst . wTop . (!! 60) . iterate step' $ world
        , let name = show (fromEnum c1) ++ "_" ++ map bit n1 ++ "_-_"
                  ++ show (fromEnum c2) ++ "_" ++ map bit n2
        ]
      bit b = if b then '1' else '0'
  renderRef <- newIORef render0
    { rPlan = plan
    , rTex = textures green white orange
    , rCorners = cs
    , rNow = tnow
    }
  start renderRef
  displayCallback $= display renderRef
  reshapeCallback $= Just (reshape renderRef)
  addTimerCallback mspf $ (if record then simulateNRT else simulateRT) mspf renderRef
  mainLoop

loadTexture :: FilePath -> IO TextureObject
loadTexture f = do
  withBinaryFile f ReadMode $ \h -> do
    let bytes = 1024 * 1024 * 4
    allocaBytes bytes $ \pixels -> do
      bytes' <- hGetBuf h pixels bytes
      when (bytes' /= bytes) exitFailure
      [tex] <- genObjectNames 1
      texture Texture2D $= Enabled
      textureBinding Texture2D $= Just tex
      build2DMipmaps Texture2D RGBA' 1024 1024
        (PixelData RGBA UnsignedByte pixels)
      textureWrapMode Texture2D S $= (Repeated, ClampToEdge)
      textureWrapMode Texture2D T $= (Repeated, ClampToEdge)
      textureBinding Texture2D $= Nothing
      texture Texture2D $= Disabled
      return tex

-- these two functions copied from hosc/Sound.OpenSoundControl.Time
utc_base :: UTCTime
utc_base = UTCTime (fromGregorian 1970 1 1) (secondsToDiffTime 0)
utcr :: IO GLdouble
utcr = do t <- getCurrentTime ; return (realToFrac (diffUTCTime t utc_base))
