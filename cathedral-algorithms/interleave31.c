#include <stdio.h>

int main(int argc, char **argv) {
  if (argc != 3) {
    return 1;
  }
  FILE *in3 = fopen(argv[1], "rb");
  if (in3) {
    FILE *in1 = fopen(argv[2], "rb");
    if (in1) {
      for (int i = 0; i < 1024*1024; ++i) {
        putchar(getc(in3));
        putchar(getc(in3));
        putchar(getc(in3));
        putchar(getc(in1));
      }
      fclose(in1);
    } else {
      return 1;
    }
    fclose(in3);
  } else {
    return 1;
  }
  return 0;
}
