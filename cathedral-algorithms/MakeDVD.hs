import System.Environment(getArgs)
import Data.Ord(comparing)
import Data.List(sortBy)
import Data.Map((!))
import qualified Data.Map as M
import qualified Data.Set as S

default(Int)

dvd :: [String] -> String
dvd mpegs =
  unlines
    [ "<dvdauthor jumppad=\"no\"><vmgm>"
    , "<fpc>{ g1 = random(" ++ show (M.size nodes) ++ "); jump titleset 1 menu entry root; }</fpc>"
    , "</vmgm><titleset><menus>"
    , unlines . map menu . sortBy (comparing snd) . M.toList $ nodes
    , rootMenu (M.size nodes)
    , "</menus><titles>"
    , unlines . map pgc  . sortBy (comparing snd) . M.toList $ trans
    , "</titles></titleset></dvdauthor>"
    ]
  where
    mpeg f t = f ++ "_-_" ++ t ++ ".mpeg"
    froms  = map (take 62          ) mpegs
    tos    = map (take 62 . drop 65) mpegs
    edges  = zip froms tos
    source = M.fromList (zip mpegs froms)
    sink   = M.fromList (zip mpegs tos)
    nodes  = M.fromList $ zip (S.toList (S.fromList (M.elems source) `S.union` S.fromList (M.elems sink))) [1..]
    trans  = M.fromList $ zip mpegs [1..]
    pgc (vob, n) =
      unlines
        [ "<pgc><!-- " ++ show n ++ " -->"
        , "<pre>{ g1 = " ++ show (nodes ! (sink ! vob)) ++ "; }</pre>"
        , "<vob file=\"" ++ vob ++ "\" />"
        , "<post>call menu entry root;</post>"
        , "</pgc>"
        ]
    menu (from, n) =
      unlines
        [ "<pgc><!-- " ++ show n ++ " -->"
        , "<pre>{ "
        , "g0 = random(" ++ show m ++ ");"
        , switch "g0" m [1 .. m] (map snd to) $ \t ->
            "jump title " ++ show (trans ! mpeg from t) ++ ";" 
        , " }</pre><vob file=\"menu.mpeg\" /></pgc>"
        ]
      where
        m = length to
        to = filter ((from ==) . fst) edges
    rootMenu n =
      unlines
        [ "<pgc entry=\"root\">"
        , "<pre>{ "
        , switch "g1" n [1 .. n] [1 .. n] $ \t ->
            "jump menu " ++ show t ++ ";"
        , " }</pre><vob file=\"menu.mpeg\" /></pgc>"
        ]
    switch r k xs ts f
      | k >  1 = unlines
                   [ "if (" ++ r ++ " lt " ++ show (head xs2) ++ ") {"
                   , switch r k2 xs1 ts1 f
                   , "} else {"
                   , switch r (k - k2) xs2 ts2 f
                   , "}"
                   ]
      | k == 1 = f (head ts)
      | otherwise = error "empty switch"
      where
        k2 = k `div` 2
        (xs1, xs2) = splitAt k2 xs
        (ts1, ts2) = splitAt k2 ts

main :: IO ()
main = do
  mpegs <- getArgs
  putStrLn $ dvd mpegs
