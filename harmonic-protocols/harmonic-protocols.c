/*
Harmonic Protocols (AGPL3+) 2019,2020,2022 Claude Heiland-Allen <claude@mathr.co.uk>
License: https://www.gnu.org/licenses/agpl-3.0.en.html
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_opengl.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#else
#include <unistd.h>
#include <sndfile.h>
#endif

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

typedef float sample;
static float SR = 48000;
static int TET = 5;
#define MAXTET 128
static sample MATRIX[MAXTET][MAXTET][4];
static int NOTES = (72 - 24) / 12 * 12;
#define MAXNOTES 512
#define pi 3.141592653589793
#define twopi (2 * pi)
#define FPS 60
#define CELLSIZE 48

static inline sample random_matrix(void)
{
  return rand() / (double) RAND_MAX < 2.0 / TET;
}

static inline sample clamp(sample x, sample lo, sample hi) {
  return fminf(fmaxf(x, lo), hi);
}

static inline sample mix(sample x, sample y, sample t) {
  return (1 - t) * x + t * y;
}

static inline sample noise() {
  return 2 * (rand() / (sample) RAND_MAX - (sample)0.5);
}

static inline sample mtof(sample f) {
  return (sample)8.17579891564 * expf((sample)0.0577622650 * fminf(f, 1499));
}

#define log10overten 0.23025850929940458
#define tenoverlog10 4.3429448190325175

static inline sample dbtorms(sample f) {
  return expf((sample)0.5 * (sample)log10overten * (fminf(f, 870) - 100));
}

static inline sample rmstodb(sample f) {
  return 100 + 2 * (sample)tenoverlog10 * logf(f);
}

// based on pd's [lop~] [hip~]

typedef struct { sample y; } LOP;

static inline sample lop(LOP *s, sample x, sample hz) {
  sample c = clamp(twopi * hz / SR, 0, 1);
  return s->y = mix(x, s->y, 1 - c);
}

typedef struct { sample y; } HIP;

static inline sample hip(HIP *s, sample x, sample hz) {
  sample c = clamp(1 - twopi * hz / SR, 0, 1);
  sample n = (1 + c) / 2;
  sample y = x + c * s->y;
  sample o = n * (y - s->y);
  s->y = y;
  return o;
}

// one-pole one-zero low pass filter designed in the Z plane

typedef struct {
  sample a;
  sample b;
  sample x;
  sample y;
} LOP1;

static inline LOP1 *lop1_(LOP1 *s, sample hz)
{
  const sample w = twopi * clamp(fabsf(hz / SR), 0, (sample)0.5);
  s->a = (1 - sinf(w)) / cosf(w);
  s->b = (1 - s->a) / 2;
  return s;
}

static inline sample lop1(LOP1 *s, sample x)
{
  const sample y = s->b * (x + s->x) + s->a * s->y;
  s->x = x;
  s->y = y;
  return y;
}

typedef struct { sample b0, b1, b2, a1, a2, dummy_for_alignment[3]; sample y1[2], y2[2]; sample x1[2], x2[2]; } BIQUADS;

static inline void biquads(sample out[2], BIQUADS *bq, sample in[2]) {
  const sample b0 = bq->b0, b1 = bq->b1, b2 = bq->b2, a1 = bq->a1, a2 = bq->a2;
  const sample
      x00 = in[0], x01 = in[1]
    , y10 = bq->y1[0], y11 = bq->y1[1]
    , y20 = bq->y2[0], y21 = bq->y2[1]
    , x10 = bq->x1[0], x11 = bq->x1[1]
    , x20 = bq->x2[0], x21 = bq->x2[1]
    ;
  const sample y00 = b0 * x00 + b1 * x10 + b2 * x20 - a1 * y10 - a2 * y20;
  const sample y01 = b0 * x01 + b1 * x11 + b2 * x21 - a1 * y11 - a2 * y21;
  bq->y1[0] = y00;
  bq->y1[1] = y01;
  bq->y2[0] = y10;
  bq->y2[1] = y11;
  bq->x1[0] = x00;
  bq->x1[1] = x01;
  bq->x2[0] = x10;
  bq->x2[1] = x11;
  out[0] = y00;
  out[1] = y01;
}

static inline BIQUADS *bandpass(BIQUADS *bq, sample hz, sample q) {
  sample w0 = hz * twopi / SR;
  sample a = fabsf(sinf(w0) / (2 * q));
  sample c = cosf(w0);
  sample b0 = a, b1 = 0, b2 = -a;
  sample a0 = 1 + a, a1 = -2 * c, a2 = 1 - a;
  bq->b0 = b0 / a0;
  bq->b1 = b1 / a0;
  bq->b2 = b2 / a0;
  bq->a1 = a1 / a0;
  bq->a2 = a2 / a0;
  return bq;
}

typedef struct { HIP hip[2]; LOP lop[3]; } COMPRESS;

static inline void compress(sample out[2], COMPRESS *s, sample hiphz, sample lophz1, sample lophz2, sample db, const sample in[2]) {
  sample h[2] =
    { hip(&s->hip[0], in[0], hiphz)
    , hip(&s->hip[1], in[1], hiphz)
    };
  h[0] *= h[0];
  h[1] *= h[1];
  h[0] = lop(&s->lop[0], h[0], lophz1);
  h[1] = lop(&s->lop[1], h[1], lophz1);
  sample env = lop(&s->lop[2], sqrtf(fmaxf(0, h[0] + h[1])), lophz2);
  sample env0 = env;
  env = rmstodb(env);
  if (env > db) {
    env = db + (env - db) / 4;
  } else {
    env = db;
  }
  env = (sample)0.25 * dbtorms(env) / dbtorms((100 - db) / 4 + db);
  sample gain = env / env0;
  if (isnan(gain) || isinf(gain)) { gain = 0; }
  gain = clamp(gain, 0, 0.5);
  out[0] = tanhf(in[0] * gain);
  out[1] = tanhf(in[1] * gain);
}

// https://en.wikipedia.org/wiki/Cubic_Hermite_spline#Interpolation_on_the_unit_interval_without_exact_derivatives

typedef struct { int length, woffset; } DELAY;

static void delwrite(DELAY *del, sample x0) {
  float *buffer = (float *) (del + 1);
  int l = del->length;
  l = (l > 0) ? l : 1;
  int w = del->woffset;
  buffer[w++] = x0;
  if (w >= l) { w -= l; }
  del->woffset = w;
}

static sample delread1(DELAY *del, sample ms) {
  float *buffer = (float *) (del + 1);
  int l = del->length;
  l = (l > 0) ? l : 1;
  int w = del->woffset;
  sample d = ms / (sample) 1000 * SR;
  int d1 = floorf(d);
  int r1 = w - d1;
  r1 = r1 < 0 ? r1 + l : r1;
  return buffer[r1];
}

static sample delread4(DELAY *del, sample ms) {
  float *buffer = (float *) (del + 1);
  int l = del->length;
  l = (l > 0) ? l : 1;
  int w = del->woffset;
  sample d = ms / (sample) 1000 * SR;
  int d1 = floorf(d);
  int d0 = d1 - 1;
  int d2 = d1 + 1;
  int d3 = d1 + 2;
  sample t = d - d1;
  d0 = (0 < d0 && d0 < l) ? d0 : 0;
  d1 = (0 < d1 && d1 < l) ? d1 : d0;
  d2 = (0 < d2 && d2 < l) ? d2 : d1;
  d3 = (0 < d3 && d3 < l) ? d3 : d2;
  int r0 = w - d0;
  int r1 = w - d1;
  int r2 = w - d2;
  int r3 = w - d3;
  r0 = r0 < 0 ? r0 + l : r0;
  r1 = r1 < 0 ? r1 + l : r1;
  r2 = r2 < 0 ? r2 + l : r2;
  r3 = r3 < 0 ? r3 + l : r3;
  sample y0 = buffer[r0];
  sample y1 = buffer[r1];
  sample y2 = buffer[r2];
  sample y3 = buffer[r3];
  sample a0 = -t*t*t + 2*t*t - t;
  sample a1 = 3*t*t*t - 5*t*t + 2;
  sample a2 = -3*t*t*t + 4*t*t + t;
  sample a3 = t*t*t - t*t;
  return (a0 * y0 + a1 * y1 + a2 * y2 + a3 * y3) / 2;
}


typedef struct {
  int reloaded;
  float dummy_for_alignment[15];
  BIQUADS hp_band[MAXNOTES];
  LOP1 hp_rms[MAXNOTES][2];
  DELAY hp_del1; float hp_del1buf[192000];
  DELAY hp_del2; float hp_del2buf[192000];
  sample hp_filtered[MAXNOTES][2];
  COMPRESS hp_compress;
  SDL_Window* window;
  SDL_GLContext context;
  sample band[MAXTET][2];
} S;

static S state;

static int go(S *s, float *out) {
  sample hz1 = 9;
  sample hz2 = hz1 / 32;
  sample inc = hz1 / SR;

  if (s->reloaded) {
    s->hp_del1.length = 192000;
    s->hp_del2.length = 192000;
    NOTES = (72 - 24) / 12 * TET;
    sample f1 = 1.0;
    sample f2 = powf(2, 1.0 / TET);
    sample Q = sqrtf(f2 * f1) / (f2 - f1);
    for (int note = 0; note < NOTES; ++note)
    {
      sample f = mtof(24 + note * 12.0 / TET);
      bandpass(&s->hp_band[note], f, Q);
      for (int c = 0; c < 2; ++c)
      {
        lop1_(&s->hp_rms[note][c], hz2);
      }
    }
    s->reloaded = 0;
  }
  {
    sample band[TET][2];
    memset(band, 0, sizeof(band));
    sample input[2] =
      { delread1(&s->hp_del1, 1000./hz1) + (sample)1e-8 * noise()
      , delread1(&s->hp_del2, 1000./hz1) + (sample)1e-8 * noise()
      };
    for (int note = 0; note < NOTES; ++note)
    {
      sample bs[2];
      biquads(bs, &s->hp_band[note], input);
      for (int c = 0; c < 2; ++c)
      {
        sample b = bs[c];
        s->hp_filtered[note][c] = b;
        band[note % TET][c] += lop1(&s->hp_rms[note][c], b * b);
      }
    }
    sample ma = 0;
    for (int i = 0; i < TET; ++i)
    {
      for (int c = 0; c < 2; ++c)
      {
        band[i][c] = sqrtf(band[i][c]);
        ma = fmaxf(band[i][c], ma);
      }
    }
    ma /= 8;
    for (int i = 0; i < TET; ++i)
    {
      for (int c = 0; c < 2; ++c)
      {
        s->band[i][c] = band[i][c] / ma;
      }
    }
    sample gain[TET][2];
    memset(gain, 0, sizeof(gain));
    for (int note = 0; note < TET; ++note)
    {
      for (int other = 0; other < TET; ++other)
      {
        MATRIX[note][other][3] = MATRIX[note][other][0] + (MATRIX[note][other][1] - MATRIX[note][other][0]) * MATRIX[note][other][2];
        MATRIX[note][other][2] += inc;
        if (MATRIX[note][other][2] >= 1)
        {
          MATRIX[note][other][0] = MATRIX[note][other][1];
          MATRIX[note][other][2] -= 1;
        }
        for (int c = 0; c < 2; ++c)
        {
          gain[note][c] += MATRIX[note][other][3] * s->band[other][c];
        }
      }
    }
    sample output[2] = { 0, 0 };
    for (int note = 0; note < NOTES; ++note)
    {
      for (int c = 0; c < 2; ++c)
      {
        sample x = gain[note % TET][c] * s->hp_filtered[note][c];
        // <https://www.musicdsp.org/en/latest/Other/238-rational-tanh-approximation.html>
#if 0
        x = tanh(x);
#else
        x = clamp(x, -3, 3);
        x = x * ( 27 + x * x ) / ( 27 + 9 * x * x );
#endif
        output[c] += x;
      }
    }
    sample o[2] = { output[0], output[1] };
    sample a = twopi / 12;
    sample co = cosf(a);
    sample si = sinf(a);
    output[0] = (co * o[0] - si * o[1]);
    output[1] = (si * o[0] + co * o[1]);
    compress(output, &s->hp_compress, 5, 10, 25, 48, output);
    delwrite(&s->hp_del1, output[0]);
    delwrite(&s->hp_del2, output[1]);
    out[0] = output[0];
    out[1] = output[1];
  }
  return 0;
}

static void audio(void *userdata, Uint8 *stream, int len)
{
  (void) userdata;
  float *b = (float *) stream;
  int m = len / sizeof(float) / 2;
  int k = 0;
  for (int i = 0; i < m; ++i)
  {
    float out[2];
    go(&state, out);
    b[k++] = out[0];
    b[k++] = out[1];
  }
}

static bool interact(void)
{
  bool running = true;
  SDL_Event event;
  while (SDL_PollEvent(&event) != 0)
  {
    switch (event.type)
    {
      case SDL_QUIT:
      {
        running = false;
        break;
      }
    }
  }
  int x = -1, y = -1;
  Uint32 b = SDL_GetMouseState(&x, &y);
  int from = x / CELLSIZE - 1;
  int to = TET - y / CELLSIZE;
  double delta = 2 * (0.5 - ((y % CELLSIZE) + 0.5) / CELLSIZE);
  if ( (b & SDL_BUTTON(SDL_BUTTON_LEFT)) &&
       0 <= from && from < TET &&
       0 <= to && to < TET &&
      -1 <= delta && delta <= 1
     )
  {
    double r = MATRIX[from][to][1];
    r += delta / FPS;
    r = fmin(fmax(r, 0), 1);
    MATRIX[from][to][0] = MATRIX[from][to][3];
    MATRIX[from][to][1] = r;
    MATRIX[from][to][2] = 0;
  }
  return running;
}

static void video1(void)
{
  glClearColor(1, 1, 1, 1);
  glClear(GL_COLOR_BUFFER_BIT);
  glBegin(GL_QUADS);
  {
    for (int i = -1; i <= TET; ++i)
    {
      for (int j = -1; j <= TET; ++j)
      {
        double r = 0;
        if (i == -1 || i == TET)
        {
          if (j == -1 || j == TET)
          {
            // nop
          }
          else
          {
            r = pow((state.band[j][0] + state.band[j][1]) / 16, 4);
          }
        }
        else if (j == -1 || j == TET)
        {
          r = pow((state.band[i][0] + state.band[i][1]) / 16, 4);
        }
        else
        {
          r = MATRIX[i][j][1];
        }
        double radius = fmin(fmax(sqrt(r) / 2.0, 3.0 / CELLSIZE), 0.5);
        const int sx[4] = { -1, -1, 1, 1 };
        const int sy[4] = { -1, 1, 1, -1 };
        for (int k = 0; k < 4; ++k)
        {
          glColor4f(i == -1 || i == TET || j == -1 || j == TET, 0, 0, 1);
          glVertex4f
            ( ((i + 1.5 + sx[k] * radius) / (TET + 2) - 0.5) * 2.0
            , ((j + 1.5 + sy[k] * radius) / (TET + 2) - 0.5) * 2.0
            , 0
            , 1
            );
        }
        radius = 1.0 / CELLSIZE;
        for (int k = 0; k < 4; ++k)
        {
          glColor4f(1, 1, 1, 1);
          glVertex4f
            ( ((i + 1.5 + sx[k] * radius) / (TET + 2) - 0.5) * 2.0
            , ((j + 1.5 + sy[k] * radius) / (TET + 2) - 0.5) * 2.0
            , 0
            , 1
            );
        }
      }
    }
  }
  glEnd();
  SDL_GL_SwapWindow(state.window);
}

void video(void)
{
  while (interact())
  {
    video1();
  }
}

void main1(void)
{
  if (interact())
  {
    video1();
  }
}


extern void edo(int TETv)
{
  if (! (1 <= TETv && TETv <= MAXTET))
  {
    TETv = 5;
  }
  TET = TETv;
  for (int i = 0; i < TET; ++i)
  {
    for (int j = 0; j < TET; ++j)
    {
      for (int c = 0; c < 4; ++c)
      {
        MATRIX[i][j][c] = 0;
      }
    }
  }
  state.reloaded = 1;
  printf("%d-EDO\n", TET);
}

int main(int argc, char **argv)
{
  SDL_AudioDeviceID dev = 0;
  srand(time(0));
  memset(&state, 0, sizeof(state));
  printf("Harmonic Protocols (AGPL3+) 2019,2020,2022 Claude Heiland-Allen\n");
#ifndef __EMSCRIPTEN__
  if (argc > 3)
  {
    SR = 44100;
    SF_INFO info = { 0, SR, 2, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
    SNDFILE *sf = sf_open(argv[3], SFM_WRITE, &info);
    if (! sf)
    {
      return 1;
    }
    edo(atoi(argv[1]));
    float out[44100][2];
    for (int i = 0; i < 20 * 60; ++i)
    {
      for (int j = 0; j < 44100; ++j)
      {
        go(&state, &out[j][0]);
      }
      sf_writef_float(sf, &out[0][0], 44100);
    }
    sf_close(sf);
  }
  else
#endif
  {
    // initialize SDL2 audio
    SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO);
    SDL_AudioSpec want, have;
    want.freq = SR;
    want.format = AUDIO_F32;
    want.channels = 2;
    want.samples = 4096;
    want.callback = audio;
    dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_ANY_CHANGE);
    if (have.freq > 192000 || have.format != AUDIO_F32 || have.channels != 2)
    {
      printf("want: %d %d %d %d\n", want.freq, want.format, want.channels, want.samples);
      printf("have: %d %d %d %d\n", have.freq, have.format, have.channels, have.samples);
      printf("error: bad audio parameters\n");
      return 1;
    }
    SR = have.freq;
    if (argc > 2)
    {
      edo(atoi(argv[1]));
    }
    else if (argc > 1)
    {
      edo(atoi(argv[1]));
    }
    else
    {
      edo(5);
    }
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
    state.window = SDL_CreateWindow("Harmonic Protocols", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (TET + 2) * CELLSIZE, (TET + 2) * CELLSIZE, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    if (! state.window)
    {
      printf("error: couldn't create SDL2 window\n");
      return 1;
    }
    state.context = SDL_GL_CreateContext(state.window);
    if (! state.context)
    {
      printf("error: couldn't create OpenGL context\n");
      return 1;
    }
    // start audio processing
    SDL_PauseAudioDevice(dev, 0);
#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop(main1, 0, 1);
#else
    video();
#endif
  }
  SDL_DestroyWindow(state.window);
  state.window = NULL;
  SDL_Quit();
  return 0;
}
