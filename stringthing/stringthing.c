/*
    stringthing -- pseudo-generative spinning string drone physical model
    Copyright (C) 2011-10-18 Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jack/jack.h>
#include <GL/glew.h>
#include <GL/glut.h>

#define PI 3.141592653589793
#define DT 0.0625
#define FRICTION 0.9999
#define GRAVITY 2

static double SR = 48000;
static double HZ;
#define MINN 32
#define MAXN 2048
static int N = 1024;

struct elem {
  double x, y, z, dx, dy, dz;
};
static struct elem string[2][MAXN];
static volatile int w = 0;
static jack_default_audio_sample_t buf[MAXN][2];

static jack_client_t *client;
static jack_port_t *port[2];

static int phase = -1;
static int t = 0;
static double f1 = 0;
static double f2 = 0;
static double p1 = 0;
static double p2 = 0;
static double lo = 0;
static double lh = 0;
static double lb = 0;
static double ro = 0;
static double rh = 0;
static double rb = 0;

static float vstring[MAXN][4];
static int ufudge = 0;
static int uangle = 0;

static int width = 512;
static int height = 288;
static int fullscreen = 0;

static void displaycb(void) {
  int s = w;
  float tot = 0;
  for (int i = 0; i < N; ++i) {
    vstring[i][0] = string[s][i].dx; tot += string[s][i].dx * string[s][i].dx;
    vstring[i][1] = string[s][i].dy; tot += string[s][i].dy * string[s][i].dy;
    vstring[i][2] = string[s][i].dz; tot += string[s][i].dz * string[s][i].dz;
    vstring[i][3] = 0.0;
  }
  tot = sqrt(tot / (3 * N));
  for (int i = 0; i < N; ++i) {
    vstring[i][0] /= tot;
    vstring[i][1] /= tot;
    vstring[i][2] /= tot;
    vstring[i][3] /= tot;
  }
  glTexSubImage1D(GL_TEXTURE_1D, 0, 0, N, GL_RGBA, GL_FLOAT, vstring);
  glUniform1fARB(ufudge, (N - 1) / (float) MAXN);
  glUniform1fARB(uangle, atan2(vstring[N-1][1], vstring[N-1][0]));
  glBegin(GL_QUADS); {
    float a = width * 1.0 / height;
    float f = N * 1.25 /  MAXN;
    float x = a * f;
    float y =     f;
    glTexCoord2f( x, -y); glVertex2f(1, 0);
    glTexCoord2f( x,  y); glVertex2f(1, 1);
    glTexCoord2f(-x,  y); glVertex2f(0, 1);
    glTexCoord2f(-x, -y); glVertex2f(0, 0);
  } glEnd();
  glutSwapBuffers();
  glutReportErrors();
}

static void reshapecb(int w, int h) {
  width = w;
  height = h;
  glViewport(0, 0, width, height);
  glLoadIdentity();
  gluOrtho2D(0, 1, 1, 0);
  glutPostRedisplay();
}

static void timercb(int v) {
  glutPostRedisplay();
  glutTimerFunc(20, timercb, v);
}

static double f1s[256];
static double f2s[256];
static void keyboard1cb(unsigned char key, int x, int y) {
  if (isalpha(key)) {
    if (isupper(key)) {
      f1s[tolower(key)] = f1;
      f2s[tolower(key)] = f2;
    } else {
      f1 = f1s[key];
      f2 = f2s[key];
    }
  } else {
    switch (key) {
    case ' ': { double temp = f1; f1 = f2; f2 = -temp; break; }
    }
  }
}

static void keyboard2cb(int key, int x, int y) {
  switch (key) {
  case GLUT_KEY_F11:
    fullscreen = !fullscreen;
    if (fullscreen) {
      glutFullScreen();
      glutSetCursor(GLUT_CURSOR_NONE);
    } else {
      glutReshapeWindow(512, 288);
      glutSetCursor(GLUT_CURSOR_INHERIT);
    }
    break;
  }
}

static void mousecb(int x, int y) {
  double f = (x - width  / 2) * 2.0 / width;
  double d = (height / 2 - y) * 4.0 / height;
  double m = (pow(2, fabs(f)) - 1) * (f < 0 ? -1 : 1);
  double r = pow(2, d);
  f1 = (m * r) / 8.0;
  f2 = (m / r) / 8.0;
}

static double ls = 0;

static int processcb(jack_nframes_t nframes, void *arg) {
  jack_default_audio_sample_t *out[2];
  out[0] = (jack_default_audio_sample_t *)
    jack_port_get_buffer(port[0], nframes);
  out[1] = (jack_default_audio_sample_t *)
    jack_port_get_buffer(port[1], nframes);
  for (unsigned int k = 0; k < nframes; ++k, ++phase) {
    if (phase >= 2 * N) {
      phase = 0;
    }
    if (phase == 0) {
      double s = 0;
      for (int i = 0; i < N; ++i) {
        s += string[w][i].dx * string[w][i].dx
          +  string[w][i].dy * string[w][i].dy
          +  string[w][i].dz * string[w][i].dz;
      }
      s = 4 * sqrt(s / N);
      ls = 0.99 * ls + 0.01 * s;
      if (!(ls > 0)) { ls = 1; }
      double a0 = -atan2(string[w][N-1].dy, string[w][N-1].dx);
      double s0 = sin(a0);
      double c0 = cos(a0);
      for (int j = 0; j < 2 * N; ++j) {
        int i = j >= N ? 2 * N - 1 - j : j;
        double x =  c0 * string[w][i].dx + s0 * string[w][i].dy;
        double y = -s0 * string[w][i].dx + c0 * string[w][i].dy;
        double lv = x * x + string[w][i].dz * string[w][i].dz;
        double rv = y * y + string[w][i].dz * string[w][i].dz;
        double wd = (1 - cos(j * PI / N)) / 2;
        lv = sqrt(lv) / ls;
        rv = sqrt(rv) / ls;
        lo = 0.999 * lo + 0.001 * lv;
        ro = 0.999 * ro + 0.001 * rv;
        lh = lv - lo;
        rh = rv - ro;
        lb = tanh(0.5 * lb + 0.5 * wd * lh);
        rb = tanh(0.5 * rb + 0.5 * wd * rh);
        buf[j][0] = tanh(2 * lb);
        buf[j][1] = tanh(2 * rb);
      }
      double a1 = 1 / (1 + f1 * f2);
      double a2 = 1 / (1 + f1 * f2);
      for (int tt = 0; tt < 256; ++tt) {
        p1 += f1 * N / SR;
        p2 += f2 * N / SR;
        while (p1 > 1) { p1 -= 1; } while (p1 < 0) { p1 += 1; }
        while (p2 > 1) { p2 -= 1; } while (p2 < 0) { p2 += 1; }
        {
          double fx = a1 * cos(2 * PI * p1) - a2 * sin(2 * PI * p2);
          double fy = a1 * sin(2 * PI * p1) + a2 * cos(2 * PI * p2);
          double fz = 0;
          string[1-w][0].dx = FRICTION * string[w][0].dx + fx * DT;
          string[1-w][0].dy = FRICTION * string[w][0].dy + fy * DT;
          string[1-w][0].dz = FRICTION * string[w][0].dz + fz * DT;
          string[1-w][0].x = string[w][0].x + string[1-w][0].dx * DT;
          string[1-w][0].y = string[w][0].y + string[1-w][0].dy * DT;
          string[1-w][0].z = string[w][0].z + string[1-w][0].dz * DT;
        }
        for (int i = 1; i < N - 1; ++i) {
          double fx = string[w][i-1].x + string[w][i+1].x - 2 * string[w][i].x;
          double fy = string[w][i-1].y + string[w][i+1].y - 2 * string[w][i].y;
          double fz = string[w][i-1].z + string[w][i+1].z - 2 * string[w][i].z
                    - GRAVITY;
          string[1-w][i].dx = FRICTION * string[w][i].dx + fx * DT;
          string[1-w][i].dy = FRICTION * string[w][i].dy + fy * DT;
          string[1-w][i].dz = FRICTION * string[w][i].dz + fz * DT;
          string[1-w][i].x = string[w][i].x + string[1-w][i].dx * DT;
          string[1-w][i].y = string[w][i].y + string[1-w][i].dy * DT;
          string[1-w][i].z = string[w][i].z + string[1-w][i].dz * DT;
        }
        for (int i = N - 1; i < N; ++i) {
          double fx = string[w][i-1].x - string[w][i].x;
          double fy = string[w][i-1].y - string[w][i].y;
          double fz = string[w][i-1].z - string[w][i].z - GRAVITY;
          string[1-w][i].dx = FRICTION * string[w][i].dx + fx * DT;
          string[1-w][i].dy = FRICTION * string[w][i].dy + fy * DT;
          string[1-w][i].dz = FRICTION * string[w][i].dz + fz * DT;
          string[1-w][i].x = string[w][i].x + string[1-w][i].dx * DT;
          string[1-w][i].y = string[w][i].y + string[1-w][i].dy * DT;
          string[1-w][i].z = string[w][i].z + string[1-w][i].dz * DT;
        }
        w = 1 - w;
      }
      ++t;
    }
    out[0][k] = buf[phase][0];
    out[1][k] = buf[phase][1];
  }
  return 0;
}

static int sratecb(jack_nframes_t nframes, void *arg) {
  SR = nframes;
  N = fmin(fmax(SR / HZ, MINN), MAXN);
  return 0;
}

static void errorcb(const char *desc) {
  fprintf(stderr, "JACK error: %s\n", desc);
}

static void shutdowncb(void *arg) {
  exit(1);
}

static void exitcb(void) {
  jack_client_close(client);
}

static const char *src =
"uniform sampler1D tex;\n"
"uniform sampler2D palette;\n"
"uniform float fudge;\n"
"uniform float angle;\n"
"void main() {\n"
"  vec2 q = gl_TexCoord[0].xy;\n"
"  float l = length(q);\n"
"  float a = atan(q.y, q.x) + angle;\n"
"  float s = sin(a);\n"
"  float c = cos(a);\n"
"  mat2 m = mat2(c, s, -s, c);\n"
"  float r = clamp(l, 0.0, fudge);\n"
"  vec2 p = texture1D(tex, r).rg;\n"
"  vec4 rgba = texture2D(palette, p * m * 0.125 + vec2(0.5));\n"
"  rgba.rgb *= pow(clamp(r / l, 0.0, 1.0), 2.0);\n"
"  gl_FragColor = rgba;\n"
"}\n";

int main(int argc, char **argv) {
  HZ = 0;
  if (argc > 1) {
    HZ = atof(argv[1]);
  }
  if (! HZ) { HZ = 96000.0 / 1024.0; }
  memset(string, 0, 2 * MAXN * sizeof(struct elem));
  memset(buf, 0, 2 * MAXN * sizeof(jack_default_audio_sample_t));
  for (int i = 0; i < N; ++i) {
    string[w][i].z = -i;
  }
  memset(f1s, 0, 256 * sizeof(double));
  memset(f2s, 0, 256 * sizeof(double));
  /* initialize OpenGL */
  glutInitWindowSize(width, height);
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow("stringthing");
  glewInit();
  glShadeModel(GL_FLAT);
  GLuint texp;
  glActiveTexture(GL_TEXTURE1);
  glGenTextures(1, &texp);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, texp);
  unsigned char *bufp = malloc(512 * 512 * 4);
  double gamma = 1;
  for (int j = 0; j < 512; ++j) {
    for (int i = 0; i < 512; ++i) {
      double x = (i - 256) / 64.0;
      double y = (j - 256) / 64.0;
      double h = atan2(y, x) * 0.15915494309189535;
      double s = fmin(0.25 * sqrt(x * x + y * y), 1.0);
      double v = 0.0625 * (x * x + y * y);
      h -= floor(h);
      h *= 6;
      int k = floor(h);
      double f = h - k;
      double p = v * (1 - s);
      double q = v * (1 - s * f);
      double t = v * (1 - s * (1 - f));
      double r, g, b;
      switch (k) {
      case  0: r = v; g = t; b = p; break;
      case  1: r = q; g = v; b = p; break;
      case  2: r = p; g = v; b = t; break;
      case  3: r = p; g = q; b = v; break;
      case  4: r = t; g = p; b = v; break;
      default: r = v; g = p; b = q; break;
      }
      double z = 0.5 * s / (0.001 + 0.299 * r + 0.587 * g + 0.114 * b);
      double w = pow(256, 1.0/gamma) * 256 * z / (1 + pow(fmax(x * x + y * y, 16), 2));
      bufp[(512 * j + i)*4 + 0] = fmin(fmax(pow(w * r, gamma), 0), 255);
      bufp[(512 * j + i)*4 + 1] = fmin(fmax(pow(w * g, gamma), 0), 255);
      bufp[(512 * j + i)*4 + 2] = fmin(fmax(pow(w * b, gamma), 0), 255);
      bufp[(512 * j + i)*4 + 3] = 1;
    }
  }
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 512, 512, 0, GL_RGBA, GL_UNSIGNED_BYTE, bufp);
  free(bufp);
  glGenerateMipmap(GL_TEXTURE_2D);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glActiveTexture(GL_TEXTURE0);
  GLuint tex;
  glGenTextures(1, &tex);
  glEnable(GL_TEXTURE_1D);
  glBindTexture(GL_TEXTURE_1D, tex);
  glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA32F_ARB, MAXN, 0, GL_RGBA, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  /* compile shaders */
  GLint success;
  int prog = glCreateProgramObjectARB();
  int frag = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
  glShaderSourceARB(frag, 1, (const GLcharARB **) &src, 0);
  glCompileShaderARB(frag);
  glAttachObjectARB(prog, frag);
  glLinkProgramARB(prog);
  glGetObjectParameterivARB(prog, GL_OBJECT_LINK_STATUS_ARB, &success);
  if (!success) exit(1);
  glUseProgramObjectARB(prog);
  glUniform1iARB(glGetUniformLocationARB(prog, "tex"), 0);
  glUniform1iARB(glGetUniformLocationARB(prog, "palette"), 1);
  ufudge = glGetUniformLocationARB(prog, "fudge");
  uangle = glGetUniformLocationARB(prog, "angle");

  /* set up callbacks */
  glutDisplayFunc(displaycb);
  glutReshapeFunc(reshapecb);
  glutMotionFunc(mousecb);
  glutPassiveMotionFunc(mousecb);
  glutKeyboardFunc(keyboard1cb);
  glutSpecialFunc(keyboard2cb);
  glutTimerFunc(20, timercb, 0);

  /* initialize JACK */
  jack_set_error_function(errorcb);
  if (!(client = jack_client_open("stringthing", 0, 0))) {
    return 1;
  }
  atexit(exitcb);
  jack_set_process_callback(client, processcb, 0);
  jack_set_sample_rate_callback(client, sratecb, 0);
  jack_on_shutdown(client, shutdowncb, 0);
  port[0] = jack_port_register(
    client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
  );
  port[1] = jack_port_register(
    client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
  );
  if (jack_activate(client)) {
    fprintf (stderr, "cannot activate JACK client");
    return 1;
  }
  const char **ports;
  if ((ports = jack_get_ports(
    client, NULL, NULL, JackPortIsPhysical | JackPortIsInput
  ))) {
    int i = 0;
    while (ports[i] && i < 2) {
      if (jack_connect(
        client, jack_port_name(port[i]), ports[i]
      )) {
        fprintf(stderr, "cannot connect output port\n");
      }
      i++;
    }
    free(ports);
  }

  glutMainLoop();
  return 0;
}
