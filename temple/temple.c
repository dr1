#define _DEFAULT_SOURCE

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <jack/jack.h>

static const float pif = 3.141592653589793f;

struct vector {
  float v[3];
};

#define BUCKETS 8

struct histogram {
  float h[BUCKETS];
};

static void clear(struct histogram *h) {
  for (int i = 0; i < BUCKETS; ++i) {
    h->h[i] = 0.0f;
  }
}

static void accum(struct histogram *h, const struct vector *v) {
  int b = ((int) floor(BUCKETS/2.0f * (atan2f(v->v[1], v->v[2]) / pif + 1.0f)) + BUCKETS) % BUCKETS;
  h->h[b] += v->v[0];
}

static void histy4m(struct histogram *h, const unsigned char *y, const unsigned char *u, const unsigned char *v, int width, int height) {
  clear(h);
  for (int j = 0; j < height/2; ++j) {
    for (int i = 0; i < width/2; ++i) {
      struct vector p;
      p.v[0] = 0.25f * (y[2*j*width + 2*i] + y[2*j*width + 2*i+1] + y[(2*j+1)*width + 2*i] + y[(2*j+1)*width + 2*i+1]);
      p.v[1] = u[j*width/2 + i] - 128.0f;
      p.v[2] = v[j*width/2 + i] - 128.0f;
      accum(h, &p);
    }
  }
  for (int i = 0; i < BUCKETS; ++i) {
    h->h[i] /= (width * height * 256.0);
  }
}

#define TABSIZE 1024

struct borgan {
  float table[2][TABSIZE];
  int read[2];
  int write;
  int which;
  float env[TABSIZE];
  float hipcoef;
  float hip[2];
  float lopcoef;
  float lop[2];
  float phase;
  float inc;
};

void borgan_init(struct borgan *b, float sr, float f) {
  memset(b, 0, sizeof(struct borgan));
  for (int i = 0; i < TABSIZE; ++i) {
    b->env[i] = sinf(i * pif / TABSIZE);
  }
  b->hipcoef = 1.0f - 10.0f * 2.0f * pif / sr;
  b->lopcoef = 1000.0f * 2.0f * pif / sr;
  b->inc = TABSIZE * f / sr;
}

float borgan_dsp(struct borgan *b, float vol) {
  float osc1 = b->table[b->which][b->read[0]];
  float osc2 = b->table[b->which][b->read[1]];
  float osc = tanhf(1.5f * (osc1 - osc2 + 0.25f * (rand() / (float) RAND_MAX * 2.0f - 1.0f)));
  float n = 0.5f * (1.0f + b->hipcoef);
  float t = osc + b->hipcoef * b->hip[0];
  osc = n * (t - b->hip[0]);
  b->hip[0] = t;
  t = osc + b->hipcoef * b->hip[1];
  osc = n * (t - b->hip[1]);
  b->hip[1] = t;
  n = 1.0f - b->lopcoef;
  b->lop[0] = osc = b->lopcoef * osc + n * b->lop[0];
  b->lop[1] = osc = b->lopcoef * osc + n * b->lop[1];
  b->table[1 - b->which][b->write] = b->env[b->write] * osc;
  int i = floorf(b->phase);
  float x = b->phase - i;
  float p = b->table[b->which][(i - 1) & (TABSIZE - 1)];
  float q = b->table[b->which][(i    ) & (TABSIZE - 1)];
  float r = b->table[b->which][(i + 1) & (TABSIZE - 1)];
  float s = b->table[b->which][(i + 2) & (TABSIZE - 1)];
  float out = 0.5f * ((-x*x*x+2.0f*x*x-x) * p + (3.0f*x*x*x-5.0f*x*x+2.0f) * q + (-3.0f*x*x*x+4.0f*x*x+x) * r + (x*x*x-x*x) * s);
  b->phase += b->inc;
  if (b->phase >= TABSIZE) {
    b->phase -= TABSIZE;
  }
  b->read[0] = (b->read[0] + 1) & (TABSIZE - 1);
  b->read[1] = (b->read[1] - 2) & (TABSIZE - 1);
  b->write = (b->write + 1) & (TABSIZE - 1);
  if (! b->write) {
    b->which = 1 - b->which;
  }
  return out * vol;
}

struct dsp {
  struct borgan b[2][BUCKETS];
};

void dsp_init(struct dsp *d, float sr) {
  for (int i = 0; i < BUCKETS; ++i) {
    borgan_init(&d->b[0][i], sr, 30.0f * (i + 1));
    borgan_init(&d->b[1][i], sr, 30.0f * (i + 1));
  }
}

void dsp_dsp(struct dsp *d, const struct histogram *h, float *oleft, float *oright) {
  float o[2] = { 0.0f, 0.0f };
  for (int i = 0; i < BUCKETS; ++i) {
    o[0] += borgan_dsp(&d->b[0][i], h->h[i]);
    o[1] += borgan_dsp(&d->b[1][i], h->h[i]);
  }
  *oleft = o[0];
  *oright = o[1];
}

struct dsp dsp;
int which = 0;
struct histogram histogram[2];

static jack_client_t *client;
static jack_port_t *port[2];

static int sratecb(jack_nframes_t sr, void *arg) {
  (void) arg;
  dsp_init(&dsp, sr);
  return 0;
}

static void errorcb(const char *desc) {
  fprintf(stderr, "JACK error: %s\n", desc);
}

static void shutdowncb(void *arg) {
  (void) arg;
  exit(1);
}

static void exitcb(void) {
  jack_client_close(client);
}

static int processcb(jack_nframes_t nframes, void *arg) {
  (void) arg;
  jack_default_audio_sample_t *out[2];
  out[0] = (jack_default_audio_sample_t *)
    jack_port_get_buffer(port[0], nframes);
  out[1] = (jack_default_audio_sample_t *)
    jack_port_get_buffer(port[1], nframes);
  for (unsigned int k = 0; k < nframes; ++k) {
    float l, r;
    dsp_dsp(&dsp, &histogram[which], &l, &r);
    out[0][k] = l;
    out[1][k] = r;
  }
  return 0;
}

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  for (int i = 0; i < BUCKETS; ++i) {
    histogram[0].h[i] = 1.0 / BUCKETS;
    histogram[1].h[i] = 1.0 / BUCKETS;
  }
  jack_set_error_function(errorcb);
  if (!(client = jack_client_open("temple", 0, 0))) {
    return 1;
  }
  atexit(exitcb);
  jack_set_process_callback(client, processcb, 0);
  jack_set_sample_rate_callback(client, sratecb, 0);
  jack_on_shutdown(client, shutdowncb, 0);
  port[0] = jack_port_register(
    client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
  );
  port[1] = jack_port_register(
    client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
  );
  if (jack_activate(client)) {
    fprintf (stderr, "cannot activate JACK client");
    return 1;
  }
  const char **ports;
  if ((ports = jack_get_ports(
    client, NULL, NULL, JackPortIsPhysical | JackPortIsInput
  ))) {
    int i = 0;
    while (ports[i] && i < 2) {
      if (jack_connect(
        client, jack_port_name(port[i]), ports[i]
      )) {
        fprintf(stderr, "cannot connect output port\n");
      }
      i++;
    }
    free(ports);
  }
  int width, height;
  FILE *f = stdin;
  fscanf(f, "YUV4MPEG2 W%d H%d F25:1 Ip A1:1 C420mpeg2 XYSCSS=420MPEG2\n", &width, &height);
  unsigned char *ybuf = malloc(width * height);
  unsigned char *ubuf = malloc(width * height / 4);
  unsigned char *vbuf = malloc(width * height / 4);
  while (EOF != fscanf(f, "FRAME\n")) {
    putchar('.');
    fflush(stdout);
    fread(ybuf, width * height, 1, f);
    fread(ubuf, width * height / 4, 1, f);
    fread(vbuf, width * height / 4, 1, f);
    histy4m(&histogram[1 - which], ybuf, ubuf, vbuf, width, height);
    which = 1 - which;
    usleep(40000);
  }
  free(ybuf);
  free(ubuf);
  free(vbuf);
  return 0;
}
