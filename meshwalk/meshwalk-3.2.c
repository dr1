/*
  meshwalk -- audiovisual drone
  Copyright (C) 2018  Claude Heiland-Allen <claude@mathr.co.uk>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
/*
# build
make

# realtime mode with JACK audio, press Q to exit
./meshwalk-3.2

# render
./meshwalk-3.2.sh
*/

#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <jack/jack.h>
#include <sndfile.h>

#define pi 3.141592653f

#define SR 48000
#define FPS 30
#define LOD 4
#define OSCILLATORS 4000
#define GAIN 0.1f

#define DEBUG_AMBISONICS 0
#define DEBUG_AMBISONICS_SPIN 0 //(2 * pi / FPS / 12)
#define DEBUG_AMBISONICS_AXES 0

#define DRAW_CLEAR 0
#define DRAW_VORONOI 1
#define DRAW_LINES 0
#define DRAW_POINTS 0
#define DRAW_TRAILS 0

#define RECORD_AUDIO 1
#define RECORD_VIDEO 1

#define R0 4
#define W 8192
#define H 4096

#define WINW 1024
#define WINH 512

// note: must be <= 4000 (limit set in fragment shader)
#define N OSCILLATORS
#define B 12

// one-pole one-zero low pass filter designed in the Z plane

typedef float sample;

typedef struct {
  sample x, b;
  double y, a;
} LOP1;

static inline LOP1 *lop1_init(LOP1 *s, sample hz)
{
  const double w = 2 * pi * fminf(fmaxf(fabs(hz / SR), 0), (sample)0.5);
  const double a = (1 - sin(w)) / cos(w);
  const double b = (1 - a) / 2;
  s->a = a;
  s->b = b;
  s->x = 0;
  s->y = 0;
  return s;
}

static inline sample lop1(LOP1 *s, sample x)
{
  const double y = s->b * (x + s->x) + s->a * s->y;
  s->x = x;
  s->y = y;
  return y;
}


float DISTANCE;
float Ky;
float K;
float sqrtK;
const float dt = 1.0f / FPS;
const float SPEED = 2.0 * sqrtf(1.0f / N);

GLint u_position;
GLfloat position[N][4];

GLfloat V[3][3] = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };

float focus[3];

const float spin_axes[4][3] =
  { { -1, -1, -1 }
  , {  1,  1, -1 }
  , {  1, -1,  1 }
  , { -1,  1,  1 }
  };

int oscillator_targets[OSCILLATORS];

LOP1 output_filters[16];

typedef struct { float f; int i; } index_t;

typedef struct
{
  float x, y, z, dx, dy, dz, ddx, ddy, ddz, speed;
  index_t nbnb[B * B + B];
  int nb[B];
  int count;
} node;

node atom[N];
int which_oscillator_buffer = 0;
index_t oscillators[3][N];

static inline uint32_t hash(uint32_t a)
{
	a = (a+0x7ed55d16u) + (a<<12);
	a = (a^0xc761c23cu) ^ (a>>19);
	a = (a+0x165667b1u) + (a<<5);
	a = (a+0xd3a2646cu) ^ (a<<9);
	a = (a+0xfd7046c5u) + (a<<3);
	a = (a^0xb55a4f09u) ^ (a>>16);
	return a;
}

// GLFW keyboard callback
static void keycb
  ( GLFWwindow *window
  , int key
  , int scancode
  , int action
  , int mods
  )
{
  (void) key;
  (void) scancode;
  (void) mods;
  if (action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
}

float audio_xyz[N][3];
static void audiocb(float *output[16], int n, int f, int which)
{
  float out[16][n];
  int source = which_oscillator_buffer;
  for (int o = 0; o < OSCILLATORS && o < N; ++o)
  {
    int i = oscillators[source][o].i;
    oscillators[2][o] = oscillators[source][o];
    float x = atom[i].x;
    float y = atom[i].y;
    float z = atom[i].z;
    audio_xyz[o][0] = V[0][0] * x + V[0][1] * y + V[0][2] * z;
    audio_xyz[o][1] = V[1][0] * x + V[1][1] * y + V[1][2] * z;
    audio_xyz[o][2] = V[2][0] * x + V[2][1] * y + V[2][2] * z;
  }
  for (int c = 0; c < 16; ++c)
    for (int t = 0; t < n; ++t)
      out[c][t] = 0;
  for (int o = 0; o < OSCILLATORS && o < N; ++o)
  {
    int i = oscillators[2][o].i;
    if (which >= 0 && (i & 3) != which) continue;
    float amp = oscillators[2][o].f;
    float x = audio_xyz[o][0];
    float y = audio_xyz[o][1];
    float z = audio_xyz[o][2];
    float factor[16];
    // order 0
    factor[ 0] = 1;
    // order 1
    factor[ 1] = y;
    factor[ 2] = z;
    factor[ 3] = x;
    //  order 2
    factor[ 4] = sqrt(3) * x * y;
    factor[ 5] = sqrt(3) * y * z;
    factor[ 6] = 1/2. * (3 * z * z - 1);
    factor[ 7] = sqrt(3) * x * z;
    factor[ 8] = sqrt(3/4.) * (x * x - y * y);
    // order 3
    factor[ 9] = sqrt(5/8.) * y * (3 * x * x - y * y);
    factor[10] = sqrt(15) * x * y * z;
    factor[11] = sqrt(3/8.) * y * (5 * z * z - 1);
    factor[12] = 1/2. * z * (5 * z * z - 3);
    factor[13] = sqrt(3/8.) * x * (5 * z * z - 1);
    factor[14] = sqrt(15/4.) * z * (x * x - y * y);
    factor[15] = sqrt(5/8.) * x * (x * x - 3 * y * y);
    int period = (720 + ((i % 15)-7)) / ((i % 4) + 1);
    float osc[n];
    for (int t = 0; t < n; ++t)
      osc[t] = hash(hash((f + t) % period) * N + i) / (float) UINT_MAX - 0.5f;
    for (int c = 0; c < 16; ++c)
    {
      factor[c] *= amp;
      for (int t = 0; t < n; ++t)
        out[c][t] += osc[t] * factor[c];
    }
  }
  float level = GAIN;
  for (int c = 0; c < 16; ++c)
    for (int t = 0; t < n; ++t)
      output[c][t] = lop1(&output_filters[c], out[c][t] * level);
}

jack_client_t *client[4];
jack_port_t *port[4][16];
static int processcb(jack_nframes_t nframes, void *arg) {
  int *which = arg;
  static jack_nframes_t f[4] = { 0, 0, 0, 0 };
  jack_default_audio_sample_t *out[16];
  for (int c = 0; c < 16; ++c)
  {
    out[c] = jack_port_get_buffer(port[*which][c], nframes);
    for (jack_nframes_t t = 0; t < nframes; ++t)
      out[c][t] = 0;
  }
  audiocb(out, nframes, f[*which], *which);
  f[*which] += nframes;
  return 0;
}


static void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    fprintf(stderr, "program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

static void debug_shader(GLuint shader, GLenum type, const char *source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
      case GL_COMPUTE_SHADER: type_str = "compute"; break;
    }
    fprintf
      ( stderr
      , "%s shader compile info:\n%s\nshader source:\n%s"
      , type_str
      , info ? info : "(no info log)"
      , source ? source : "(no source)"
      );
  }
  if (info) {
    free(info);
  }
}

static GLuint vertex_fragment_shader(const char *vert, const char *frag) {
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(shader, 1, &vert, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_VERTEX_SHADER, vert);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(shader, 1, &frag, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_FRAGMENT_SHADER, frag);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  debug_program(program);
  return program;
}

int cmp_index_f(const void *a, const void *b)
{
  const index_t *p = a;
  const index_t *q = b;
  const float x = p->f;
  const float y = q->f;
  return (x > y) - (x < y);
}

int cmp_index_f_rev(const void *a, const void *b)
{
  return -cmp_index_f(a, b);
}

void upload_uniforms(void)
{
  for (int i = 0; i < N; ++i)
  {
    float x = atom[i].x;
    float y = atom[i].y;
    float z = atom[i].z;
    position[i][0] = V[0][0] * x + V[0][1] * y + V[0][2] * z;
    position[i][1] = V[1][0] * x + V[1][1] * y + V[1][2] * z;
    position[i][2] = V[2][0] * x + V[2][1] * y + V[2][2] * z;
    position[i][3] = 1;
  }
  glUniform4fv(u_position, N, &position[0][0]);
}


unsigned char ppm[4096][8192][3];

void insert_by_distance(int i, int k, float d)
{
  int b;
  for (b = 0; b < B && b < atom[i].count; ++b)
  {
    if (d < atom[i].nbnb[b].f) break;
  }
  if (b < B)
  {
    for (int c = B - 1; c > b; --c)
    {
      atom[i].nbnb[c] = atom[i].nbnb[c - 1];
    }
    atom[i].nbnb[b].f = d;
    atom[i].nbnb[b].i = k;
    if (atom[i].count < B) atom[i].count += 1;
  }
}

int find_target(float tx, float ty, float tz)
{
  float md = -2.0;
  int ix = -1;
  for (int i = 0; i < N; ++i)
  {
    float d = atom[i].x * tx + atom[i].y * ty + atom[i].z * tz;
    if (d > md)
    {
      md = d;
      ix = i;
    }
  }
  return ix;
}

void init(void)
{
  for (int i = 0; i < N; ++i)
  {
    #define U (rand() / (float) RAND_MAX)
    float R;
      float t = 2 * pi * (i + U) / N;
      float s = 2.0 * (U - 0.5);
    atom[i].x = cosf(t) * cosf(asinf(s));
    atom[i].y = sinf(t) * cosf(asinf(s));
    atom[i].z = s;
    R = atom[i].x * atom[i].x + atom[i].y * atom[i].y + atom[i].z * atom[i].z;
    R = sqrtf(R);
    atom[i].x /= R;
    atom[i].y /= R;
    atom[i].z /= R;
    atom[i].dx = 0;
    atom[i].dy = 0;
    atom[i].dz = 0;
    #undef U
  }
  #pragma omp parallel for
  for (int i = 0; i < N; ++i)
  {
    atom[i].count = 0;
    for (int k = 0; k < N; ++k)
    {
      if (i == k) continue;
      float dx = atom[i].x - atom[k].x;
      float dy = atom[i].y - atom[k].y;
      float dz = atom[i].z - atom[k].z;
      float d = dx * dx + dy * dy + dz * dz;
      insert_by_distance(i, k, d);
    }
    assert(atom[i].count == B);
    for (int b = 0; b < B; ++b)
    {
      atom[i].nb[b] = atom[i].nbnb[b].i;
    }
  }
}

static void field_at_atom(float *ddx, float *ddy, float *ddz, int i)
{
  *ddx = 0;
  *ddy = 0;
  *ddz = 0;
  int j = i % 4;
  for (int b = 0; b < B; ++b)
  {
    int k = atom[i].nb[b];
    int l = k % 4;
    float dx = atom[k].x - atom[i].x;
    float dy = atom[k].y - atom[i].y;
    float dz = atom[k].z - atom[i].z;
    float d2 = dx * dx + dy * dy + dz * dz;
    if (d2 == 0) continue;
    float d = sqrtf(d2);
    float F = 0.0;
    F += 1 / (d2 * K);
    if (l != j)
      F -= 1 / (d * sqrtK);
    else
      F *= 2;
    *ddx -= F * dx / d;
    *ddy -= F * dy / d;
    *ddz -= F * dz / d;
  }
  if (isnan(*ddx)) *ddx = 0;
  if (isnan(*ddy)) *ddy = 0;
  if (isnan(*ddz)) *ddz = 0;
}

void insert_by_index(int i, int k, float d)
{
  for (int b = 0; b < atom[i].count; ++b)
  {
    if (atom[i].nbnb[b].i == k) return;
  }
  if (atom[i].count < B * B + B)
  {
    atom[i].nbnb[atom[i].count].f = d;
    atom[i].nbnb[atom[i].count].i = k;
    atom[i].count += 1;
  }
}

void sort_by_distance(int i)
{
  qsort(&atom[i].nbnb[0], atom[i].count, sizeof(atom[i].nbnb[0]), cmp_index_f);
}

float metric = 0;
void update_nearest_neighbours()
{
  float m = 0.0;
  for (int i = 0; i < N; ++i)
  {
    atom[i].count = 0;
    for (int b = 0; b < B; ++b)
    {
      int j = atom[i].nb[b];
      {
        float dx = atom[i].x - atom[j].x;
        float dy = atom[i].y - atom[j].y;
        float dz = atom[i].z - atom[j].z;
        float d = dx * dx + dy * dy + dz * dz;
        if (i != j)
        insert_by_index(i, j, d);
      }
      for (int c = 0; c < B; ++c)
      {
        int k = atom[j].nb[c];
        if (i == k) continue;
        float dx = atom[i].x - atom[k].x;
        float dy = atom[i].y - atom[k].y;
        float dz = atom[i].z - atom[k].z;
        float d = dx * dx + dy * dy + dz * dz;
        insert_by_index(i, k, d);
      }
    }
    assert(atom[i].count >= B);
    sort_by_distance(i);
    for (int b = 0; b < B; ++b)
      atom[i].nb[b] = atom[i].nbnb[b].i;
    m = fmaxf(m, atom[i].nbnb[B-1].f);
  }
  metric = m;
}

void update_oscillator_amplitudes(void)
{
  int dest = 1 - which_oscillator_buffer;
#if 0
  int l = 0;
  for (int i = -1; i <= 1; i += 1)
  for (int j = -1; j <= 1; j += 1)
  for (int k = -1; k <= 1; k += 1)
  {
    if (l >= OSCILLATORS) break;
    if (i == 0 && j == 0 && k == 0) continue;
    oscillator_targets[l] = find_target(i, j, k);
    ++l;
  }
  for (int i = 0; i < OSCILLATORS && i < N; ++i)
  {
    oscillators[dest][i].i = oscillator_targets[i];
    oscillators[dest][i].f = powf(atom[oscillator_targets[i]].speed, 8.0);
  }
  qsort(&oscillators[dest][0], OSCILLATORS < N ? OSCILLATORS : N, sizeof(oscillators[0][0]), cmp_index_f_rev);
#else
  for (int i = 0; i < N; ++i)
  {
    oscillators[dest][i].i = i;
    oscillators[dest][i].f = powf(atom[i].speed, 8.0);
  }
//  qsort(&oscillators[dest][0], N, sizeof(oscillators[0][0]), cmp_index_f_rev);
#endif
  which_oscillator_buffer = dest;
//  return atom[oscillators[dest][OSCILLATORS - 1].i].speed;
}

void step(void)
{
  for (int i = 0; i < N; ++i)
    field_at_atom(&atom[i].ddx, &atom[i].ddy, &atom[i].ddz, i);
  for (int i = 0; i < N; ++i)
  {
    atom[i].dx += atom[i].ddx * dt;
    atom[i].dy += atom[i].ddy * dt;
    atom[i].dz += atom[i].ddz * dt;
    // d = normalize(cross(p, cross(p, d)))
    float a1 = atom[i].x;
    float a2 = atom[i].y;
    float a3 = atom[i].z;
    float b1 = atom[i].dx;
    float b2 = atom[i].dy;
    float b3 = atom[i].dz;
    float s1 = a2 * b3 - a3 * b2;
    float s2 = a3 * b1 - a1 * b3;
    float s3 = a1 * b2 - a2 * b1;
    float t1 = a2 * s3 - a3 * s2;
    float t2 = a3 * s1 - a1 * s3;
    float t3 = a1 * s2 - a2 * s1;
    float t = -sqrtf(t1 * t1 + t2 * t2 + t3 * t3);
    atom[i].dx = t1 / t;
    atom[i].dy = t2 / t;
    atom[i].dz = t3 / t;
  }
  for (int i = 0; i < N; ++i)
  {
    float ox = atom[i].x;
    float oy = atom[i].y;
    float oz = atom[i].z;
    // boost = exp(dot(o, focus));
    float boost = expf(ox * focus[0] + oy * focus[1] + oz * focus[2]);
    float x = atom[i].x + atom[i].dx * dt * SPEED * boost;
    float y = atom[i].y + atom[i].dy * dt * SPEED * boost;
    float z = atom[i].z + atom[i].dz * dt * SPEED * boost;
    float R = x * x + y * y + z * z;
    R = sqrtf(R);
    atom[i].x = x / R;
    atom[i].y = y / R;
    atom[i].z = z / R;
#if 0
    float nx = atom[i].x;
    float ny = atom[i].y;
    float nz = atom[i].z;
    float dx = nx - ox;
    float dy = ny - oy;
    float dz = nz - oz;
    float s = sqrtf(dx * dx + dy * dy + dz * dz);
    atom[i].speed = tanhf(s / (SPEED * dt));
#else
    atom[i].speed = tanhf(boost);
#endif

  }
  update_nearest_neighbours();
  update_oscillator_amplitudes();
}

void plot(int x, int y, int r, int g, int b)
{
  if (0 <= x && x < W && 0 <= y && y < H)
  {
    y = H-1 - y;
    x = (W/2 + W-1 - x) % W;
    ppm[y][x][0] = r;
    ppm[y][x][1] = g;
    ppm[y][x][2] = b;
  }
}

void lineL(int x0,int y0, int x1, int y1, int r, int g, int b)
{
  int dx = x1 - x0;
  int dy = y1 - y0;
  int yi = 1;
  if (dy < 0)
  {
    yi = -1;
    dy = -dy;
  }
  int D = 2*dy - dx;
  int y = y0;
  for (int x = x0; x <= x1; ++x)
  {
    plot(x, y, r, g, b);
    if (D > 0)
    {
       y = y + yi;
       D = D - 2*dx;
    }
    D = D + 2*dy;
  }
}

void lineH(int x0, int y0, int x1, int y1, int r, int g, int b)
{
  int dx = x1 - x0;
  int dy = y1 - y0;
  int xi = 1;
  if (dx < 0)
  {
    xi = -1;
    dx = -dx;
  }
  int D = 2*dx - dy;
  int x = x0;

  for (int y = y0; y <= y1; ++y)
  {
    plot(x, y, r, g, b);
    if (D > 0)
    {
       x = x + xi;
       D = D - 2*dy;
    }
    D = D + 2*dx;
  }
}

void line(int x0, int y0, int x1, int y1, int r, int g, int b)
{
  if (abs(y1 - y0) < abs(x1 - x0))
    if (x0 > x1)
      lineL(x1, y1, x0, y0, r, g, b);
    else
      lineL(x0, y0, x1, y1, r, g, b);
  else
    if (y0 > y1)
      lineH(x1, y1, x0, y0, r, g, b);
    else
      lineH(x0, y0, x1, y1, r, g, b);
}

void draw_geodesic(float x0, float y0, float z0, float x1, float y1, float z1, int red, int grn, int blu, int depth)
{
  if (depth == 0)
  {
    int i0 = atan2f(y0, x0) / (2 * pi) * W;
    if (i0 < 0) i0 += W;
    int j0 = acosf(z0) / pi * H;
    int i1 = atan2f(y1, x1) / (2 * pi) * W;
    if (i1 < 0) i1 += W;
    int j1 = acosf(z1) / pi * H;
    if (i0 < W * 0.25 && W * 0.75 < i1)
    {
      line(i0, j0, i1 - W, j1, red, grn, blu);
      line(i0 + W, j0, i1, j1, red, grn, blu);
    }
    else
    if (i1 < W * 0.25 && W * 0.75 < i0)
    {
      line(i0 - W, j0, i1, j1, red, grn, blu);
      line(i0, j0, i1 + W, j1, red, grn, blu);
    }
    else
      line(i0, j0, i1, j1, red, grn, blu);
  }
  else
  {
    float x2 = x0 + x1;
    float y2 = y0 + y1;
    float z2 = z0 + z1;
    float r2 = x2 * x2 + y2 * y2 + z2 * z2;
    float r = sqrtf(r2);
    x2 /= r;
    y2 /= r;
    z2 /= r;
    draw_geodesic(x0, y0, z0, x2, y2, z2, red, grn, blu, depth - 1);
    draw_geodesic(x2, y2, z2, x1, y1, z1, red, grn, blu, depth - 1);
  }
}

void draw_lines_for(int i, int red, int grn, int blu)
{
  for (int b = 0; b < B; ++b)
  {
    int j = atom[i].nb[b];
    draw_geodesic
      ( atom[i].x, atom[i].y, atom[i].z
      , atom[j].x, atom[j].y, atom[j].z
      , red, grn, blu
      , 6
      );
  }
}

void draw_point(int i, int red, int grn, int blu)
{
  float ax = atom[i].x;
  float ay = atom[i].y;
  float az = atom[i].z;
  float x0 = atan2f(ay, ax) / (2 * pi);
  x0 -= floor(x0);
  x0 *= W;
  float y0 = acosf(az) / pi * H;
  for (int dy = -R0; dy <= R0; ++dy)
  {
    int y = y0 + dy;
    float w = 1 - fabsf((y0 + dy + 0.5f) / H * 2 - 1);
    if (y < 0) continue;
    if (y >= H) continue;
    for (int dx = -R0 / w; dx <= R0 / w; ++dx)
    {
      int x = x0 + dx;
      while (x < 0) x += W;
      while (x >= W) x -= W;
      if (dx * dx * w * w + dy * dy > R0 * R0) continue;
      plot(x, y, red, grn, blu);
    }
  }
}

void draw_lines(int target)
{
  // everything dark blue
  for (int i = 0; i < N; ++i)
    draw_lines_for(i, 0x00, 0x00, 0x80);
  // neighbours bright green
  for (int b = 0; b < B; ++b)
    draw_lines_for(atom[target].nb[b], 0x00, 0xFF, 0x00);
  // target yellow
  draw_lines_for(target, 0xFF, 0xFF, 0x00);
}


void draw_points(int target)
{
  // everything dark blue
  for (int i = 0; i < N; ++i)
    draw_point(i, 0x00, 0x00, 0x80);
  // neighbours of neighbours bright green
  for (int b = 0; b < B; ++b)
  {
    int j = atom[target].nb[b];
    for (int c = 0; c < B; ++c)
      draw_point(atom[j].nb[c], 0x00, 0xFF, 0x00);
  }
  // neighbours yellow
  for (int b = 0; b < B; ++b)
    draw_point(atom[target].nb[b], 0xFF, 0xFF, 0x00);
  // target red
  draw_point(target, 0xFF, 0x00, 0x00);
}

void draw_trails(void )
{
  const int colour[4][3] = { { 0, 0, 0 }, { 0, 0x80, 0xFF }, { 0xFF, 51, 0 }, { 0xFF, 0xFF, 0xFF } };
  for (int i = 0; i < N; ++i)
  {
    if (i % 4 != 3) continue;
    draw_point(i, colour[i%4][0], colour[i%4][1], colour[i%4][2]);
  }
}

void output(FILE *vidfile)
{
  fprintf(vidfile, "P6\n%d %d\n255\n", W, H);
  fwrite(&ppm[0][0][0], sizeof(ppm[0][0][0]) * 3 * W * H, 1, vidfile);
  fflush(vidfile);
}


static int global_which[4] = { 0, 1, 2, 3 };

// entry point
extern int main(int argc, char **argv)
{
  (void) argv;
  int RECORD = argc > 1;

  for (int o = 0; o < 16; ++o)
    lop1_init(&output_filters[o], 1000);

  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  glfwWindowHint(GLFW_DECORATED, GL_TRUE);
  GLFWwindow *window = glfwCreateWindow(WINW, WINH, "meshwalk", 0, 0);
  glfwMakeContextCurrent(window);
  glfwSwapInterval(2);
  glfwSetKeyCallback(window, keycb);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew
  // set up vertex array object
  glClearColor(0, 0, 0, 1);
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  // set up framebuffer
  GLuint fbo[LOD + 1];
  glGenFramebuffers(LOD + 1, &fbo[0]);
  GLuint tex;
  glGenTextures(1, &tex);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 8192, 4096, 0, GL_RGBA, GL_FLOAT, 0);
  glGenerateMipmap(GL_TEXTURE_2D);
  for (int lod = 0; lod <= LOD; ++lod)
  {
    glBindFramebuffer(GL_FRAMEBUFFER, fbo[lod]);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, lod);
    GLint status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
      fprintf(stderr, "GL FRAMEBUFFER STATUS %d\n", status);
  }

  // set up framebuffer 2
  GLuint fbo2;
  glGenFramebuffers(1, &fbo2);
  GLuint tex2;
  glGenTextures(1, &tex2);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex2);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 8192, 4096, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo2);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex2, 0);
  GLint status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE)
    fprintf(stderr, "GL FRAMEBUFFER STATUS %d\n", status);

  // set up voronoi shader
  const char *s_voronoi_vertex =
    "#version 330 core\n"
    "out vec2 tc;\n"
    "const float pi = 3.141592653;\n"
    "void main()\n"
    "{\n"
    "  if (gl_VertexID == 0) { tc = vec2( pi, -pi/2.0); gl_Position = vec4(-1.0, -1.0, 0.0, 1.0); } else\n"
    "  if (gl_VertexID == 1) { tc = vec2(-pi, -pi/2.0); gl_Position = vec4( 1.0, -1.0, 0.0, 1.0); } else\n"
    "  if (gl_VertexID == 2) { tc = vec2( pi,  pi/2.0); gl_Position = vec4(-1.0,  1.0, 0.0, 1.0); } else\n"
    "  if (gl_VertexID == 3) { tc = vec2(-pi,  pi/2.0); gl_Position = vec4( 1.0,  1.0, 0.0, 1.0); } else\n"
    "  { tc = vec2(0.0); gl_Position = vec4(0.0); }\n"
    "}\n"
    ;
  const char *s_voronoi_fragment =
    "#version 330 core\n"
    "in vec2 tc;\n"
    "uniform int N;\n"
    "uniform vec4 position[4000];\n"
    "layout (location = 0) out vec4 colour;\n"
    "void main()\n"
    "{\n"
    "  vec3 me = vec3(cos(tc.x) * cos(tc.y), sin(tc.x) * cos(tc.y), sin(tc.y));\n"
    "  float delta = length(vec2(length(dFdx(me)), length(dFdy(me))));\n"
    "  float maxdot = -2.0;\n"
    "  int ix = 0;\n"
    "  for (int i = 0; i < N && i < 4000; ++i)\n"
    "  {\n"
    "    float d = dot(position[i].xyz, me);\n"
    "    if (d > maxdot) { maxdot = d; ix = i; }\n"
    "  }\n"
    "  colour = vec4(position[ix].xyz, ix);\n"
    "}\n"
    ;
  GLuint p_voronoi = vertex_fragment_shader(s_voronoi_vertex, s_voronoi_fragment);
  glUseProgram(p_voronoi);
  u_position = glGetUniformLocation(p_voronoi, "position");
  GLint u_voronoi_N = glGetUniformLocation(p_voronoi, "N");
  glUniform1i(u_voronoi_N, N);

  // set up refinement shader
  const char *s_refine_vertex =
    "#version 330 core\n"
    "out vec4 tc;\n"
    "const float pi = 3.141592653;\n"
    "void main()\n"
    "{\n"
    "  if (gl_VertexID == 0) { tc = vec4( pi, -pi/2.0, 0.0, 0.0); gl_Position = vec4(-1.0, -1.0, 0.0, 1.0); } else\n"
    "  if (gl_VertexID == 1) { tc = vec4(-pi, -pi/2.0, 1.0, 0.0); gl_Position = vec4( 1.0, -1.0, 0.0, 1.0); } else\n"
    "  if (gl_VertexID == 2) { tc = vec4( pi,  pi/2.0, 0.0, 1.0); gl_Position = vec4(-1.0,  1.0, 0.0, 1.0); } else\n"
    "  if (gl_VertexID == 3) { tc = vec4(-pi,  pi/2.0, 1.0, 1.0); gl_Position = vec4( 1.0,  1.0, 0.0, 1.0); } else\n"
    "  { tc = vec4(0.0); gl_Position = vec4(0.0); }\n"
    "}\n"
    ;
  const char *s_refine_fragment =
    "#version 330 core\n"
    "in vec4 tc;\n"
    "uniform int R;\n"
    "uniform int lod;\n"
    "uniform sampler2D tex;\n"
    "layout (location = 0) out vec4 colour;\n"
    "void main()\n"
    "{\n"
    "  vec3 me = vec3(cos(tc.x) * cos(tc.y), sin(tc.x) * cos(tc.y), sin(tc.y));\n"
    "  ivec2 s = textureSize(tex, lod);\n"
    "  ivec2 loc = ivec2(floor(vec2(s) * tc.zw));\n"
    "  vec4 nearest = texelFetch(tex, loc, lod);\n"
    "  for (int j = -R; j <= R; ++j)\n"
    "    for (int i = -R; i <= R; ++i)\n"
    "    {\n"
    "      ivec2 c = loc + ivec2(i, j);\n"
    "      if (c.x < 0) c.x += s.x;\n"
    "      if (c.x >= s.x) c.x -= s.x;\n"
    "      if (c.y < 0) continue;\n"
    "      if (c.y >= s.y) continue;\n"
    "      vec4 test = texelFetch(tex, c, lod);\n"
    "      if (distance(me, test.xyz) < distance(me, nearest.xyz))\n"
    "        nearest = test;\n"
    "    }\n"
    "  colour = nearest;\n"
    "}\n"
  ;
  GLuint p_refine = vertex_fragment_shader(s_refine_vertex, s_refine_fragment);
  glUseProgram(p_refine);
  GLint u_refine_R = glGetUniformLocation(p_refine, "R");
  GLint u_refine_lod = glGetUniformLocation(p_refine, "lod");
  GLint u_refine_tex = glGetUniformLocation(p_refine, "tex");
  glUniform1i(u_refine_R, 2);
  glUniform1i(u_refine_tex, 1);

  // set up colourize shader
  const char *s_colour_vertex =
    "#version 330 core\n"
    "out vec4 tc;\n"
    "const float pi = 3.141592653;\n"
    "void main()\n"
    "{\n"
    "  if (gl_VertexID == 0) { tc = vec4( pi, -pi/2.0, 0.0, 0.0); gl_Position = vec4(-1.0, -1.0, 0.0, 1.0); } else\n"
    "  if (gl_VertexID == 1) { tc = vec4(-pi, -pi/2.0, 1.0, 0.0); gl_Position = vec4( 1.0, -1.0, 0.0, 1.0); } else\n"
    "  if (gl_VertexID == 2) { tc = vec4( pi,  pi/2.0, 0.0, 1.0); gl_Position = vec4(-1.0,  1.0, 0.0, 1.0); } else\n"
    "  if (gl_VertexID == 3) { tc = vec4(-pi,  pi/2.0, 1.0, 1.0); gl_Position = vec4( 1.0,  1.0, 0.0, 1.0); } else\n"
    "  { tc = vec4(0.0); gl_Position = vec4(0.0); }\n"
    "}\n"
    ;
  const char *s_colour_fragment =
    "#version 330 core\n"
    "in vec4 tc;\n"
    "uniform int R;\n"
    "uniform int lod;\n"
    "uniform sampler2D tex;\n"
    "layout (location = 0) out vec4 colour;\n"
    "void main()\n"
    "{\n"
    "  vec3 me = vec3(cos(tc.x) * cos(tc.y), sin(tc.x) * cos(tc.y), sin(tc.y));\n"
    "  float delta = length(vec2(length(dFdx(me)), length(dFdy(me))));\n"
    "  ivec2 s = textureSize(tex, lod);\n"
    "  ivec2 loc = ivec2(floor(vec2(s) * tc.zw));\n"
    "  vec4 nearest = texelFetch(tex, loc, lod);\n"
    "  vec4 nearest2 = nearest;\n"
    "  for (int j = -R; j <= R; ++j)\n"
    "    for (int i = -R; i <= R; ++i)\n"
    "    {\n"
    "      ivec2 c = loc + ivec2(i, j);\n"
    "      if (c.x < 0) c.x += s.x;\n"
    "      if (c.x >= s.x) c.x -= s.x;\n"
    "      if (c.y < 0) continue;\n"
    "      if (c.y >= s.y) continue;\n"
    "      vec4 test = texelFetch(tex, c, lod);\n"
    "      if (distance(me, test.xyz) < distance(me, nearest.xyz))\n"
    "      {\n"
    "        nearest2 = nearest;\n"
    "        nearest = test;\n"
    "      }\n"
    "      else\n"
    "      if (distance(me, test.xyz) < distance(me, nearest2.xyz))\n"
    "      {\n"
    "        nearest2 = test;\n"
    "      }\n"
    "    }\n"
    "  int ix = int(nearest.w);\n"
    "  int ix2 = int(nearest2.w);\n"
    "  vec3 blue = vec3(0.0, 0.5, 1.0);\n"
    "  vec3 white = vec3(1.0);\n"
    "  vec3 red = vec3(1.0, 0.2, 0.0);\n"
    "  vec3 black = vec3(0.0);\n"
    "  vec3 mine = vec3[4](black, blue, red, white)[ix & 3];\n"
    "  vec3 them = vec3[4](black, blue, red, white)[ix2 & 3];\n"
    "  float lmine = length(nearest.xyz - me);\n"
    "  float lthem = length(nearest2.xyz - me);\n"
    "  float lme = lmine - lthem;\n"
    "  colour = vec4(mix(mine, them, smoothstep(-delta, delta, lme)), 1.0);\n"
    "}\n"
    ;
  GLuint p_colour = vertex_fragment_shader(s_colour_vertex, s_colour_fragment);
  glUseProgram(p_colour);
  GLint u_colour_R = glGetUniformLocation(p_colour, "R");
  GLint u_colour_lod = glGetUniformLocation(p_colour, "lod");
  GLint u_colour_tex = glGetUniformLocation(p_colour, "tex");
  glUniform1i(u_colour_R, 0);
  glUniform1i(u_colour_lod, 0);
  glUniform1i(u_colour_tex, 1);
  
  SF_INFO info = { 0, SR, 16, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
  SNDFILE *sndfile = 0;
  FILE *vidfile = stdout;
  if (RECORD)
  {
    sndfile = sf_open("meshwalk-3.2_a.wav", SFM_WRITE, &info);
  }
  else
  {
    for (int j = 0; j < 4; ++j)
    {
      char clientname[100];
      snprintf(clientname, 100, "meshwalk_%d", j + 1);
      if (!(client[j] = jack_client_open(clientname, JackNoStartServer, 0))) {
        fprintf (stderr, "jack server not running?\n");
        return 1;
      }
      jack_set_process_callback(client[j], processcb, &global_which[j]);
      /* create ports */
      for (int i = 0; i < 16; ++i)
      {
        char portname[100];
        snprintf(portname, 100, "output_%d", i + 1);
        port[j][i] = jack_port_register(client[j], portname, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
      }
      /* activate audio */
      if (jack_activate(client[j])) {
        fprintf (stderr, "cannot activate JACK client");
        return 1;
      }
    }
  }

  float frames[SR/FPS][16];
  // write titles
  if (RECORD)
  {
    for (int i = 0; i < SR/FPS; ++i)
      for (int c = 0; c < 16; ++c)
        frames[i][c] = 0;
    for (int f = 0; f < FPS * 3; ++f)
      sf_writef_float(sndfile, &frames[0][0], SR/FPS);
  }
  init();
  int dirty = 1;
  double last = glfwGetTime();
  int last_count = 0;
  memset(&ppm[0][0][0], 0x80, 8192 * 4096 * 3);
  float focus_target[3] = { 0, 0, 0 };
  LOP1 focus_filter[3];
  float fhz = 4.0f * SR / FPS;
  lop1_init(&focus_filter[0], fhz);
  lop1_init(&focus_filter[1], fhz);
  lop1_init(&focus_filter[2], fhz);
  // 1/2 = g * (1 + (1-g) + (1-g)^2 + ... + (1-g)^(H-1))
  //     = g * (1 - (1 - g)^(H-1)) / (1 - (1 - g))
  //     = 1 - (1 - g)^(H-1)
  //   g = 1 - (1/2)^(1/(H-1))
  double geiger = 1.0 - pow(0.5, 4.0 / FPS);
  for (int f = 0; ! glfwWindowShouldClose(window); ++f)
  {
    DISTANCE = 1;
    Ky = DISTANCE * 2.0f * sqrtf(4.0f / N);
    K = 1.0 / (Ky * Ky);
    sqrtK = sqrtf(K);
    if (rand() / (double) RAND_MAX < geiger)
    {
      float R;
      do
      {
        R = 0;
        for (int c = 0; c < 3; ++c)
        {
          focus_target[c] = 2.0f * (rand() / (float) RAND_MAX - 0.5f);
          R += focus_target[c] * focus_target[c];
        }
      } while (R > 1);
      R = sqrtf(R);
      float G = 0.5 * (1 - cos(2 * pi * (f - (FPS * (1 * 60 + 0))) / (FPS * (4 * 60 - 6)))) / R;
      focus_target[0] *= G;
      focus_target[1] *= G;
      focus_target[2] *= G;
    }
    focus[0] = lop1(&focus_filter[0], focus_target[0]);
    focus[1] = lop1(&focus_filter[1], focus_target[1]);
    focus[2] = lop1(&focus_filter[2], focus_target[2]);
    if (DEBUG_AMBISONICS_AXES)
    {
      int k = (f / FPS) % 6;
      const float targets[6][3] =
        { { 1, 0, 0 }, { -1, 0, 0 } // front, back
        , { 0, 1, 0 }, { 0, -1, 0 } // left, right
        , { 0, 0, 1 }, { 0, 0, -1 } // top, bottom
        };
      focus[0] = targets[k][0];
      focus[1] = targets[k][1];
      focus[2] = targets[k][2];
    }
    if (DEBUG_AMBISONICS_SPIN != 0)
    {
      float t = DEBUG_AMBISONICS_SPIN;
      float c = cos(t);
      float s = sin(t);
      float M[3][3] = { { c, s, 0 }, { -s, c, 0}, { 0, 0, 1 } };
      float nV[3][3];
      for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
      {
        nV[i][j] = 0;
        for (int k = 0; k < 3; ++k)
          nV[i][j] += M[i][k] * V[k][j];
      }
      for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
        V[i][j] = nV[i][j];
    }
    double now = glfwGetTime();
    if (now - last > 1)
    {
      fprintf(stderr, "\n%f fps\n%f max\n", (f - last_count) / (now - last), sqrtf(metric) / Ky);
      last_count = f;
      last = now;
    }
    if (RECORD && f >= FPS * (60 * 5 - 6))
      break;

    step();

    if ((RECORD && f >= FPS * 60 * 1) || ! RECORD)
    {
#if RECORD_VIDEO

      // voronoi
      glUseProgram(p_voronoi);
      upload_uniforms();
      glBindFramebuffer(GL_FRAMEBUFFER, fbo[LOD]);
      glViewport(0, 0, 8192 >> LOD, 4096 >> LOD);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

      // refine
      glUseProgram(p_refine);
      for (int lod = LOD; lod >= 1; --lod)
      {
        glBindFramebuffer(GL_FRAMEBUFFER, fbo[lod - 1]);
        glViewport(0, 0, 8192 >> (lod - 1), 4096 >> (lod - 1));
        glUniform1i(u_refine_lod, lod);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
      }
      
      // colourize
      glUseProgram(p_colour);
      glBindFramebuffer(GL_FRAMEBUFFER, fbo2);
      glViewport(0, 0, 8192, 4096);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

      if (RECORD && f >= FPS * 60 * 1 && DRAW_VORONOI)
      {
        dirty = 0;
        glReadPixels(0, 0, 8192, 4096, GL_RGB, GL_UNSIGNED_BYTE, &ppm[0][0][0]);
      }
      else if (dirty && DRAW_CLEAR)
      {
        dirty = 0;
        memset(&ppm[0][0][0], 0, 8192 * 4096 * 3);
      }
      int target = find_target(1, 0, 0);
      if (DRAW_LINES)
      {
        dirty = 1;
        draw_lines(target);
      }
      if (DRAW_POINTS)
      {
        dirty = 1;
        draw_points(target);
      }
      if (DRAW_TRAILS)
      {
        dirty = 1;
        draw_trails();
      }
      // preview
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      glClear(GL_COLOR_BUFFER_BIT);
      glBlitFramebuffer
        ( 0, 0, 8192, 4096
        , 0, (WINH - WINW/2) / 2, WINW, (WINH + WINW/2) / 2
        , GL_COLOR_BUFFER_BIT, GL_LINEAR
        );
      // redisplay
      glfwSwapBuffers(window);
      glfwPollEvents();
      GLuint e;
      while ((e = glGetError())) fprintf(stderr, "GL ERROR %d\n", e);
#endif

      // output PPM to stdout (flipped vertically)
      if (RECORD && f >= FPS * 60 * 1)
      {
#if RECORD_VIDEO
        fprintf(vidfile, "P6\n%d %d\n255\n", 8192, 4096);
        fwrite(&ppm[0][0][0], 8192 * 4096 * 3, 1, vidfile);
        if (f == FPS * 60 * 1)
        {
          FILE *imgfile = popen("pnmtopng -force -interlace -compression 9 > meshwalk-3.2.png", "w");
          fprintf(imgfile, "P6\n%d %d\n255\n", 8192, 4096);
          fwrite(&ppm[0][0][0], 8192 * 4096 * 3, 1, imgfile);
          pclose(imgfile);
        }
#endif
#if RECORD_AUDIO
        float audio[16][SR/FPS];
        float *audiop[16];
        for (int c = 0; c < 16; ++c)
          audiop[c] = &audio[c][0];
        audiocb(audiop, SR/FPS, f * SR/FPS, -1);
        for (int i = 0; i < SR/FPS; ++i)
          for (int c = 0; c < 16; ++c)
            frames[i][c] = audio[c][i];
        sf_writef_float(sndfile, &frames[0][0], SR/FPS);
#endif
      }
    }
  }
  if (RECORD)
  {
    // write credits
    for (int i = 0; i < SR/FPS; ++i)
      for (int c = 0; c < 16; ++c)
        frames[i][c] = 0;
    for (int f = 0; f < FPS * 3; ++f)
      sf_writef_float(sndfile, &frames[0][0], SR/FPS);
    sf_close(sndfile);
  }
  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}
