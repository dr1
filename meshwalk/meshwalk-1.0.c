/*
gcc -std=c99 -Wall -Wextra -pedantic -O3 -march=native \
    -o meshwalk-1.0 meshwalk-1.0.c -lGLEW -lGL -lglfw -lm
for i in $(seq 180) ; do pngtopnm < meshwalk-1.0-title.png ; done |
ffmpeg -r 60 -i - -vf fade=in:30:30,fade=out:120:30 meshwalk-1.0-title-%04d.ppm
for i in $(seq 180) ; do pngtopnm < meshwalk-1.0-credits.png ; done |
ffmpeg -r 60 -i - -vf fade=in:30:30,fade=out:120:30 meshwalk-1.0-credits-%04d.ppm
( cat meshwalk-1.0-title-0*.ppm ; ./meshwalk-1.0 ; cat meshwalk-1.0-credits-0*.ppm ) |
ffmpeg -r 60 -i - -i silence.wav \
    -pix_fmt yuv420p -profile:v high -level:v 4.1 -tune:v animation \
    -movflags +faststart meshwalk-1.0.mov
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define pi 3.141592653589793

static void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    fprintf(stderr, "program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

static void debug_shader(GLuint shader, GLenum type, const char *source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
      case GL_COMPUTE_SHADER: type_str = "compute"; break;
    }
    fprintf
      ( stderr
      , "%s shader compile info:\n%s\nshader source:\n%s"
      , type_str
      , info ? info : "(no info log)"
      , source ? source : "(no source)"
      );
  }
  if (info) {
    free(info);
  }
}

static GLuint vertex_fragment_shader(const char *vert, const char *frag) {
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(shader, 1, &vert, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_VERTEX_SHADER, vert);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(shader, 1, &frag, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_FRAGMENT_SHADER, frag);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  debug_program(program);
  return program;
}

#define W (16 * 8)
#define H ( 9 * 8)

// positions (even store average, odd store edge offsets)
static double g[H][W];
// velocities (odd for edges)
static double dg[H][W];

// OpenGL triangles: one quad per even coordinate, convexity guaranteed
static GLfloat t[H/2 + 2][W/2 + 2][2][2][3][3];

// http://burtleburtle.net/bob/hash/integer.html
static uint32_t burtle_hash(uint32_t a)
{
    a = (a+0x7ed55d16) + (a<<12);
    a = (a^0xc761c23c) ^ (a>>19);
    a = (a+0x165667b1) + (a<<5);
    a = (a+0xd3a2646c) ^ (a<<9);
    a = (a+0xfd7046c5) + (a<<3);
    a = (a^0xb55a4f09) ^ (a>>16);
    return a;
}

// pseudo-random uniform number in [0,1)
static double uniform(uint32_t x, uint32_t y, uint32_t c)
{
  return
    burtle_hash(x +
    burtle_hash(y +
    burtle_hash(c))) /
    (double) (0x100000000LL);
}

// randomize positions, no velocity
static void init()
{
  for (int j = 0; j < H; ++j)
  for (int i = 0; i < W; ++i)
  {
    g[j][i] = 0.25 + 0.5 * uniform(i, j, -1);
    dg[j][i] = 0;
  }
}

// compute average deviation from even coordinates
static void even()
{
  for (int j = 0; j < H; ++j)
  for (int i = 0; i < W; ++i)
  if (((i & 1) == 0) && ((j & 1) == 0))
  {
    int ip = (i + 1) % W;
    int jp = (j + 1) % H;
    int im = (i - 1 + W) % W;
    int jm = (j - 1 + H) % H;
    double xp =     g[j][ip];
    double xm = 1 - g[j][im];
    double yp =     g[jp][i];
    double ym = 1 - g[jm][i];
    double m = 0.25 * (xp + xm + yp + ym);
    g[j][i] = m;
  }
}

// update deviation of even coordinates
static void odd(int k)
{
  // integration time constant
  const double dt = 0.1;
  // friction proportional to velocity
  const double friction = 0.005;
  // elastic collisions at the end points of the intervals
  const double elastic = 0.125;
  // time-varying forcing of the spring constant gives interesting effects
  const double spring = 0.1 + 0.02 * sin(2 * pi * k / 60.0);

  for (int j = 0; j < H; ++j)
  for (int i = 0; i < W; ++i)
  if (((i + j) & 1) == 1) // edges
  {
    // get current position
    double a = g[j][i];
    // get aveage of endpoints
    double ap, am;
    if (i & 1) // horizontal
    {
      int ip = (i + 1) % W;
      int im = (i - 1 + W) % W;
      ap = g[j][ip];
      am = g[j][im];
    }
    else // vertical
    {
      int jm = (j - 1 + H) % H;
      int jp = (j + 1) % H;
      ap = g[jp][i];
      am = g[jm][i];
    }
    double target = 0.5 * (ap + 1 - am);
    // get current velocity
    double d = dg[j][i];
    // compute force
    double f = spring * (target - a) - friction * d;
    // integrate position
    a = a + dt * d;
    // handle far out of bounds
    if (a >  2) { a = 0.5; d = 0; }
    if (a < -1) { a = 0.5; d = 0; }
    // handle elastic reflections at end points
    if (a > 1) { a = 1 - (a - 1); d = - elastic * d; }
    if (a < 0) { a = 0 - (a - 0); d = - elastic * d; }
    // store new position
    g[j][i] = a;
    // integrate velocity
    dg[j][i] = d + dt * f;
  }
}

// update triangle positions for OpenGL
static void tris()
{
  for (int j0 = -1; j0 <= H; ++j0)
  for (int i0 = -1; i0 <= W; ++i0)
  {
    int i = (i0 + W) % W;
    int j = (j0 + H) % H;
    if ((i & 1) == (j & 1)) // even coordinates own a quad
    {
      int x = i0 >> 1;
      int y = j0 >> 1;
      int j1 = y + 1;
      int i1 = x + 1;
      int f = j & 1; // quad type (coordinates both even vs both odd)
      int c = f | ((i0 ^ j0) & 2); // colour
      t[j1][i1][f][0][0][0] = x + f;
      t[j1][i1][f][0][0][1] = y;
      t[j1][i1][f][0][0][2] = c;
      t[j1][i1][f][0][1][0] = x + 1;
      t[j1][i1][f][0][1][1] = y + f;
      t[j1][i1][f][0][1][2] = c;
      t[j1][i1][f][0][2][0] = x + f;
      t[j1][i1][f][0][2][1] = y + 1;
      t[j1][i1][f][0][2][2] = c;
      t[j1][i1][f][1][0][2] = c;
      t[j1][i1][f][1][1][2] = c;
      t[j1][i1][f][1][2][0] = x;
      t[j1][i1][f][1][2][1] = y + f;
      t[j1][i1][f][1][2][2] = c;
      t[j1][i1][f][0][0][  f] += g[(j    ) % H][(i + 1) % W];
      t[j1][i1][f][0][1][1-f] += g[(j + 1) % H][(i + 2) % W];
      t[j1][i1][f][0][2][  f] += g[(j + 2) % H][(i + 1) % W];
      t[j1][i1][f][1][2][1-f] += g[(j + 1) % H][(i    ) % W];
      t[j1][i1][f][1][0][0] = t[j1][i1][f][0][0][0];
      t[j1][i1][f][1][0][1] = t[j1][i1][f][0][0][1];
      t[j1][i1][f][1][1][0] = t[j1][i1][f][0][2][0];
      t[j1][i1][f][1][1][1] = t[j1][i1][f][0][2][1];
    }
  }
}

// GLFW keyboard callback
static void keycb
  ( GLFWwindow *window
  , int key
  , int scancode
  , int action
  , int mods
  )
{
  (void) scancode;
  (void) mods;
  if (action == GLFW_PRESS)
  {
    switch (key)
    {
      case GLFW_KEY_Q:
      case GLFW_KEY_ESCAPE:
        glfwSetWindowShouldClose(window, GL_TRUE);
        break;
    }
  }
}

// entry point
extern int main()
{
  // start OpenGL 3.3 core profile 1920x1080 full screen
  int w = 1920;
  int h = 1080;
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  glfwWindowHint(GLFW_DECORATED, GL_FALSE);
  glfwWindowHint(GLFW_SAMPLES, 32);
  GLFWwindow *window = glfwCreateWindow(w, h, "meshwalk", 0, 0);
  glfwMakeContextCurrent(window);
  glfwSetKeyCallback(window, keycb);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew 
  // set up vertex array object
  glEnable(GL_MULTISAMPLE);
  glClearColor(0, 0, 0.5, 1);
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  // set up buffer
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(t), 0, GL_DYNAMIC_DRAW);
  // set up shader
  const char *s_vertex =
    "#version 330 core\n"
    "uniform mat4 MVP;\n"
    "layout (location = 0) in vec3 position;\n"
    "flat out int c;\n"
    "void main()\n"
    "{\n"
    "  c = int(position.z);\n"
    "  gl_Position = MVP * vec4(position.xy, 0.0, 1.0);\n"
    "}\n"
    ;
  const char *s_fragment = 
    "#version 330 core\n"
    "flat in int c;\n"
    "layout (location = 0) out vec4 colour;\n"
    "void main()\n"
    "{\n"
    "  vec3 blue = vec3(0.0, 0.5, 1.0);\n"
    "  vec3 white = vec3(1.0);\n"
    "  vec3 red = vec3(1.0, 0.2, 0.0);\n"
    "  vec3 black = vec3(0.0);\n"
    "  colour = vec4(vec3[4](blue, white, red, black)[c], 1.0);\n"
    "}\n"
  ;
  GLuint display = vertex_fragment_shader(s_vertex, s_fragment);
  glUseProgram(display);
  GLfloat m[4][4] = // ortho2d
    { { 4.0 / W, 0, 0, 0 }
    , { 0, 4.0 / H, 0, 0 }
    , { 0, 0, -1, 0 }
    , { -1, -1, 0, 1 }
    };
  GLint u_MVP = glGetUniformLocation(display, "MVP");
  glUniformMatrix4fv(u_MVP, 1, GL_FALSE, &m[0][0]);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);
  // main loop
  unsigned char *video = malloc(w * h * 3);
  init();
  glfwPollEvents();
  for (int k = 0; !glfwWindowShouldClose(window); ++k)
  {
    // step
    even();
    odd(k);
    if (k > 0x3FFFF)
    {
      tris();
      // draw
      glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(t), t);
      glClear(GL_COLOR_BUFFER_BIT);
      glDrawArrays(GL_TRIANGLES, 0, (H/2 + 2) * (W/2 + 2) * 2 * 2 * 3);
      // download output frame
      glReadPixels(0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, video);
      // output PPM to stdout (flipped vertically)
      printf("P6\n%d %d\n255\n", w, h);
      fwrite(video, w * h * 3, 1, stdout);
      // redisplay
      glfwSwapBuffers(window);
      glfwPollEvents();
      if (k > 0x3FFFF + 60 * 234)
        break;
    }
  }
  // cleanup
  free(video);
  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}
