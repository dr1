/*
  meshwalk -- audiovisual drone
  Copyright (C) 2018  Claude Heiland-Allen <claude@mathr.co.uk>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
/*
# build (or use `make`)
gcc -std=c99 -Wall -Wextra -pedantic -Ofast -ffast-math -march=native \
    -o meshwalk-2.0 meshwalk-2.0.c \
    -ljack -lsndfile -lGLEW -lGL -lglfw -lm

# realtime mode with JACK audio, press Q to exit
./meshwalk-2.0

# render
for i in $(seq 180) ; do pngtopnm < meshwalk-2.0-title.png ; done |
ffmpeg -r 60 -i - -vf fade=in:30:30,fade=out:120:30 meshwalk-2.0-title-%04d.ppm
for i in $(seq 180) ; do pngtopnm < meshwalk-2.0-credits.png ; done |
ffmpeg -r 60 -i - -vf fade=in:30:30,fade=out:120:30 meshwalk-2.0-credits-%04d.ppm
(
  cat meshwalk-2.0-title-0*.ppm
  ./meshwalk-2.0 -nrt
  cat meshwalk-2.0-credits-0*.ppm
) |
ffmpeg -framerate 60 -i - -codec:v png meshwalk-2.0_v.mov
ffmpeg -i meshwalk-2.0_v.mov -i meshwalk-2.0_a.mov \
  -codec:v copy -codec:a copy -movflags +faststart \
  meshwalk-2.0.mov
ffmpeg -i meshwalk-2.0.mov \
  -pix_fmt yuv420p -profile:v high -level:v 4.1 -tune:v animation \
  -crf:v 20 -b:a 384k -movflags +faststart \
  meshwalk-2.0.mp4
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <jack/jack.h>
#include <sndfile.h>

#define SR 48000
#define FPS 60
#define W (16 * 4)
#define H ( 9 * 4)

#define pi 3.141592653589793

// precondition: 0 <= phase <= 1
static inline float cosf9(float phase) {
  float p = fabsf(4.0f * phase - 2.0f) - 1.0f;
  float p2 = p * p;
  // p in -1 .. 1
  float s
    = 1.5707963267948965580e+00f * p
    - 6.4596271553942852250e-01f * p * p2
    + 7.9685048314861006702e-02f * p * p2 * p2
    - 4.6672571910271187789e-03f * p * p2 * p2 * p2
    + 1.4859762069630022552e-04f * p * p2 * p2 * p2 * p2;
  // compiler figures out optimal simd multiplications
  return s;
}

static inline float wrap(float x) { return x - floor(x); }

static void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    fprintf(stderr, "program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

static void debug_shader(GLuint shader, GLenum type, const char *source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
      case GL_COMPUTE_SHADER: type_str = "compute"; break;
    }
    fprintf
      ( stderr
      , "%s shader compile info:\n%s\nshader source:\n%s"
      , type_str
      , info ? info : "(no info log)"
      , source ? source : "(no source)"
      );
  }
  if (info) {
    free(info);
  }
}

static GLuint vertex_fragment_shader(const char *vert, const char *frag) {
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(shader, 1, &vert, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_VERTEX_SHADER, vert);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(shader, 1, &frag, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_FRAGMENT_SHADER, frag);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  debug_program(program);
  return program;
}

// positions (even store average, odd store edge offsets)
static float g[H][W];
// velocities (odd for edges)
static float dg[H][W];

// OpenGL triangles: two quads per even coordinate, convexity guaranteed
static GLfloat t[H/2 + 2][W/2 + 2][2][2][3][3];

// audio oscillators: one per quad, double buffered with linear interpolation
static int a_which = 0;
static float pan  [2][H/2 + 2][W/2 + 2][2];
static float pitch[2][H/2 + 2][W/2 + 2][2];
static float level[2][H/2 + 2][W/2 + 2][2];
static float phase[H/2 + 2][W/2 + 2][2];

// http://burtleburtle.net/bob/hash/integer.html
static uint32_t burtle_hash(uint32_t a)
{
    a = (a+0x7ed55d16) + (a<<12);
    a = (a^0xc761c23c) ^ (a>>19);
    a = (a+0x165667b1) + (a<<5);
    a = (a+0xd3a2646c) ^ (a<<9);
    a = (a+0xfd7046c5) + (a<<3);
    a = (a^0xb55a4f09) ^ (a>>16);
    return a;
}

// pseudo-random uniform number in [0,1)
static float uniform(uint32_t x, uint32_t y, uint32_t c)
{
  return
    burtle_hash(x +
    burtle_hash(y +
    burtle_hash(c))) /
    (float) (0x100000000LL);
}

// randomize positions, no velocity
static void init()
{
  for (int j = 0; j < H; ++j)
  for (int i = 0; i < W; ++i)
  {
    g[j][i] = 0.25 + 0.5 * uniform(i, j, 0);
    dg[j][i] = 0;
  }
  for (int j = 0; j < H/2 + 2; ++ j)
  for (int i = 0; i < W/2 + 2; ++ i)
  {
    phase[j][i][0] = uniform(i, j, 1);
    phase[j][i][1] = uniform(i, j, 2);
  }
}

// compute average deviation from even coordinates
static void even()
{
  for (int j = 0; j < H; ++j)
  for (int i = 0; i < W; ++i)
  if (((i & 1) == 0) && ((j & 1) == 0))
  {
    int ip = (i + 1) % W;
    int jp = (j + 1) % H;
    int im = (i - 1 + W) % W;
    int jm = (j - 1 + H) % H;
    float xp =     g[j][ip];
    float xm = 1 - g[j][im];
    float yp =     g[jp][i];
    float ym = 1 - g[jm][i];
    float m = 0.25f * (xp + xm + yp + ym);
    g[j][i] = m;
  }
}

// update deviation of even coordinates
static void odd(int k)
{
  // integration time constant
  const float dt = 1.0f/32.0f;
  // friction proportional to velocity
  const float friction = 0.07f * dt;
  // elastic collisions at the end points of the intervals
  const float elastic = 0.125f;
  // time-varying forcing of the spring constant gives interesting effects
  const float spring = 0.1f + 0.02f * sinf(2.0f * pi * k / 60.0f);

  for (int j = 0; j < H; ++j)
  for (int i = 0; i < W; ++i)
  if (((i + j) & 1) == 1) // edges
  {
    // get current position
    float a = g[j][i];
    // get aveage of endpoints
    float ap, am;
    if (i & 1) // horizontal
    {
      int ip = (i + 1) % W;
      int im = (i - 1 + W) % W;
      ap = g[j][ip];
      am = g[j][im];
    }
    else // vertical
    {
      int jm = (j - 1 + H) % H;
      int jp = (j + 1) % H;
      ap = g[jp][i];
      am = g[jm][i];
    }
    float target = 0.5f * (ap + 1.0f - am);
    // get current velocity
    float d = dg[j][i];
    // compute force
    float f = spring * (target - a) - friction * d;
    // integrate position
    a = a + dt * d;
    // handle far out of bounds
    if (a >  2) { a = 0.5; d = 0; }
    if (a < -1) { a = 0.5; d = 0; }
    // handle elastic reflections at end points
    if (a > 1) { a = 1 - (a - 1); d = - elastic * d; }
    if (a < 0) { a = 0 - (a - 0); d = - elastic * d; }
    // store new position
    g[j][i] = a;
    // integrate velocity
    dg[j][i] = d + dt * f;
  }
}

// update triangle positions for OpenGL
static void tris()
{
  int w = 1 - a_which;
  float base_pitch[4] = { 150.0f / SR, 250.0f / SR, 150.0f / SR, 100.0f / SR };
  float gain = 0.5f;
  float volume = gain / sqrtf((H/2 + 2) * (W/2 + 2) * 2);
  for (int j0 = -1; j0 <= H; ++j0)
  for (int i0 = -1; i0 <= W; ++i0)
  {
    int i = (i0 + W) % W;
    int j = (j0 + H) % H;
    if ((i & 1) == (j & 1)) // even coordinates own a quad
    {
      int x = i0 >> 1;
      int y = j0 >> 1;
      int j1 = y + 1;
      int i1 = x + 1;
      int f = j & 1; // quad type (coordinates both even vs both odd)
      int c = f | ((i0 ^ j0) & 2); // colour
      t[j1][i1][f][0][0][0] = x + f;
      t[j1][i1][f][0][0][1] = y;
      t[j1][i1][f][0][0][2] = c;
      t[j1][i1][f][0][1][0] = x + 1;
      t[j1][i1][f][0][1][1] = y + f;
      t[j1][i1][f][0][1][2] = c;
      t[j1][i1][f][0][2][0] = x + f;
      t[j1][i1][f][0][2][1] = y + 1;
      t[j1][i1][f][0][2][2] = c;
      t[j1][i1][f][1][0][2] = c;
      t[j1][i1][f][1][1][2] = c;
      t[j1][i1][f][1][2][0] = x;
      t[j1][i1][f][1][2][1] = y + f;
      t[j1][i1][f][1][2][2] = c;
      float top    = g[(j    ) % H][(i + 1) % W];
      float right  = g[(j + 1) % H][(i + 2) % W];
      float bottom = g[(j + 2) % H][(i + 1) % W];
      float left   = g[(j + 1) % H][(i    ) % W];
      float xpos0, ypos0;
      (void) xpos0;
      {
        float x0 = t[j1][i1][f][0][0][0];
        float x1 = t[j1][i1][f][0][1][0];
        float x2 = t[j1][i1][f][0][2][0];
        float x3 = t[j1][i1][f][1][2][0];
        float y0 = t[j1][i1][f][0][0][1];
        float y1 = t[j1][i1][f][0][1][1];
        float y2 = t[j1][i1][f][0][2][1];
        float y3 = t[j1][i1][f][1][2][1];
        xpos0 = 0.25f * (x0 + x1 + x2 + x3 + 1.0f);
        ypos0 = 0.25f * (y0 + y1 + y2 + y3 + 1.0f);
      }
      t[j1][i1][f][0][0][  f] += top;
      t[j1][i1][f][0][1][1-f] += right;
      t[j1][i1][f][0][2][  f] += bottom;
      t[j1][i1][f][1][2][1-f] += left;
      float x0 = t[j1][i1][f][0][0][0];
      float x1 = t[j1][i1][f][0][1][0];
      float x2 = t[j1][i1][f][0][2][0];
      float x3 = t[j1][i1][f][1][2][0];
      float y0 = t[j1][i1][f][0][0][1];
      float y1 = t[j1][i1][f][0][1][1];
      float y2 = t[j1][i1][f][0][2][1];
      float y3 = t[j1][i1][f][1][2][1];
      float xpos = 0.25f * (x0 + x1 + x2 + x3);
      float ypos = 0.25f * (y0 + y1 + y2 + y3);
      float d1x = x2 - x0;
      float d2x = x3 - x1;
      float d1y = y2 - y0;
      float d2y = y3 - y1;
      d1x *= d1x;
      d2x *= d2x;
      d1y *= d1y;
      d2y *= d2y;
      float d1 = d1x + d1y;
      float d2 = d2x + d2y;
      float areaSquared = d1 * d2;
      t[j1][i1][f][1][0][0] = t[j1][i1][f][0][0][0];
      t[j1][i1][f][1][0][1] = t[j1][i1][f][0][0][1];
      t[j1][i1][f][1][1][0] = t[j1][i1][f][0][2][0];
      t[j1][i1][f][1][1][1] = t[j1][i1][f][0][2][1];
      pan  [w][j1][i1][f] = xpos / ((W >> 1) + 2) * 0.25f;
      pitch[w][j1][i1][f] = pow(1.5, ypos - ypos0) * base_pitch[c];
      level[w][j1][i1][f] = areaSquared * volume;
    }
  }
}

static void audiocb(float *out_left, float *out_right, int nframes)
{
  for (int n = 0; n < nframes; ++n)
  {
    out_left[n] = out_right[n] = 0;
  }
  int w = a_which;
  int w1 = 1 - w;
  for (int j0 = -1; j0 <= H; ++j0)
  {
    float l[nframes], r[nframes];
    for (int n = 0; n < nframes; ++n)
    {
      l[n] = r[n] = 0;
    }
    for (int i0 = -1; i0 <= W; ++i0)
    {
      int i = (i0 + W) % W;
      int j = (j0 + H) % H;
      if ((i & 1) == (j & 1)) // even coordinates own a quad
      {
        int x = i0 >> 1;
        int y = j0 >> 1;
        int j1 = y + 1;
        int i1 = x + 1;
        int f = j & 1; // quad type (coordinates both even vs both odd)
        int c = f | ((i0 ^ j0) & 2); // colour
        float pan_0c = cosf9(        pan[w ][j1][i1][f]);
        float pan_0s = cosf9(0.25f - pan[w ][j1][i1][f]);
        float pan_1c = cosf9(        pan[w1][j1][i1][f]);
        float pan_1s = cosf9(0.25f - pan[w1][j1][i1][f]);
        float pit_0 = pitch[w ][j1][i1][f];
        float pit_1 = pitch[w1][j1][i1][f];
        float lev_0 = level[w ][j1][i1][f];
        float lev_1 = level[w1][j1][i1][f];
        for (int n = 0; n < nframes; ++n)
        {
          float lin = (n + 0.5) / nframes;
          float lin1 = 1 - lin;
          float pan1c  = pan_0c* lin1 + pan_1c* lin;
          float pan1s  = pan_0s* lin1 + pan_1s* lin;
          float pitch1 = pit_0 * lin1 + pit_1 * lin;
          float level1 = lev_0 * lin1 + lev_1 * lin;
          float p = phase[j1][i1][f] = wrap(phase[j1][i1][f] + pitch1);
          float o = 0;
          switch (c)
          {
            case 0: o = 0.5f - p; break;
            case 1: o = fabsf(p - 0.5f) - 0.25f; break;
            case 2: o = p - 0.5f; break;
            case 3: o = (p < 0.5f) - 0.5f; break;
          }
          o *= level1;
          l[n] += o * pan1c;
          r[n] += o * pan1s;
        }
      }
    }
    for (int n = 0; n < nframes; ++n)
    {
      out_left[n] += l[n];
      out_right[n] += r[n];
    }
  }
  a_which = 1 - a_which;
}

jack_client_t *client;
jack_port_t *port[2];
static int processcb(jack_nframes_t nframes, void *arg) {
  (void) arg;
  jack_default_audio_sample_t *out[2];
  out[0] = jack_port_get_buffer(port[0], nframes);
  out[1] = jack_port_get_buffer(port[1], nframes);
  audiocb(out[0], out[1], nframes);
  return 0;
}

// GLFW keyboard callback
static void keycb
  ( GLFWwindow *window
  , int key
  , int scancode
  , int action
  , int mods
  )
{
  (void) key;
  (void) scancode;
  (void) mods;
  if (action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
}

// entry point
extern int main(int argc, char **argv)
{
  (void) argv;
  int RECORD = argc > 1;
  // start OpenGL 3.3 core profile 1920x1080 full screen
  int w = 1920;
  int h = 1080;
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  glfwWindowHint(GLFW_DECORATED, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(w, h, "meshwalk", 0, 0);
  glfwMakeContextCurrent(window);
  glfwSetKeyCallback(window, keycb);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew 
  // set up vertex array object
  glClearColor(0, 1, 0, 1);
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  // set up buffer
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(t), 0, GL_DYNAMIC_DRAW);
  // set up framebuffer
  GLuint fbo;
  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, tex);
  glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 8, GL_RGB, w, h, GL_FALSE);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, tex, 0);
  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE)
    fprintf(stderr, "GL FRAMEBUFFER STATUS %d\n", status);
  // set up shader
  const char *s_vertex =
    "#version 330 core\n"
    "uniform mat4 MVP;\n"
    "layout (location = 0) in vec3 position;\n"
    "flat out int c;\n"
    "void main()\n"
    "{\n"
    "  c = int(position.z);\n"
    "  gl_Position = MVP * vec4(position.xy, 0.0, 1.0);\n"
    "}\n"
    ;
  const char *s_fragment = 
    "#version 330 core\n"
    "flat in int c;\n"
    "layout (location = 0) out vec4 colour;\n"
    "void main()\n"
    "{\n"
    "  vec3 blue = vec3(0.0, 0.5, 1.0);\n"
    "  vec3 white = vec3(1.0);\n"
    "  vec3 red = vec3(1.0, 0.2, 0.0);\n"
    "  vec3 black = vec3(0.0);\n"
    "  colour = vec4(vec3[4](blue, white, red, black)[c], 1.0);\n"
    "}\n"
  ;
  GLuint display = vertex_fragment_shader(s_vertex, s_fragment);
  glUseProgram(display);
  GLfloat m[4][4] = // ortho2d
    { { 4.0 / W, 0, 0, 0 }
    , { 0, 4.0 / H, 0, 0 }
    , { 0, 0, -1, 0 }
    , { -1, -1, 0, 1 }
    };
  GLint u_MVP = glGetUniformLocation(display, "MVP");
  glUniformMatrix4fv(u_MVP, 1, GL_FALSE, &m[0][0]);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);
  // prepare
  init();
  SF_INFO info = { 0, 48000, 2, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
  SNDFILE *sndfile = 0;
  unsigned char *video = 0;
  if (RECORD)
  {
    sndfile = sf_open("meshwalk-2.0_a.wav", SFM_WRITE, &info);
    video = malloc(w * h * 3);
    // write 3 seconds silence for title
    float frames[SR/FPS][2];
    for (int i = 0; i < SR/FPS; ++i)
      for (int c = 0; c < 2; ++c)
        frames[i][c] = 0;
    for (int k = 0; k < 3 * FPS; ++k)
      sf_writef_float(sndfile, &frames[0][0], SR/FPS);
  }
  else
  {
    if (!(client = jack_client_open("meshwalk", JackNoStartServer, 0))) {
      fprintf (stderr, "jack server not running?\n");
      return 1;
    }
    jack_set_process_callback(client, processcb, 0);
    /* create ports */
    port[0] = jack_port_register(
      client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
    );
    port[1] = jack_port_register(
      client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
    );
    /* activate audio */
    if (jack_activate(client)) {
      fprintf (stderr, "cannot activate JACK client");
      return 1;
    }
    /* must be activated before connecting JACK ports */
    const char **ports;
    if ((ports = jack_get_ports(
      client, NULL, NULL, JackPortIsPhysical | JackPortIsInput
    ))) {
      /* connect up to two physical playback ports */
      int i = 0;
      while (ports[i] && i < 2) {
        if (jack_connect(
          client, jack_port_name(port[i]), ports[i]
        )) {
          fprintf(stderr, "cannot connect output port\n");
        }
        i++;
      }
      free(ports);
    }
  }
  // main loop
  glfwPollEvents();
  for (int k = 0; !glfwWindowShouldClose(window); ++k)
  {
    // step
    even();
    odd(k);
    if (k > 0x3FFFF)
    {
      tris();
      glBindFramebuffer(GL_FRAMEBUFFER, fbo);
      glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(t), t);
      glClear(GL_COLOR_BUFFER_BIT);
      glDrawArrays(GL_TRIANGLES, 0, (H/2 + 2) * (W/2 + 2) * 2 * 2 * 3);
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      glBlitFramebuffer
        ( 0, 0, w, h
        , 0, 0, w, h
        , GL_COLOR_BUFFER_BIT, GL_LINEAR
        );
      if (RECORD)
      {
        float audio[2][SR/FPS];
        audiocb(&audio[0][0], &audio[1][0], SR/FPS);
        float frames[SR/FPS][2];
        for (int i = 0; i < SR/FPS; ++i)
          for (int c = 0; c < 2; ++c)
            frames[i][c] = audio[c][i];
        sf_writef_float(sndfile, &frames[0][0], SR/FPS);
        // download output frame
        glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
        glReadPixels(0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, video);
        // output PPM to stdout (flipped vertically)
        printf("P6\n%d %d\n255\n", w, h);
        fwrite(video, w * h * 3, 1, stdout);
      }
      // redisplay
      glfwSwapBuffers(window);
      glfwPollEvents();
      if (RECORD && k >= 0x3FFFF + (4 * 60 - 6) * FPS)
        break;
      GLuint e;
      while ((e = glGetError())) fprintf(stderr, "GL ERROR %d\n", e);
    }
  }
  // cleanup
  if (RECORD)
  {
    // write 3 seconds silence for credits
    float frames[SR/FPS][2];
    for (int i = 0; i < SR/FPS; ++i)
      for (int c = 0; c < 2; ++c)
        frames[i][c] = 0;
    for (int k = 0; k < 3 * FPS; ++k)
      sf_writef_float(sndfile, &frames[0][0], SR/FPS);
    sf_close(sndfile);
    free(video);
  }
  else
  {
    jack_client_close(client);
  }
  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}
