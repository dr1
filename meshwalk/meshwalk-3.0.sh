#!/bin/bash
set -e
(
  for i in $(seq 180)
  do
    pngtopnm < meshwalk-3.0-title.png
  done |
  ffmpeg -r 60 -i - -vf fade=in:30:30,fade=out:120:30 -f image2pipe -codec:v ppm -
  ./meshwalk-3.0 -nrt
  for i in $(seq 180)
  do
    pngtopnm < meshwalk-3.0-credits.png
  done |
  ffmpeg -r 60 -i - -vf fade=in:30:30,fade=out:120:30 -f image2pipe -codec:v ppm -
) |
ffmpeg -framerate 60 -i - -codec:v png -y meshwalk-3.0_v.mkv
ffmpeg -i meshwalk-3.0_v.mkv -i meshwalk-3.0_a.wav \
  -codec:v copy -codec:a copy \
  -y meshwalk-3.0.mkv
ffmpeg -i meshwalk-3.0.mkv -af pan="4| c0=c0 | c1=c1 | c2=c2 | c3=c3" \
  -pix_fmt yuv420p -profile:v high -level:v 5.1 -tune:v animation \
  -crf:v 20 -b:a 512k -movflags +faststart \
  -y meshwalk-3.0_m.mp4
python2.7 ../../../github.com/google/spatial-media/spatialmedia \
  -i --spatial-audio meshwalk-3.0_m.mp4 meshwalk-3.0.mp4
