#include <math.h>
#include <stdlib.h>

#include <m_pd.h>

t_class *nbody_class;

struct vec3 {
  double x;
  double y;
  double z;
};

struct body {
  double m;
  struct vec3 p;
  struct vec3 v;
  struct vec3 a;
};

struct nbody {
  t_object pd;
  t_outlet *o1;
  t_outlet *o2;
  unsigned int n;
  double g;
  double h;
  struct body *bs;
};

void nbody_g(struct nbody *self, t_float g) {
  self->g = g;
}

void nbody_step(struct nbody *self, t_float sf) {
  int s = floor(sf);
  if (s < 1) s = 1;
  for (int k = 0; k < s; ++k) {
    for (int i = 0; i < self->n; ++i) {
      struct vec3 *pi = &self->bs[i].p;
      struct vec3 f = { 0, 0, 0 };
      for (int j = 0; j < self->n; ++j) {
         if (i != j) {
           struct vec3 *pj = &self->bs[j].p;
           struct vec3 d = { pj->x - pi->x, pj->y - pi->y, pj->z - pi->z };
           double d2 = d.x * d.x + d.y * d.y + d.z * d.z;
           double k = self->g * self->bs[j].m / d2;
           f.x += k * d.x; f.y += k * d.y; f.z += k * d.z;
         }
      }
      self->bs[i].a.x = f.x; self->bs[i].a.y = f.y; self->bs[i].a.z = f.z;
    }
    double h = self->h;
    for (int i = 0; i < self->n; ++i) {
      struct body *b = &self->bs[i];
      b->v.x += b->a.x * h; b->v.y += b->a.y * h; b->v.z += b->a.z * h;
      b->p.x += b->v.x * h; b->p.y += b->v.y * h; b->p.z += b->v.z * h;
    }
  }
}

void nbody_randomize(struct nbody *self) {
  double m = 0;
  struct vec3 c = { 0, 0, 0 };
  for (int i = 0; i < self->n; ++i) {
    struct body *b = &self->bs[i];
    b->m = 1;
    b->p.x = 20.0 * ((double) rand() / (double) RAND_MAX - 0.5);
    b->p.y = 20.0 * ((double) rand() / (double) RAND_MAX - 0.5);
    b->p.z = 20.0 * ((double) rand() / (double) RAND_MAX - 0.5);
    b->v.x = 0; b->v.y = 0; b->v.z = 0;
    b->a.x = 0; b->a.y = 0; b->a.z = 0;
    m += b->m;
    c.x += b->m * b->p.x; c.y += b->m * b->p.y; c.z += b->m * b->p.z;
  }
  c.x /= m; c.y /= m; c.z /= m;
  for (int i = 0; i < self->n; ++i) {
    struct body *b = &self->bs[i];
    b->p.x -= c.x; b->p.y -= c.y; b->p.z -= c.z;
  }  
}

void nbody_positions(struct nbody *self) {
  t_atom atoms[4];
  for (int i = 0; i < self->n; ++i) {
    struct vec3 *p = &self->bs[i].p;
    SETFLOAT(&atoms[0], i+1);
    SETFLOAT(&atoms[1], p->x);
    SETFLOAT(&atoms[2], p->y);
    SETFLOAT(&atoms[3], p->z);
    outlet_list(self->o1, &s_list, 4, atoms);
  }
}

void nbody_distances(struct nbody *self) {
  t_atom atoms[3];
  for (int i = 0; i < self->n; ++i) {
    struct vec3 *pi = &self->bs[i].p;
    for (int j = 0; j < self->n; ++j) {
      if (i < j) {
        struct vec3 *pj = &self->bs[j].p;
        struct vec3 d = { pj->x - pi->x, pj->y - pi->y, pj->z - pi->z };
        SETFLOAT(&atoms[0], i+1);
        SETFLOAT(&atoms[1], j+1);
        SETFLOAT(&atoms[2], sqrt(d.x * d.x + d.y * d.y + d.z * d.z));
        outlet_list(self->o2, &s_list, 3, atoms);
      }
    }
  }
}

struct nbody *nbody_new(t_floatarg n) {
  struct nbody *self = pd_new(nbody_class);
  self->o1 = outlet_new(&self->pd, &s_list);
  self->o2 = outlet_new(&self->pd, &s_list);
  self->n = floor(n);
  if (self->n < 1) self->n = 1;
  self->g = 1;
  self->h = 0.001;
  self->bs = malloc(self->n * sizeof(struct body));
  nbody_randomize(self);
  return self;
}

void nbody_free(struct nbody *self) {
  if (self && self->bs) {
    free(self->bs);
  }
}

extern void nbody_setup() {
  nbody_class = class_new(gensym("nbody"), nbody_new, nbody_free, sizeof(struct nbody), 0, A_DEFFLOAT, 0);
  if (!nbody_class) return;
  class_addmethod(nbody_class, nbody_distances, gensym("distances"), 0);
  class_addmethod(nbody_class, nbody_positions, gensym("positions"), 0);
  class_addmethod(nbody_class, nbody_randomize, gensym("randomize"), 0);
  class_addmethod(nbody_class, nbody_step, gensym("step"), A_FLOAT, 0);
  class_addmethod(nbody_class, nbody_g, gensym("g"), A_FLOAT, 0);
}
