% tested with GNU Octave, version 4.4.1-rc2

% number of modes to compute
e = 25;

% height of plate
m = 256;

% width of plate
n = 256;

% kernel (harmonic operator, laplacian)
%kr = 1;
%kernel = [ 0,1,0; 1,-4,1; 0,1,0 ];
% kernel (biharmonic operator, laplacian of laplacian)
kr = 2;
kernel = [ 0,0,1,0,0; 0,2,-8,2,0; 1,-8,20,-8,1; 0,2,-8,2,0; 0,0,1,0,0 ];

% compute number of non-zero values
nz = 0;
for i = 1 : n
  for j = 1 : m
    k = zeros(2 * kr + 1, 2 * kr + 1);
    for di = -kr : kr
      for dj = -kr : kr
        kk = kernel(di + kr + 1, dj + kr + 1);
        ii = di;
        jj = dj;
        if (i + ii < 1)
          ii = -ii;
        endif
        if (i + ii > n)
          ii = -ii;
        endif
        if (j + jj < 1)
          jj = -jj;
        endif
        if (j + jj > m)
          jj = -jj;
        endif
        if (ii != di || jj != dj)
          %kk = -kk;
        endif
        k(ii + kr + 1, jj + kr + 1) = k(ii + kr + 1, jj + kr + 1) + kk;
      endfor
    endfor
    for ii = 1 : 2 * kr + 1
      for jj = 1 : 2 * kr + 1
        if (k(ii, jj) != 0)
          nz = nz + 1;
        endif
      endfor
    endfor
  endfor
endfor

pz = nz / (n * m)^2;
disp(sprintf("sparsity: %f%%", 100 * pz));

% compute sparse matrix
t = zeros(nz, 3);
s = 1;
for i = 1 : n
  for j = 1 : m
    k = zeros(2 * kr + 1, 2 * kr + 1);
    for di = -kr : kr
      for dj = -kr : kr
        kk = kernel(di + kr + 1, dj + kr + 1);
        ii = di;
        jj = dj;
        if (i + ii < 1)
          ii = -ii;
        endif
        if (i + ii > n)
          ii = -ii;
        endif
        if (j + jj < 1)
          jj = -jj;
        endif
        if (j + jj > m)
          jj = -jj;
        endif
        if (ii != di || jj != dj)
          %kk = -kk;
        endif
        k(ii + kr + 1, jj + kr + 1) = k(ii + kr + 1, jj + kr + 1) + kk;
      endfor
    endfor
    for ii = -kr : kr
      for jj = -kr : kr
        if (k(ii + kr + 1, jj + kr + 1) != 0)
          t(s, 1) = (i - 1) * m + j;
          t(s, 2) = (i - 1 + ii) * m + j + jj;
          t(s, 3) = k(ii + kr + 1, jj + kr + 1);
          s = s + 1;
        endif
      endfor
    endfor
  endfor
endfor
s = spconvert(t);
disp("constructed matrix");

% solve the eigen system
opts.maxit = 5000;
[v,ls,f] = eigs(s, e, "sm", opts);
l = diag(ls);
disp("solved eigen system");

% save the raw data
fid = fopen(sprintf("chladni-%d-%d.dat", m, n), "w", "native");
fwrite(fid, l, "float32", "native");
fwrite(fid, v, "float32", "native");
fclose(fid);
disp("saved raw data");
