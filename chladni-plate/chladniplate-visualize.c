// gcc -std=c99 -Wall -pedantic -Wextra -O3 -fopenmp -march=native modes.c -lm

#include <math.h>
#include <stdio.h>

// number of modes
#define E 25

// height of plate
#define M 256

// width of plate
#define N 256

// supersizing
#define A 2

// eigen values
float l[E];

// eigen vectors
float v[M][N];

// supersized vector
float z[M*A][N*A];

// RGB image
unsigned char g[M*A][N*A];

typedef float vec3[3];
typedef float vec4[4];
typedef float mat4[4][4];

// construct a cubic interpolation vector
inline void cubic(vec4 *o, float x) {
  float xx = x * x;
  (*o)[0] = 0.5f * (x * ((2 - x) * x - 1));
  (*o)[1] = 0.5f * (xx * (3 * x - 5) + 2);
  (*o)[2] = 0.5f * (x * ((4 - 3 * x) * x + 1));
  (*o)[3] = 0.5f * xx * (x - 1);
}

// row * matrix * column
inline float vmv(vec4 *l, mat4 *m, vec4 *r) {
  float s = 0;
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      s += (*l)[i] * (*m)[i][j] * (*r)[j];
    }
  }
  return s;
}

// linear blending
inline void mix(vec3 *o, vec3 *a, vec3 *b, float x) {
  float x1 = 1 - x;
  (*o)[0] = (*a)[0] * x1 + x * (*b)[0];
  (*o)[1] = (*a)[1] * x1 + x * (*b)[1];
  (*o)[2] = (*a)[2] * x1 + x * (*b)[2];
}

// avoid array out of bounds
inline int clamp(int i, int lo, int hi) {
  if (i < lo) { return lo; }
  if (i > hi) { return hi; }
  return i;
}

// main program
int main() {

  // read eigen values
  fread(l, E * sizeof(float), 1, stdin);
  for (int k = 0; k < E; ++k) {

    // read eigen vector
    fread(v, M * N * sizeof(float), 1, stdin);

    // upscale with bicubic interpolation
    #pragma omp parallel for schedule(static, 1)
    for (int x = 0; x < A; ++x) {
      float xf = (x + 0.5f) / A;
      vec4 xc;
      cubic(&xc, xf);
      for (int y = 0; y < A; ++y) {
        float yf = (y + 0.5f) / A;
        vec4 yc;
        cubic(&yc, yf);
        for (int i = 0; i < M; ++i) {
          int i0 = clamp(i - 1, 0, M - 1);
          int i1 = clamp(i    , 0, M - 1);
          int i2 = clamp(i + 1, 0, M - 1);
          int i3 = clamp(i + 2, 0, M - 1);
          for (int j = 0; j < N; ++j) {
            int j0 = clamp(j - 1, 0, N - 1);
            int j1 = clamp(j    , 0, N - 1);
            int j2 = clamp(j + 1, 0, N - 1);
            int j3 = clamp(j + 2, 0, N - 1);
            mat4 m =
              { { v[i0][j0], v[i0][j1], v[i0][j2], v[i0][j3] }
              , { v[i1][j0], v[i1][j1], v[i1][j2], v[i1][j3] }
              , { v[i2][j0], v[i2][j1], v[i2][j2], v[i2][j3] }
              , { v[i3][j0], v[i3][j1], v[i3][j2], v[i3][j3] }
              };
            z[i * A + x][j * A + y] = vmv(&xc, &m, &yc);
          }
        }
      }
    }

    // colourize in groups of 2x2 pixels
    #pragma omp parallel for
    for (int x = 0; x < M * A; ++x) {
      int x1 = x - 1;
      if (x == 0) {
        x1 = x + 1;
      }
      for (int y = 0; y < N * A; ++y) {
        int y1 = y - 1;
        if (y == 0) {
          y1 = y + 1;
        }
        float z00 = z[x][y];
        float z01 = z[x][y1];
        float z10 = z[x1][y];
        float z11 = z[x1][y1];
        // contours
        float mi = fminf(fminf(fminf(z00, z01), z10), z11);
        float ma = fmaxf(fmaxf(fmaxf(z00, z01), z10), z11);
        float si = (mi > 0) - (0 > mi);
        float sa = (ma > 0) - (0 > ma);
        int s = si == sa;
        g[x][y] = s * 255;
      }
    }

    // save as PPM
    char filename[100];
    snprintf(filename, 100, "chladni_%.18f_%02d.pgm", l[k], k);
    FILE *f = fopen(filename, "wb");
    fprintf(f, "P5\n%d %d\n255\n", N * A, M * A);
    fwrite(g, M * A * N * A, 1, f);
    fclose(f);
  }

  return 0;
}
