// ./chladniplate # rt
// ./chladniplate input.wav | ffmpeg  -i input.wav -framerate 60 -i - -pix_fmt yuv420p -profile:v high -level:v 4.1 -crf:v 18 -b:a 256k output.mkv

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fftw3.h>
#include <sndfile.h>
#include <jack/jack.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

// calculation dimensions
#define W 320
#define H 180

// upscaling factor
#define AA 4

// RGB image data
static unsigned char g[H*AA][W*AA][3];

typedef float vec3[3];
typedef float vec4[4];
typedef float _Complex cmat4[4][4];

// construct a cubic interpolation vector
static inline void cubic(vec4 *o, float x) {
  float xx = x * x;
  (*o)[0] = 0.5f * (x * ((2 - x) * x - 1));
  (*o)[1] = 0.5f * (xx * (3 * x - 5) + 2);
  (*o)[2] = 0.5f * (x * ((4 - 3 * x) * x + 1));
  (*o)[3] = 0.5f * xx * (x - 1);
}

// row * matrix * column
static inline float _Complex vmv(vec4 *l, cmat4 *m, vec4 *r) {
  float _Complex s = 0;
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      s += (*l)[i] * (*m)[i][j] * (*r)[j];
    }
  }
  return s;
}

// linear blending
static inline void mix(vec3 *o, vec3 *a, vec3 *b, float x) {
  float x1 = 1 - x;
  (*o)[0] = (*a)[0] * x1 + x * (*b)[0];
  (*o)[1] = (*a)[1] * x1 + x * (*b)[1];
  (*o)[2] = (*a)[2] * x1 + x * (*b)[2];
}

// avoid array out of bounds
static inline int clamp(int i, int lo, int hi) {
  if (i < lo) { return lo; }
  if (i > hi) { return hi; }
  return i;
}

// speed of sound
static const float c = 330; // m s^-1

// dimension of plate
static const float L = 10; // m

// attenuation
#define A 0.5

// radius (-2 / log10(A) + 1) ?
#define R 8

// number of FFT bins
#define B 16384

static const float pi = 3.141592653589793;

// compute phasor at a pixel
static inline float _Complex phasor(float x, float y, float _Complex w)
{
  double _Complex s = 0;
  for (int i = -R; i <= R; ++i)
  {
    for (int j = -R; j <= R; ++j)
    {
      float dx = x + (float) i * W / H;
      float dy = y + j;
      float d = sqrtf(dx * dx + dy * dy);
      s += cexpf(d * w);
    }
  }
  return s;
}

// sound file handle
static SNDFILE *f = 0;

// sample rate (set from input file)
static float SR = 44100;

// video framerate
static float FPS = 60;

// number of peak sinusoids to plate
#define PEAKS 12

// FFT hop length
static int HOP = 735;

// number of channels in sound file
static int CHANNELS = 2;

// window for FFT analysis
static float window[B];

// open sound file and initialize parameters
static bool open_input(const char *file)
{
  SF_INFO info = { 0, 0, 0, 0, 0, 0 };
  f = sf_open(file, SFM_READ, &info);
  if (! f) return false;
  SR = info.samplerate;
  CHANNELS = info.channels;
  HOP = roundf(SR / FPS);
  return info.channels > 0;
}

// read and window a frame of input ready for FFT
static bool read_input(float *fft_in)
{
  float q[B][CHANNELS];
  if (B == sf_readf_float(f, &q[0][0], B))
  {
    sf_seek(f, HOP - B, SEEK_CUR);
    for (int i = 0; i < B; ++i)
    {
      fft_in[i] = q[i][0] * window[i];
    }
    return true;
  }
  return false;
}

// close the input file
static void close_input(void)
{
  if (f)
  {
    sf_close(f);
    f = 0;
  }
}

// output PPM to stdout
static void output(void)
{
  fprintf(stdout, "P6\n%d %d\n255\n", W * AA, H * AA);
  fwrite(&g[0][0][0], H * AA * W * AA * 3, 1, stdout);
  fflush(stdout);
}

// tau function for FFT peak estimator
static float tau(float x)
{
  return 1.f/4 * logf(3*x*x + 6*x + 1) - sqrtf(6)/24 * logf((x + 1 - sqrtf(2.f/3)) /  (x + 1 + sqrtf(2.f/3)));
}

// FFT peak estimator
static float quinn2(int k, float _Complex *X)
{
  float x = crealf(X[k]) * crealf(X[k]) + cimagf(X[k]) * cimagf(X[k]);
  float ap = (crealf(X[k + 1]) * crealf(X[k]) + cimagf(X[k + 1]) * cimagf(X[k]))  /  x;
  float dp = -ap / (1 - ap);
  float am = (crealf(X[k - 1]) * crealf(X[k]) + cimagf(X[k - 1]) * cimagf(X[k]))  /  x;
  float dm = am / (1 - am);
  float d = (dp + dm) / 2 + tau(dp * dp) - tau(dm * dm);
  return k + d;
}

// search for largest peak and zero it
static float peak(float _Complex *X, float _Complex *r_out)
{
  float ma = 0.0;
  int k = 0;
  float _Complex mx = 0;
  for (int b = 1; b < B/2; ++b)
  {
    float x = crealf(X[b]);
    float y = cimagf(X[b]);
    float r = x * x + y * y;
    if (r > ma)
    {
      ma = r;
      k = b;
      mx = X[b];
    }
  }
  if (k == 0)
  {
    *r_out = 0;
    return 0;
  }
  else
  {
    *r_out = mx;
    float p = quinn2(k, X);
//    X[k - 1] = 0;
    X[k    ] = 0;
//    X[k + 1] = 0;
    return p;
  }
}

void debug_program(GLuint program, const char *name)
{
  if (program)
  {
    GLint linked = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (linked != GL_TRUE) fprintf(stderr, "%s: OpenGL shader program link failed\n", name);
    GLint len = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
    char *s = calloc(1, len + 1);
    if (! s) abort();
    glGetProgramInfoLog(program, len, 0, s);
    s[len] = 0;
    if (*s)
      fprintf(stderr, "%s: OpenGL shader program info log\n%s\n", name, s);
    free(s);
  }
  else
  {
    fprintf(stderr, "%s: OpenGL shader program creation failed\n", name);
  }
}

void debug_shader(GLuint shader, GLenum typ, const char *name)
{
  const char *tname = "unknown";
  if (typ == GL_VERTEX_SHADER) tname = "vertex";
  if (typ == GL_FRAGMENT_SHADER) tname = "fragment";
  if (shader)
  {
    GLint compiled = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (compiled != GL_TRUE) fprintf(stderr, "%s: OpenGL %s shader compile failed\n", name, tname);
    GLint len = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
    char *s = calloc(1, len + 1);
    if (! s) abort();
    glGetShaderInfoLog(shader, len, 0, s);
    s[len] = 0;
    if (*s) fprintf(stderr, "%s: OpenGL %s shader info log\n%s\n", name, tname, s);
    free(s);
  }
  else
  {
    fprintf(stderr, "%s: OpenGL %s shader creation failed\n", name, tname);
  }
}

void compile_shader(GLuint program, GLenum typ, const char *name, const char *source)
{
  GLuint shader = glCreateShader(typ);
  glShaderSource(shader, 1, &source, 0);
  glCompileShader(shader);
  debug_shader(shader, typ, name);
  glAttachShader(program, shader);
  glDeleteShader(shader);
}

GLuint compile_program(const char *name, const char *vert, const char *frag)
{
  GLuint program = glCreateProgram();
  compile_shader(program, GL_VERTEX_SHADER, name, vert);
  compile_shader(program, GL_FRAGMENT_SHADER, name, frag);
  glLinkProgram(program);
  debug_program(program, name);
  return program;
}

const char *chladni_vert =
"#version 330 core\n"
"void main() {\n"
"  if (gl_VertexID == 0) { gl_Position = vec4(-1.0, -1.0, 0.0, 1.0); } else\n"
"  if (gl_VertexID == 1) { gl_Position = vec4( 1.0, -1.0, 0.0, 1.0); } else\n"
"  if (gl_VertexID == 2) { gl_Position = vec4(-1.0,  1.0, 0.0, 1.0); } else\n"
"                        { gl_Position = vec4( 1.0,  1.0, 0.0, 1.0); }\n"
"}\n"
;

const char *chladni_frag =
"#version 330 core\n"
"#extension GL_ARB_explicit_uniform_location : require\n"
"#define PEAKS 12\n"
"layout (location = 0) uniform vec2 size;\n"
"layout (location = 1) uniform int sides;\n"
"layout (location = 2) uniform vec4 peaks[PEAKS];\n"
"vec2 cmul(vec2 a, vec2 b) { return vec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x); }\n"
"vec2 cdiv(vec2 a, vec2 b) { return cmul(a, vec2(b.x, -b.y)) / dot(b, b); }\n"
"vec2 cexp(vec2 t) { return exp(t.x) * vec2(cos(t.y), sin(t.y)); }\n"
"vec2 phasor(vec2 q, vec2 w) {\n"
"  vec2 s = vec2(0.0);\n"
"  if (sides == 4) {\n"
"    for (int i = -8; i <= 8; ++i) {\n"
"      float dx = q.x + float(i) * size.x / size.y;\n"
"      for (int j = -8; j <= 8; ++j) {\n"
"        float dy = q.y + float(j);\n"
"        float d = sqrt(dx * dx + dy * dy);\n"
"        s += cexp(d * w);\n"
"      }\n"
"    }\n"
"  }\n"
"  if (sides == 6) {\n"
"    for (int j = -8; j <= 8; ++j) {\n"
"      float dy0 = q.y + float(j);\n"
"      for (int i = -8; i <= 8; ++i) {\n"
"        float dx = q.x + float(i) * sqrt(3.0)/2.0;\n"
"        float dy = dy0 + (1 == (i & 1) ? 0.5 : 0.0);\n"
"        float d = sqrt(dx * dx + dy * dy);\n"
"        s += cexp(d * w);\n"
"      }\n"
"    }\n"
"  }\n"
"  return s;\n"
"}\n"
"vec2 phasors(vec2 q) {\n"
"  vec2 s = vec2(0.0);\n"
"  for (int p = 0; p < PEAKS; ++p) {\n"
"    vec2 a = peaks[p].xy;\n"
"    vec2 w = peaks[p].zw;\n"
"    s += length(a) * phasor(q, w);\n"
"  }\n"
"  return s;\n"
"}\n"
"// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl\n"
"vec3 hsv2rgb(vec3 c) {\n"
"  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);\n"
"  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);\n"
"  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);\n"
"}\n"
"void main() {\n"
"  vec2 q = gl_FragCoord.xy / size - vec2(0.5);\n"
"  q.x *= size.x / size.y;\n"
"  vec2 s = cdiv(phasors(q), normalize(phasors(vec2(0.0))));\n"
"  float hue = atan(s.y, s.x) / (2.0 * 3.141592653);\n"
"  hue -= floor(hue);\n"
"  float sat = clamp(2.0 - length(s), 0.0, 1.0);\n"
"  float val = clamp(length(s), 0.0, 1.0);\n"
"  gl_FragColor = vec4(hsv2rgb(vec3(hue, sat, val)), 1.0);\n"
"}\n"
;

void keycb(GLFWwindow *win, int key, int scancode, int action, int mods)
{
  (void) key;
  (void) scancode;
  (void) action;
  (void) mods;
  glfwSetWindowShouldClose(win, GL_TRUE);
}

static int wpeaks = 0;
static vec4 peaks[2][PEAKS];

static float abuffer[B];
static int awptr = 0;
static int acount = 0;
static float *fft_in;
static float _Complex *fft_out;
static fftwf_plan plan;

static jack_client_t *client;
static jack_port_t *input;

static int processcb(jack_nframes_t nframes, void *arg)
{
  (void) arg;
  jack_default_audio_sample_t *in = jack_port_get_buffer(input, nframes);
  for (jack_nframes_t i = 0; i < nframes; ++i)
  {
    abuffer[awptr++] = in[i];
    if (awptr >= B) awptr = 0;
    if (++acount == HOP)
    {
      acount = 0;
      for (int j = 0; j < B; ++j)
      {
        fft_in[j] = window[j] * abuffer[(awptr + j) % B];
      }
      fftwf_execute(plan);
      static float r0 = 0;
      for (int k = 0; k < PEAKS; ++k)
      {
        float _Complex r = 0;
        float bin = peak(fft_out, &r);
        r0 = fmaxf(r0, cabsf(r));
        float f = SR * bin / B;
        float wavelength = c / f / L;
        float _Complex w = logf(A) + I * 2 * pi / wavelength;
        if (r0 > 0) r = 0.25 * r / r0;
        peaks[wpeaks][k][0] = crealf(r);
        peaks[wpeaks][k][1] = cimagf(r);
        peaks[wpeaks][k][2] = crealf(w);
        peaks[wpeaks][k][3] = cimagf(w);
      }
      wpeaks = 1 - wpeaks;
    }
  }
  return 0;
}

// program entry point
int main(int argc, char **argv)
{
  // prepare FFT
  fft_in = fftwf_malloc(B * sizeof(*fft_in));
  fft_out = fftwf_malloc(B * sizeof(*fft_out));
  plan = fftwf_plan_dft_r2c_1d(B, fft_in, fft_out, FFTW_PATIENT | FFTW_PRESERVE_INPUT);
  for (int i = 0; i < B; ++i)
  {
    window[i] = (1 - cos(2 * pi * (i + 0.5f) / B)) / B;
  }
  // process audio
  if (argc == 1)
  {
    if (!(client = jack_client_open("chladni", JackNoStartServer, 0))) {
      fprintf(stderr, "jack server not running?\n");
      return 1;
    }
    jack_set_process_callback(client, processcb, 0);
    // mono processing
    input = jack_port_register(client, "input_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    if (jack_activate(client)) {
      fprintf (stderr, "cannot activate JACK client");
      return 1;
    }
  }
  else
  {
    fprintf(stderr, "%s: non-realtime mode not yet re-implemented\n", argv[0]);
    return 1;
  }
  // visualize
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_DECORATED, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow *win = glfwCreateWindow(W * AA, H * AA, "chladniplate", 0, 0);
  glfwMakeContextCurrent(win);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew
  glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
  glfwSetKeyCallback(win, keycb);
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  glUseProgram(compile_program("chladni", chladni_vert, chladni_frag));
  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, W, H, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  GLuint fbo;
  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);
  glUniform2f(0, W, H);
  glUniform1i(1, 6);
  while (1)//read_input(fft_in))
  {
    glUniform4fv(2, PEAKS, &peaks[1-wpeaks][0][0]);
    glViewport(0, 0, W, H);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
//    glViewport(0, 0, W * AA, H * AA);
    glBlitFramebuffer(0, 0, W, H, 0, 0, W * AA, H * AA, GL_COLOR_BUFFER_BIT, GL_LINEAR);
     //glReadPixels(0, 0, W * AA, H * AA, GL_RGB, GL_UNSIGNED_BYTE, &g[0][0][0]);
    glfwSwapBuffers(win);
    //output();
    glfwPollEvents();
    if (glfwWindowShouldClose(win)) {
      break;
    }
    GLenum e;
    while ((e = glGetError()))
    {
      fprintf(stderr, "%d\n", e);
    }
  }
  // cleanup
  fftwf_destroy_plan(plan);
  fftwf_free(fft_out);
  fftwf_free(fft_in);
  close_input();
  return 0;
}
