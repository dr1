#define _DEFAULT_SOURCE
#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <sndfile.h>

#ifndef USE_THREADED_FFTW
#define USE_THREADED_FFTW 1
#endif

#include <fftw3.h>
#if USE_THREADED_FFTW
#include <omp.h>
#define WISDOM_FILE "wisdom-omp.fftw3"
#else
#define WISDOM_FILE "wisdom.fftw3"
#endif

int main(int argc, char **argv)
{
  SF_INFO info = { 0 };
  if (argc < 3 || argc > 8)
  {
    fprintf(stderr, "usage: %s in.wav out.wav fftsize audiosize outstep instep threads\n", argv[0]);
    return 1;
  }
  const int FFTSIZE = argc > 3 ? atoi(argv[3]) : 1 << 18;
  const int AUDIOSIZE = argc > 4 ? atoi(argv[4]) : 1 << 13;
  const int OUTSTEP = argc > 5 ? atoi(argv[5]) : AUDIOSIZE / 4;
  const int INSTEP = argc > 6 ? atoi(argv[6]) : 1;
  const int THREADS = USE_THREADED_FFTW ? 1 : argc > 7 ? atoi(argv[7]) : 2;
  SNDFILE *ifile = sf_open(argv[1], SFM_READ, &info);
  if (! ifile || info.channels <= 0)
  {
    fprintf(stderr, "error opening input sound file '%s'\n", argv[1]);
    return 1;
  }
  const int CHANNELS = info.channels;

  // allocate memory
#ifdef USE_THREADED_FFTW
  if (! fftw_init_threads())
  {
    fprintf(stderr, "error initializing fftw threads\n");
    return 1;
  }
  fftw_plan_with_nthreads(omp_get_max_threads());
#endif
  double *window = fftw_malloc(sizeof(*window) * AUDIOSIZE);
  double *audio_in = fftw_malloc(sizeof(*audio_in) * AUDIOSIZE * CHANNELS);
  double *audio_out = fftw_malloc(sizeof(*audio_out) * AUDIOSIZE * CHANNELS);
  double _Complex *fft_in = fftw_malloc(sizeof(*fft_in) * FFTSIZE);
  double _Complex *fft_out = fftw_malloc(sizeof(*fft_out) * FFTSIZE);
  double _Complex *fft_small_in = fftw_malloc(sizeof(*fft_small_in) * THREADS * THREADS);
  double _Complex *fft_small_out = fftw_malloc(sizeof(*fft_small_out) * THREADS * THREADS);
  double _Complex **fft_old = fftw_malloc(sizeof(*fft_old) * CHANNELS);
  double _Complex **fft_new = fftw_malloc(sizeof(*fft_new) * CHANNELS);
  double _Complex **fft_acc = fftw_malloc(sizeof(*fft_acc) * CHANNELS);
  for (int c = 0; c < CHANNELS; ++c)
  {
    fft_old[c] = fftw_malloc(sizeof(*fft_old[c]) * FFTSIZE);
    fft_new[c] = fftw_malloc(sizeof(*fft_new[c]) * FFTSIZE);
    fft_acc[c] = fftw_malloc(sizeof(*fft_acc[c]) * FFTSIZE);
  }
  double _Complex *twiddle = fftw_malloc(sizeof(*twiddle) * FFTSIZE);
  fftw_plan *forward_large = fftw_malloc(sizeof(*forward_large) * THREADS);
  fftw_plan *backward_large = fftw_malloc(sizeof(*backward_large) * THREADS);
  fftw_plan *forward_small = fftw_malloc(sizeof(*forward_small) * THREADS);
  fftw_plan *backward_small = fftw_malloc(sizeof(*backward_small) * THREADS);

  SF_INFO onfo = { 0, info.samplerate, info.channels, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
  SNDFILE *ofile = sf_open(argv[2], SFM_WRITE, &onfo);
  if (! ofile)
  {
    fprintf(stderr, "error opening output sound file '%s'\n", argv[2]);
    return 1;
  }

  // plan FFTs
  fprintf(stderr, "planning fft...\n");
  fftw_import_wisdom_from_filename(WISDOM_FILE);
  fftw_set_timelimit(60 * 60); // one hour
  for (int t = 0; t < THREADS; ++t)
  {
    forward_large[t]  = fftw_plan_dft_1d(FFTSIZE / THREADS, &fft_in [t * FFTSIZE / THREADS], &fft_out[t * FFTSIZE / THREADS], FFTW_FORWARD, FFTW_PATIENT | FFTW_DESTROY_INPUT);
    backward_large[t] = fftw_plan_dft_1d(FFTSIZE / THREADS, &fft_out[t * FFTSIZE / THREADS], &fft_in [t * FFTSIZE / THREADS], FFTW_BACKWARD, FFTW_PATIENT | FFTW_DESTROY_INPUT);
    if (THREADS > 1)
    {
      forward_small[t]  = fftw_plan_dft_1d(THREADS, &fft_small_in [t * THREADS], &fft_small_out[t * THREADS], FFTW_FORWARD, FFTW_PATIENT | FFTW_DESTROY_INPUT);
      backward_small[t] = fftw_plan_dft_1d(THREADS, &fft_small_out[t * THREADS], &fft_small_in [t * THREADS], FFTW_BACKWARD, FFTW_PATIENT | FFTW_DESTROY_INPUT);
    }
  }
  fftw_export_wisdom_to_filename(WISDOM_FILE);
  fprintf(stderr, "fft planned\n");

  // compute twiddle factors
  if (! USE_THREADED_FFTW)
  {
    #pragma omp parallel for
    for (int i = 0; i < FFTSIZE; ++i)
    {
      double N = FFTSIZE;
      double T = THREADS;
      int t = i * THREADS / FFTSIZE;
      int k = i % (FFTSIZE / THREADS);
      twiddle[i] = cexp((-2 * M_PI * I * k * t / N) - (-2 * M_PI * I * k * t / T));
    }
  }
  for (int i = 0; i < AUDIOSIZE; ++i)
  {
    window[i] = (1 - cos(6.283185307179586 * i / AUDIOSIZE)) / sqrt(FFTSIZE);
  }
  double GAIN = (2.0 * OUTSTEP) / AUDIOSIZE;

  // initialize
  for (int c = 0; c < CHANNELS; ++c)
  {
    for (int i = 0; i < FFTSIZE; ++i)
    {
      fft_old[c][i] = 0;
      fft_new[c][i] = 0;
      fft_acc[c][i] = 1;
    }
    for (int i = 0; i < AUDIOSIZE; ++i)
    {
      audio_in[i * CHANNELS + c] = 0;
    }
  }

  // process
  int drain = AUDIOSIZE;
  while (drain > 0)
  {

    // input audio
    for (int c = 0; c < CHANNELS; ++c)
    {
      for (int i = 0; i < AUDIOSIZE - INSTEP; ++i)
      {
        audio_in[i * CHANNELS + c] = audio_in[(i + INSTEP) * CHANNELS + c];
      }
      for (int i = AUDIOSIZE - INSTEP; i < AUDIOSIZE; ++i)
      {
        audio_in[i * CHANNELS + c] = 0;
      }
    }
    if (drain == AUDIOSIZE)
    {
      if (INSTEP != sf_readf_double(ifile, &audio_in[(AUDIOSIZE - INSTEP) * CHANNELS + 0], INSTEP))
      {
        drain--;
      }
    }
    else
    {
      drain -= INSTEP;
    }

    for (int c = 0; c < CHANNELS; ++c)
    {

      // shift input
      for (int i = 0; i < AUDIOSIZE; ++i)
      {
        if (i < AUDIOSIZE - OUTSTEP)
        {
          audio_out[i * CHANNELS + c] = audio_out[(i + OUTSTEP) * CHANNELS + c];
        }
        else
        {
          audio_out[i * CHANNELS + c] = 0;
        }
      }

      // deinterleave
      #pragma omp parallel for
      for (int j = 0; j < FFTSIZE; ++j)
      {
        int t = j * THREADS / FFTSIZE;
        int i = t * FFTSIZE / THREADS + (j % (FFTSIZE / THREADS));
        if (i < AUDIOSIZE)
        {
          fft_in[j] = window[i] * audio_in[i * CHANNELS + c];
        }
        else
        {
          fft_in[j] = 0;
        }
      }

      // fft
      #pragma omp parallel for if (THREADS > 1)
      for (int t = 0; t < THREADS; ++t)
      {
        fftw_execute(forward_large[t]);
      }
      if (THREADS > 1)
      {
        #pragma omp parallel for
        for (int i = 0; i < FFTSIZE; ++i)
        {
          fft_out[i] *= twiddle[i];
        }
        #pragma omp parallel for
        for (int t = 0; t < THREADS; ++t)
        {
          for (int i = 0; i < FFTSIZE / THREADS / THREADS; ++i)
          {
            for (int j = 0; j < THREADS; ++j)
            {
              int k = j * FFTSIZE / THREADS + i * THREADS + t; 
              fft_small_in[t * THREADS + j] = fft_out[k];
            }
            fftw_execute(forward_small[t]);
            for (int j = 0; j < THREADS; ++j)
            {
              int k = j * FFTSIZE / THREADS + i * THREADS + t; 
              fft_out[k] = fft_small_out[t * THREADS + j];
            }
          }
        }
      }

      // phase vocoder
      #pragma omp parallel for
      for (int i = 0; i < FFTSIZE; ++i)
      {
        fft_old[c][i] = fft_new[c][i];
        fft_new[c][i] = fft_out[i];
        double _Complex ratio0 = fft_new[c][i] / fft_old[c][i];
        ratio0 /= cabs(ratio0);
        double _Complex ratio = ratio0;
#if 1
        ratio = cpow(ratio, OUTSTEP / (double) INSTEP);
#else
        for (int s = OUTSTEP / INSTEP; s > 1; s >>= 1)
        {
          ratio *= ratio;
        }
#endif
        fft_acc[c][i] *= ratio;
        fft_acc[c][i] /= cabs(fft_acc[c][i]);
        if (fft_acc[c][i] == 0 || isnan(creal(fft_acc[c][i])) || isnan(cimag(fft_acc[c][i])) || isinf(creal(fft_acc[c][i])) || isinf(cimag(fft_acc[c][i])))
        {
          fft_acc[c][i] = 1;
        }
        fft_out[i] = cabs(fft_new[c][i]) * fft_acc[c][i];
      }

      // ifft
      if (THREADS > 1)
      {
        #pragma omp parallel for
        for (int t = 0; t < THREADS; ++t)
        {
          for (int i = 0; i < FFTSIZE / THREADS / THREADS; ++i)
          {
            for (int j = 0; j < THREADS; ++j)
            {
              int k = j * FFTSIZE / THREADS + i * THREADS + t; 
              fft_small_out[t * THREADS + j] = fft_out[k];
            }
            fftw_execute(backward_small[t]);
            for (int j = 0; j < THREADS; ++j)
            {
              int k = j * FFTSIZE / THREADS + i * THREADS + t; 
              fft_out[k] = fft_small_in[t * THREADS + j];
            }
          }
        }
        #pragma omp parallel for
        for (int i = 0; i < FFTSIZE; ++i)
        {
          fft_out[i] /= twiddle[i];
        }
      }
      #pragma omp parallel for if (THREADS > 1)
      for (int t = 0; t < THREADS; ++t)
      {
        fftw_execute(backward_large[t]);
      }

      // overlap-add output
      for (int i = 0; i < AUDIOSIZE; ++i)
      {
        audio_out[i * CHANNELS + c] += GAIN * window[i] * creal(fft_in[i]);
      }
    }

    // output
    sf_writef_double(ofile, &audio_out[0 * CHANNELS + 0], OUTSTEP);
  }
  sf_close(ofile);
  sf_close(ifile);
  return 0;
}
