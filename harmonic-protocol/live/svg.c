#include <math.h>
#include <stdio.h>

int main(int argc, char **argv)
{
  double w = 1000;
  double h = 1000;
  printf("<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink=width='100%' height='100%' viewBox='0 0 1000 1000'>\n");
  int count[5] = { 23, 19, 12, 7, 5 };
  double radius[5] = { 900, 700, 500, 300, 100 };
  for (int ring = 0; ring < 5; ++ring)
  {
    printf(" <g>\n");
    for (int dot = 1; dot < count[ring]; ++dot)
    {
      double angle = dot * 6.283185307179586 / count[ring];
      double x = w / 2 + radius[ring] * sin(angle);
      double y = h / 2 - radius[ring] * cos(angle);
      printf("  <a href='#%d/%d'><circle cx='%f' cy='%f' r='25' /></a>\n", count[ring], dot, x, y);
    }
    printf(" </g>\n");
  }
  printf(" <g>\n");
  printf("  <text x='%f' y='%f' transform='rotate(-90)' dominant-baseline='middle'>harmonic protocol</text>\n");
  printf(" </g>\n");
  printf("</svg>\n");
  return 0;
}
